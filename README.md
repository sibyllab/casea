# **C**reation **A**ssistant **S**upporting **T**riage **R**ecommenders (CASTR)#

CASTR is a tool that provides a software development project a means for configuring a machine learning recommender to assist with triage decisions such as assignment and component selection.

The diagram shows an overview of the system.
![CASToR-Overview.png](https://bitbucket.org/repo/6ajEy7/images/460510995-CASToR-Overview.png)

### Downloads ###
A stable version of CASTR is available [here](https://bitbucket.org/bugtriage/castr/downloads/CASTR%20v1.0.zip).

CASTR is under the [GPL 3.0 license](https://bitbucket.org/bugtriage/castr/raw/b58d431e6b557b45e2001bfd2dc606b88107e52a/LICENSE.txt).

### Videos ###
These are some videos showing CASTR in action.

* [Creating a Component Recommender (Data from Web)](https://bitbucket.org/bugtriage/castr/raw/4a3b43ba14960e84adf8a8d87f00c43835436acd/videos/WebDemo.mp4)
* [Creating an Assignment Recommender (Data from Database)](https://bitbucket.org/bugtriage/castr/raw/4a3b43ba14960e84adf8a8d87f00c43835436acd/videos/DBDemo.mp4)
* [Using Configuration Estimation (Data from Files)](https://bitbucket.org/bugtriage/castr/raw/c9ba094f73bee8c70b57206d6ec9eb297f1fab5f/videos/XMLDemo.mp4)

### Papers ###
Several academic papers have been written about the project.

* [Assisting Software Projects with Bug Report Assignment Recommender Creation (2014)](https://bitbucket.org/bugtriage/castr/raw/87c9760bf208b20b332bec93a88db10ea63322be/papers/seke2014.pdf)
* [Evaluating an Assistant for Creating Bug Report Assignment Recommenders (2016)](https://bitbucket.org/bugtriage/castr/raw/87c9760bf208b20b332bec93a88db10ea63322be/papers/enchires2016.pdf)


### How to Contribute ###

1. Submit ideas for improvements in the issue tracking system.
2. Write code to resolve an issue in the tracking system.

**The project requires Visual Studio 2015 to compile (can get from [here](https://www.visualstudio.com/en-us/products/visual-studio-community-vs.aspx))**

## Project Members ##
:star2: @janvik
:star2: @Trevmeister42
:star2: @BurtonH 
:star2: @CanadaJ