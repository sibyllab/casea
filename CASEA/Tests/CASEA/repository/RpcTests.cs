﻿/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using CASEA.Core;
using CASEA.src.repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Tests.CASEA.repository
{
    [TestClass]
    public class RpcTests
    {
        private BugzillaRepository _repository;

        [TestInitialize]
        public void Initialize()
        {
            _repository = new BugzillaRepository("https://bugzilla.mozilla.org/jsonrpc.cgi");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void GetCompleteReportsTest()
        {
            var results = _repository.GetCompleteReports("Firefox", DateTime.Now.AddMonths(-6), 50);

            Assert.IsTrue(results.Any());
            Assert.IsTrue(results.TrueForAll(result => !string.IsNullOrWhiteSpace(result.resolver)));
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void GetProductsTest()
        {
            var result = _repository.GetProducts();

            Assert.IsTrue(result.Any());
            Assert.IsTrue(result.TrueForAll(rec => !string.IsNullOrWhiteSpace(rec)));
        }
    }
}
