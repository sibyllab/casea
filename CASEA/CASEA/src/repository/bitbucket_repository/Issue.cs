﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace CASEA.src.repository.bitbucket_repository
{
    [DataContract]
    public class Issue
    {
        // assigned
        [DataMember]
        public string assignee;

        // component
        [DataMember]
        public string component;

        // bug_text
        [DataMember]
        public string content;

        [DataMember]
        public string content_updated_on;

        // created
        [DataMember]
        public string created_on;

        [DataMember]
        public string edited_on;

        // reportid
        [DataMember]
        public int id;

        [DataMember]
        public string kind;

        [DataMember]
        public string milestone;

        // priority
        [DataMember]
        public string priority;

        // reporter
        [DataMember]
        public string reporter;

        // pathgroup
        [DataMember]
        public string status;

        [DataMember]
        public string title;

        [DataMember]
        public string updated_on;

        [DataMember]
        public string version;

        [DataMember]
        public string[] watchers;

        [DataMember]
        public string[] voters;
    }
}
