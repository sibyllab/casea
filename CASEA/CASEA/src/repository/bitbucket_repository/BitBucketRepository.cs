﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace CASEA.src.repository.bitbucket_repository
{
    // The fields that are used for generating datasets are labelled in their files.
    // See Issue.cs and Log.cs.
    [DataContract]
    public class BitbucketRepository
    {
        [DataMember]
        public Issue[] issues;

        [DataMember]
        public Log[] logs;

        public BitbucketRepository()
        {
            
        }

        public void Sort()
        {
            // Issues sorted by issue ID ASC.
            issues = issues.OrderBy(iss => iss.id).ToArray();
            // Logs sorted by issue ID ASC and then date ASC.
            logs = logs.OrderBy(log => log.issue).ThenBy(log => log.created_on).ToArray();
        }
    }
}
