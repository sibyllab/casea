﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace CASEA.src.repository.bitbucket_repository
{
    [DataContract]
    public class Log
    {
        [DataMember]
        public string changed_from;

        // resolver/firstresponder
        [DataMember]
        public string changed_to;

        [DataMember]
        public int comment;

        [DataMember]
        public string created_on;

        [DataMember]
        public string field;

        [DataMember]
        public int issue;

        // resolver/firstresponder
        [DataMember]
        public string user;
    }
}
