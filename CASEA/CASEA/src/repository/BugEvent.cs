﻿/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace CASEA.src.repository
{
    class BugEvent : IBugEvent
    {
        private XElement Event;
        public XElement bugEvent
        {
            get
            {
                return Event;
            }
            set
            {
                Event = new XElement("event", value);
            }
        }

        private XElement Name;
        public String name
        {
            get
            {
                return Name.Value;
            }
            set
            {
                Name = new XElement("name", value);
            }
        }

        private XElement Date;
        public String date
        {
            get
            {
                return Date.Value;
            }
            set
            {
                Date = new XElement("date", value);
            }
        }

        private XElement What;
        public String what
        {
            get
            {
                return What.Value;
            }
            set
            {
                if (value != "")
                    What = new XElement("what", value);
            }
        }

        private XElement Added;
        public String added
        {
            get
            {
                return Added.Value;
            }
            set
            {
                if (value != "")
                    Added = new XElement("added", value);
            }
        }

        private XElement Removed;
        public String removed
        {
            get
            {
                return Removed.Value;
            }
            set
            {
                if (value != "")
                    Removed = new XElement("removed", value);
            }
        }

        public void initEvent()
        {
            bugEvent = null;
            added = "---";
            date = "---";
            name = "---";
            removed = "---";
            what = "---";
        }

        public void clearEvent()
        {
            bugEvent = null;
            added = "---";
            date = "---";
            name = "---";
            removed = "---";
            what = "---";
        }

        public void populateReport()
        {
            bugEvent = new XElement("event",
                Name,
                Date,
                What,
                Added,
                Removed);
        }
    }
}
