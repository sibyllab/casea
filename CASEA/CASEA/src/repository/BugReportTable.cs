﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace CASEA.src.repository
{
    public class BugReportTable : DataTable
    {
        public BugReportTable()
        {
            TableName = "bugreports";

            Columns.Add("reportid");
            Columns.Add("pathgroup");
            Columns.Add("assigned");
            Columns.Add("severity");
            Columns.Add("fixedby");
            Columns.Add("resolver");
            Columns.Add("firstresponder");
            Columns.Add("bug_text");
            Columns.Add("status");
            Columns.Add("created");
            Columns.Add("reporter");
            Columns.Add("resolution");
            Columns.Add("component");
            Columns.Add("duplicates");
        }
    }
}
