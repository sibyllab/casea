﻿/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/

using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using CASEA.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CASEA.src.repository
{
    public class BugzillaRepository
    {
        private string Url { get; }

        private readonly string[] _fieldsToIncludeInSearch =
        {
            "assigned_to", "component", "creator", "dupe_of", "keywords", "product",
            "resolution", "bug_text", "priority", "creation_time", "id"
        };

        private readonly string[] _resolutionsToIncludeInSearch = {"FIXED", "DUPLICATE", "WORKSFORME"};

        private readonly string[] _fieldsToIncludeInComments = {"creator", "text"};

        public BugzillaRepository(string url)
        {
            Url = url;
        }

        public List<string> GetProducts()
        {
            const string method = "Product.get";
            var parameters = new object[]
            {
                new
                {
                    type = "accessible",
                    include_fields = new[] {"name"}
                }
            };
            var products = MakeRequest(method, parameters)["products"];

            return products.Select(product => product["name"].ToString())
                .OrderBy(str => str)
                .ToList();
        }

        public Task<List<string>> GetProductsAsync() => Task.Factory.StartNew(GetProducts);

        public List<BugReport> GetCompleteReports(string product, DateTime creationDate, int maxResults)
        {
            var reports = SearchRepository(product, creationDate, maxResults);
            var bugIds = reports.Select(report => int.Parse(report.reportid)).ToList();
            var comments = GetComments(bugIds);
            var histories = GetHistory(bugIds);

            AttachComments(reports, comments);
            AttachHistoryInfo(reports, histories);

            return reports;
        }

        public Task<List<BugReport>> GetCompleteReportsAsync(string product, DateTime creationDate, int maxResults)
        {
            return Task.Factory.StartNew(() => GetCompleteReports(product, creationDate, maxResults));
        }

        private List<BugReport> SearchRepository(string product, DateTime creationDate, int maxResults)
        {
            const string method = "Bug.search";
            var parameters = new[]
            {
                new
                {
                    creation_time = creationDate,
                    limit = maxResults,
                    resolution = _resolutionsToIncludeInSearch,
                    include_fields = _fieldsToIncludeInSearch,
                    product = product,
                }
            };

            var bugs = MakeRequest(method, parameters)["bugs"] ??
                       new JObject();

            return bugs.Select(GetReportMetaDataFromJson).ToList();
        }

        private static BugReport GetReportMetaDataFromJson(JToken bug)
        {
            return new BugReport
            {
                reportid = bug["id"]?.ToString(),
                assigned = bug["assigned_to"]?.ToString(),
                created = bug["creation_time"]?.ToString(),
                priority = bug["priority"]?.ToString(),
                component = bug["component"]?.ToString(),
                reporter = bug["creator"]?.ToString(),
                duplicates = bug["dupe_of"]?.ToString(),
                bug_text = string.Join(" ",
                    bug["bug_text"]?.ToString(),
                    string.Join(" ", bug["keywords"]?.Select(keyword => keyword.ToString()) ?? new List<string>())),
            };
        }

        private JToken GetComments(IEnumerable<int> bugIds)
        {
            const string method = "Bug.comments";
            var parameters = new[]
            {
                new
                {
                    ids = bugIds.ToArray(),
                    include_fields = _fieldsToIncludeInComments,
                }
            };
            var comments = MakeRequest(method, parameters)["bugs"] ?? new JObject();

            return comments;
        }

        private static void AttachComments(IEnumerable<BugReport> reports, JToken comments)
        {
            foreach (var report in reports)
            {
                var descriptionComment = comments[report.reportid]?["comments"]?[0];
                if (descriptionComment == null) continue;

                report.bug_text += $" {descriptionComment["text"]}";
                report.firstresponder = descriptionComment["creator"].ToString();
            }
        }

        private JToken GetHistory(IEnumerable<int> bugIds)
        {
            const string method = "Bug.history";
            var parameters = new[]
            {
                new
                {
                    ids = bugIds,
                }
            };
            return MakeRequest(method, parameters)?["bugs"] ?? new JObject();
        }

        private static void AttachHistoryInfo(IEnumerable<BugReport> reports, JToken histories)
        {
            foreach (var report in reports)
            {
                var history =
                    histories.SingleOrDefault(hist => hist["id"].ToString().Equals(report.reportid))?["history"];
                if (history == null) continue;

                var changes = history.SelectMany(hist => hist["changes"]).ToList();
                report.pathgroup = GetPathGroup(changes);

                report.resolver = GetLastChanger(history, "RESOLVED");
                report.fixedby = GetLastChanger(history, "FIXED");
            }
        }

        private static string GetLastChanger(JToken history, string statusAdded)
        {
            return history.LastOrDefault(hist => ChangesContainsStatus(hist["changes"], statusAdded))?["who"]?.ToString() ??
                    string.Empty;
        }

        private static bool ChangesContainsStatus(JToken changes, string value)
        {
            var hasValue = changes.Where(change => change["added"].ToString() == value).Take(1).Any();
            return hasValue;
        }

        private static string GetPathGroup(IEnumerable<JToken> changes)
        {
            var statuses =
                changes.Where(
                    change =>
                        change["field_name"].ToString() == "status" ||
                        change["field_name"].ToString() == "resolution")
                    .Select(change => change["added"].ToString()).ToList();
            var paths = statuses.Select(GetPathCharacter).ToArray();
            return $"N{string.Join("", paths)}";
        }

        private static string GetPathCharacter(string status)
        {
            switch (status)
            {
                case "NEW":
                    return "N";
                case "ASSIGNED":
                    return "A";
                case "FIXED":
                    return "F";
                case "VERIFIED":
                    return "V";
                case "CLOSED":
                    return "C";
                case "REOPENED":
                    return "R";
                case "WORKSFORME":
                    return "M";
                case "WONTFIX":
                    return "X";
                case "RESOLVED":
                case "DUPLICATE":
                case "UNCONFIRMED":
                case "INVALID":
                case "STARTED":
                case "INCOMPLETE":
                case "":
                    return string.Empty;
                default:
                    return string.Empty;
            }
        }

        private JToken MakeRequest(string method, object parameters)
        {
            var requestUrl = $"{Url}?method={method}&params={parameters.ToJson()}";
            var request = (HttpWebRequest) WebRequest.Create(requestUrl);
            request.Method = WebRequestMethods.Http.Get;

            var response = GetResponse(request);

            return JsonConvert.DeserializeObject<JObject>(response)["result"] ?? new JObject();
        }

        private static string GetResponse(WebRequest request)
        {
            using (var response = request.GetResponse())
            using (var stream = response.GetResponseStream())
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
