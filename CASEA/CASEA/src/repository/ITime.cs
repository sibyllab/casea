﻿/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CookComputing.XmlRpc;

namespace Cyberdyne.src.bugreports.bugzilla {

    public struct TimeValue {
        public DateTime db_time;
        public DateTime web_time;
        public DateTime web_time_utc;
        public string tz_name;
        public string tz_short_name;
        public string tz_offset;
    }

    public interface ITime : IXmlRpcProxy
    {
        [XmlRpcMethod("Bugzilla.time")]
        TimeValue Time();
    }
}
