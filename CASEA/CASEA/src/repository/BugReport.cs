﻿/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace CASEA.src.repository
{
    public class BugReport : IBug
    {
        private XElement Report;
        public XElement report
        {
            get
            {
                return Report;
            }
            set
            {
                Report = value;
            }
        }

        private XElement Reportid;
        public string reportid
        {
            get
            {
                return Reportid.Value;
            }
            set
            {
                Reportid = new XElement("reportid", value);
            }
        }

        private XElement Pathgroup;
        public string pathgroup
        {
            get
            {
                return Pathgroup.Value;
            }
            set
            {
                Pathgroup = new XElement("pathgroup", value);
            }
        }

        private XElement Assigned;
        public string assigned
        {
            get
            {
                return Assigned.Value;
            }
            set
            {
                Assigned = new XElement("assigned", value);
            }
        }

        private XElement Fixedby;
        public string fixedby
        {
            get
            {
                return Fixedby.Value;
            }
            set
            {
                Fixedby = new XElement("fixedby", value);
            }
        }

        private XElement Resolver;
        public string resolver
        {
            get
            {
                return Resolver.Value;
            }
            set
            {
                Resolver = new XElement("resolver", value);
            }
        }

        private XElement Firstresponder;
        public string firstresponder
        {
            get
            {
                return Firstresponder.Value;
            }
            set
            {
                Firstresponder = new XElement("firstresponder", value);
            }
        }

        private XElement Bug_Text;
        public string bug_text
        {
            get
            {
                return Bug_Text.Value;
            }
            set
            {
                Bug_Text = new XElement("bug_text", Sanitize(value));
            }
        }

        private XElement Created;
        public string created
        {
            get
            {
                return Created.Value;
            }
            set
            {
                Created = new XElement("created", value);
            }
        }

        private XElement Component;
        public string component
        {
            get
            {
                return Component.Value;
            }
            set
            {
                Component = new XElement("component", value);
            }
        }

        private XElement Priority;
        public string priority
        {
            get
            {
                return Priority.Value;
            }
            set
            {
                Priority = new XElement("priority", value);
            }
        }

        private XElement Reporter;
        public string reporter
        {
            get
            {
                return Reporter.Value;
            }
            set
            {
                Reporter = new XElement("reporter", value);
            }
        }

        private XElement Duplicates;
        public string duplicates
        {
            get
            {
                return Duplicates.Value;
            }
            set
            {
                Duplicates = new XElement("duplicates", value);
            }
        }

        private XElement History;
        public XElement history
        {
            get
            {
                return History;
            }
            set
            {
                History = value;
            }
        }

        public void initReport()
        {
            report = null;
            reportid = "--";
            pathgroup = "--";
            assigned = "--";
            fixedby = "--";
            resolver = "--";
            firstresponder = "--";
            bug_text = "--";
            created = "--";
            component = "--";
            priority = "--";
            reporter = "--";
            duplicates = "--";
            history = null;
        }

        public void clearReport()
        {
            report = null;
            reportid = "--";
            pathgroup = "--";
            assigned = "--";
            fixedby = "--";
            resolver = "--";
            firstresponder = "--";
            bug_text = "--";
            created = "--";
            component = "--";
            priority = "--";
            reporter = "--";
            duplicates = "--";
            history = null;
        }

        public void populateReport()
        {
            report = new XElement("bugreport" ,  
            Reportid,
            Pathgroup,
            Assigned,
            Fixedby,
            Resolver,
            Firstresponder,
            Bug_Text,
            Created,
            Component,
            Priority,
            Reporter,
            Duplicates,
            history);
        }

        /// <summary>
        /// Removes invalid characters in XML.
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        private static string Sanitize(string txt)
        {
            string regex = "[\x00-\x08\x0B\x0C\x0E-\x1F\x26]";
            return Regex.Replace(txt, regex, "", RegexOptions.Compiled);
        }
    }
}
