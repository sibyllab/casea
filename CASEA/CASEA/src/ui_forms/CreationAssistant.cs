/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CASEA.src.classifier;
using CASEA.src.save_structure;
using CASEA.src.functions;
using System.Diagnostics;
using CASEA.src.configuration;

namespace CASEA.src.ui_forms
{
    // Holds the hidden and non hidden heuristics elements
    public struct heuristicsFields
    {
        public Label path;
        public Label percentage;
        public ComboBox selectionBox;
    }

    // TODO: This should be moved so model classes don't depend on UI
    public enum RecommenderType {Developer, Component };

    public partial class CreationAssistant : Form
    {
        public heuristicsFields[] heuristicsElements;
        public SavedRecommender loadedInformation;
        public MLAlgorithm classifier;
        public string currentFilePath;
        public List<string> status;
        public Stopwatch stopwatch;
        public int attempt;
        public List<HeuristicEnum> currentHeuristics;
        public string savePath = "";
        public bool isBuilt = false;
        public bool isLoadedConfig = false;        
        public int NUM_SAMPLE_REPORTS = 10;

        // Pre-filtered features
        Input trainingFeatures, validationFeatures;

        // For testing, explictedly set recommender type
        RecommenderType recType = RecommenderType.Developer;

        public CreationAssistant(string filePath)
        {
            InitializeComponent();

            attempt = 0;

            // Store this recommenders filePath
            currentFilePath = filePath;
            //Default save path.
            savePath = filePath;

            // Will hold and hide or unhide all of the objects. Add all elements to the list;
            heuristicsElements = new heuristicsFields[10];
            heuristicsElements[0].path = pathLabel1; heuristicsElements[0].percentage = percentageLabel1; heuristicsElements[0].selectionBox = heuristicsBox1;
            heuristicsElements[1].path = pathLabel2; heuristicsElements[1].percentage = percentageLabel2; heuristicsElements[1].selectionBox = heuristicsBox2;
            heuristicsElements[2].path = pathLabel3; heuristicsElements[2].percentage = percentageLabel3; heuristicsElements[2].selectionBox = heuristicsBox3;
            heuristicsElements[3].path = pathLabel4; heuristicsElements[3].percentage = percentageLabel4; heuristicsElements[3].selectionBox = heuristicsBox4;
            heuristicsElements[4].path = pathLabel5; heuristicsElements[4].percentage = percentageLabel5; heuristicsElements[4].selectionBox = heuristicsBox5;
            heuristicsElements[5].path = pathLabel6; heuristicsElements[5].percentage = percentageLabel6; heuristicsElements[5].selectionBox = heuristicsBox6;
            heuristicsElements[6].path = pathLabel7; heuristicsElements[6].percentage = percentageLabel7; heuristicsElements[6].selectionBox = heuristicsBox7;
            heuristicsElements[7].path = pathLabel8; heuristicsElements[7].percentage = percentageLabel8; heuristicsElements[7].selectionBox = heuristicsBox8;
            heuristicsElements[8].path = pathLabel9; heuristicsElements[8].percentage = percentageLabel9; heuristicsElements[8].selectionBox = heuristicsBox9;
            heuristicsElements[9].path = pathLabel10; heuristicsElements[9].percentage = percentageLabel10; heuristicsElements[9].selectionBox = heuristicsBox10;

            // Load recommender and deserialize
            loadedInformation = Serializer.DeserializeRecommender(filePath);

            object[] temp = new object[loadedInformation.TDS.paths.pathNames.Length < 10 ?
                loadedInformation.TDS.paths.pathNames.Length : 10];

            // Init heuristic dropdowns
            otherHeuristicBox.DataSource = Enum.GetValues(typeof(HeuristicEnum));
            heuristicsBox1.DataSource = Enum.GetValues(typeof(HeuristicEnum));
            heuristicsBox2.DataSource = Enum.GetValues(typeof(HeuristicEnum));
            heuristicsBox3.DataSource = Enum.GetValues(typeof(HeuristicEnum));
            heuristicsBox4.DataSource = Enum.GetValues(typeof(HeuristicEnum));
            heuristicsBox5.DataSource = Enum.GetValues(typeof(HeuristicEnum));
            heuristicsBox6.DataSource = Enum.GetValues(typeof(HeuristicEnum));
            heuristicsBox7.DataSource = Enum.GetValues(typeof(HeuristicEnum));
            heuristicsBox8.DataSource = Enum.GetValues(typeof(HeuristicEnum));
            heuristicsBox9.DataSource = Enum.GetValues(typeof(HeuristicEnum));
            heuristicsBox10.DataSource = Enum.GetValues(typeof(HeuristicEnum));

            // Set heuristic labels
            int numOfPaths = loadedInformation.TDS.paths.pathDistribution.Sum();
            for (int i = 0; i < temp.Length; i++)
            {
                heuristicsElements[i].path.Text = loadedInformation.TDS.paths.pathNames[i];
                heuristicsElements[i].percentage.Text = 
                    String.Format("{0:0.#}", loadedInformation.TDS.paths.pathDistribution[i] / (double)numOfPaths * 100) + " %";
            }       

            for (int i = 1; i < temp.Length + 1; i++)
            {
                    temp[i - 1] = i.ToString();
            }
            this.numOfHeuristicsBox.Items.AddRange(temp);
            this.numOfHeuristicsBox.Text = (loadedInformation.TDS.paths.pathNames.Length < 5 ?
               loadedInformation.TDS.paths.pathNames.Length : 5).ToString();

            //// Init ML algorithm selection
            mlAlgorithmSelect.DataSource = Enum.GetValues(typeof(MLAlgorithm.Types));

            // Init recommender selection
            recommenderSelectionComboBox.DataSource = Enum.GetValues(typeof(RecommenderType));

            // Save training and validation data so filtering doesn't lose data we may use in the future
            trainingFeatures = new Input(loadedInformation.TDS.input);
            validationFeatures = new Input(loadedInformation.VDS.input);
            
            // Load the configuration
            loadConfiguration(loadedInformation.currentConfiguration);
            mlAlgorithmSelect.SelectedItem = MLAlgorithm.Types.Auto;
            setHistoryLabelVisibility();
            status = new List<string>();
            stopwatch = new Stopwatch();

            // Build all charts
            buildFrequencyChart();
            buildClassesTextBox(); // TODO Allow user to select type
            buildHeuristicsChart();

            // build # of recommendations combobox (go up to 50 by 5's) and default to 10
            cbShowRecommendations.DataSource = new int[] { 5, 10, 15, 20, 25, 30, 35, 40, 45, 50 };
            cbShowRecommendations.SelectedIndex = cbShowRecommendations.Items.IndexOf(10);

            Refresh();

            numOfHeuristicsBox.SelectedItem = loadedInformation.currentConfiguration.heuristics.Length;
            showHeuristicsFields(int.Parse(numOfHeuristicsBox.Text));

            if (loadedInformation.history.Count > 0)
            {
                isLoadedConfig = true;
            }
            else
            {
                isLoadedConfig = false;
            }

            isBuilt = true;
        }

        // Loads a prior configuration
        public void loadConfiguration(Configuration config)
        {
            attempt = config.attempt;

            // Set recommender type
            recommenderSelectionComboBox.SelectedItem = config.confType;

            // Load saved heuristics values
            for (int i = 0; i < config.heuristics.Length; i++)
            {
                switch (config.heuristics[i])
                {
                    case HeuristicEnum.AssignedTo:
                        heuristicsElements[i].selectionBox.SelectedItem = HeuristicEnum.AssignedTo;
                        break;
                    case HeuristicEnum.FirstResponder:
                        heuristicsElements[i].selectionBox.SelectedItem = HeuristicEnum.FirstResponder;
                        break;
                    case HeuristicEnum.FixedBy:
                        heuristicsElements[i].selectionBox.SelectedItem = HeuristicEnum.FixedBy;
                        break;
                    case HeuristicEnum.Reporter:
                        heuristicsElements[i].selectionBox.SelectedItem = HeuristicEnum.Reporter;
                        break;
                    case HeuristicEnum.Resolver:
                        heuristicsElements[i].selectionBox.SelectedItem = HeuristicEnum.Resolver;
                        break;
                    case HeuristicEnum.Unused:
                        heuristicsElements[i].selectionBox.SelectedItem = HeuristicEnum.Unused;
                        break;
                }
            }

            // Load other heuristic
            switch (config.otherHeuristic)
            {
                case HeuristicEnum.AssignedTo:
                    otherHeuristicBox.SelectedItem = HeuristicEnum.AssignedTo;
                    break;
                case HeuristicEnum.FirstResponder:
                    otherHeuristicBox.SelectedItem = HeuristicEnum.FirstResponder;
                    break;
                case HeuristicEnum.FixedBy:
                    otherHeuristicBox.SelectedItem = HeuristicEnum.FixedBy;
                    break;
                case HeuristicEnum.Reporter:
                    otherHeuristicBox.SelectedItem = HeuristicEnum.Reporter;
                    break;
                case HeuristicEnum.Resolver:
                    otherHeuristicBox.SelectedItem = HeuristicEnum.Resolver;
                    break;
                case HeuristicEnum.Unused:
                    otherHeuristicBox.SelectedItem = HeuristicEnum.Unused;
                    break;
            }

            // Load frequencyCutoff
            frequencySelectBar.Value = config.frequencyCutoff;          
            
            // Set num of heuristics
            numOfHeuristicsBox.SelectedItem = config.heuristics.Length.ToString();

            // Select the appropriate algorithm.
            mlAlgorithmSelect.SelectedItem = config.algorithm;
        }

        public void showHeuristicsFields(int x)
        {
            // Hide all of the elements
            for (int i = 0; i < 10; i++)
            {
                heuristicsElements[i].path.Visible = false;
                heuristicsElements[i].percentage.Visible = false;
                heuristicsElements[i].selectionBox.Visible = false;
            }

            // Make the selected number of elements visible
            for (int i = 0; i < x; i++)
            {
                heuristicsElements[i].path.Visible = true;
                heuristicsElements[i].percentage.Visible = true;
                heuristicsElements[i].selectionBox.Visible = true;
            }
        }

        public void appendToStatus(string text)
        {
            status.Add(text);
            string[] tempArray = status.ToArray<string>();
            statusBox.Lines = tempArray;
            statusBox.SelectionStart = statusBox.Text.Length;
            statusBox.ScrollToCaret();
            Refresh();
        }

        // Construct the frequencyChart. This method need only be called on load and after developer changes.
        private void buildFrequencyChart()
        {
            // Set chart and axis titles
            switch (recType)
            {
                case RecommenderType.Component:
                    frequencyChart.Titles[0].Text = "Reports by Component";
                    frequencyChart.ChartAreas[0].AxisY.Title = "Report by Component";                    
                    break;
                case RecommenderType.Developer:
                default:
                    frequencyChart.Titles[0].Text = "Reports Solved by Developers";
                    frequencyChart.ChartAreas[0].AxisY.Title = "Reports Solved by Developers";                    
                    break;
            }
                        
            // Clear points from the chart
            frequencyChart.Series["frequency"].Points.Clear();
            frequencyChart.Series["frequencyCutoff"].Points.Clear();

            // Add points from a sorted (greatest to least) array
            int[] freqVals;
            string[] labels;
            switch (recType)
            {
                case RecommenderType.Component:
                    freqVals = loadedInformation.TDS.componentDistribution();
                    labels = loadedInformation.TDS.getComponentNames();
                    break;
                case RecommenderType.Developer:
                default:
                    freqVals = loadedInformation.TDS.reportDistribution();
                    labels = loadedInformation.TDS.getDeveloperNames();
                    break;
            }

            List<KeyValuePair<string, int>> freqData = new List<KeyValuePair<string, int>>();
            for (int iii = 0; iii < freqVals.Length; iii++)
            {
                freqData.Add(new KeyValuePair<string, int>(labels[iii], freqVals[iii]));
            }

            freqData.Sort(delegate (KeyValuePair<string, int> pair1, KeyValuePair<string, int> pair2)
            {
                return pair2.Value.CompareTo(pair1.Value);
            });

            ///Array.Sort(freqVals, (x, y) => y.CompareTo(x));

            // Set maximum freqency cutoff to the highest developer.
            frequencySelectBar.Maximum = freqVals.Max();

            // Estimate cutoff
            frequencySelectBar.Value = (int)freqVals.Average();

            // Populate the table.
            for (int i = 0; i < freqData.Count; i++)
            {
                // Instances with frequency of 1 are not included.
                // Assumption: freqData is sorted in descending order
                if (freqData[i].Value == 1)
                {
                    break;
                }
                frequencyChart.Series["frequency"].Points.AddXY(freqData[i].Key, freqData[i].Value);
                frequencyChart.Series["frequencyCutoff"].Points.Add(frequencySelectBar.Value);
            }
        }

        // Construct the classes textbox
        private void buildClassesTextBox()
        {
            classesBox.Text = "";
            string[] classArray = { };
            int[] classFrequency = { };
            switch (recType)
            {
                case RecommenderType.Developer:
                    classArray = loadedInformation.TDS.output.developerInstances.names;
                    classFrequency = loadedInformation.TDS.reportDistribution(); break;
                case RecommenderType.Component:
                    classArray = loadedInformation.TDS.components.names;
                    classFrequency = loadedInformation.TDS.components.freqs; break;
            }
            
            Dictionary<string, int> classInfo = 
                classArray.Zip(classFrequency, (str, nt) => new { str, nt }).ToDictionary(item => item.str, item => item.nt);

            classInfo = (from entry in classInfo orderby entry.Value descending select entry).TakeWhile(className => className.Value > 1).ToDictionary(pair => pair.Key, pair => pair.Value);

            string[] lines = new string[classInfo.Count + 1];

            switch (recType)
            {
                case RecommenderType.Developer:
                    lines[0] = "Reports\t: Developer\n=========================="; break;
                case RecommenderType.Component:
                    lines[0] = "Reports\t: Component\n=========================="; break;
            }

            int index = 1;
            foreach (var c in classInfo)
            {
                lines[index] = c.Value + "\t: " + c.Key;
                index++;
            }

            classesBox.AppendText(lines[0] + "\n");

            for(int iii = 1; iii < lines.Length; iii++)
            {
                string frequency = lines[iii].Split(':')[0];
                classesBox.SelectionColor = Convert.ToInt32(frequency.Trim()) >= frequencySelectBar.Value ? Color.Green : Color.Red;
                classesBox.AppendText(lines[iii] + "\n");
            }
        }

        // Construct the Heuristics Chart
        private void buildHeuristicsChart()
        {
            // Clear chart
            heuristicsChart.Series["categoryChart"].Points.Clear();

            // vars
            double sum = 0, total = 0;
            string[] categories = { };
            int[] distribution = { };
            int numItems = 0;

            switch (recType)
            {
                case RecommenderType.Developer: // report groups
                    total = loadedInformation.TDS.paths.pathDistribution.Sum();
                    categories = loadedInformation.TDS.paths.pathNames;
                    distribution = loadedInformation.TDS.paths.pathDistribution;
                    numItems = int.Parse(numOfHeuristicsBox.Text);
                    break;
                case RecommenderType.Component: // component groups
                    total = loadedInformation.TDS.components.freqs.Sum();
                    categories = loadedInformation.TDS.components.names;
                    distribution = loadedInformation.TDS.components.freqs;
                    numItems = loadedInformation.TDS.components.names.Length < 8 ? loadedInformation.TDS.components.names.Length : 8;
                    break;
            }

            // ensure sorting of graph
            List<Tuple<string, int>> graphElements = Enumerable.Zip(categories, distribution, (x, y) => new Tuple<string, int>(x, y)).ToList();
            graphElements.Sort((x, y) => y.Item2.CompareTo(x.Item2));

            // Set an element of the chart
            foreach (Tuple<string, int> element in graphElements.Take(numItems))
            {
                string category = element.Item1;
                double percentage = element.Item2 / (double)total * 100.0;
                sum += percentage;
                heuristicsChart.Series["categoryChart"].Points.Add(percentage).LegendText = 
                    (string.Format("{0:0.#}", percentage) + " % - " + category);
            }

            // Set the very last element
            if (100 - sum > 0)
            {
                heuristicsChart.Series["categoryChart"].Points.Add(100 - sum).LegendText =
(string.Format("{0:0.#}", 100 - sum) + " % - Other");
            }
        }

        // When numOfHeuristics is changed.
        private void numOfHeuristicsBox_ValueMemberChanged(object sender, EventArgs e)
        {
            // Set heuristics when the value is changed
            showHeuristicsFields(int.Parse(numOfHeuristicsBox.Text));           

            // Reparse heuristics information and update relevant fields.
            // Arguments
            string[] names = new string[int.Parse(numOfHeuristicsBox.Text)];
            HeuristicEnum[] selectedHeuristics = new HeuristicEnum[int.Parse(numOfHeuristicsBox.Text)];

            for (int i = 0; i < names.Length && i < loadedInformation.TDS.paths.pathNames.Length; i++)
            {
                names[i] = loadedInformation.TDS.paths.pathNames[i];
                selectedHeuristics[i] = (HeuristicEnum)heuristicsElements[i].selectionBox.SelectedItem;
            }

            // Update the savedRecommender info
            loadedInformation.TDS.updateOutputs(names, selectedHeuristics, (HeuristicEnum)otherHeuristicBox.SelectedItem, loadedInformation.TDS, System.IO.Path.GetDirectoryName(currentFilePath));

            // build other fields
            buildFrequencyChart();
            buildClassesTextBox(); // TODO Allow user to select recommender type 
            buildHeuristicsChart();

            // Show/Hide "Other" heuristics dropdown
            if (int.Parse(numOfHeuristicsBox.Text) < loadedInformation.TDS.paths.pathNames.Length)
            {
                otherLabel.Show();
                otherHeuristicBox.Show();
            }
            else
            {
                otherLabel.Hide();
                otherHeuristicBox.Hide();
            }


            isBuilt = false;
            isLoadedConfig = false;
        }

        // When the frequency slider is changed
        private void frequencySelectBar_ValueChanged(object sender, EventArgs e)
        {
            loadedInformation.currentConfiguration.frequencyCutoff = frequencySelectBar.Value;

            // Reflect FC change in the label
            fcValue.Text = frequencySelectBar.Value.ToString();

            int numDataPoints = 0;
            switch (recType)
            {
                case RecommenderType.Developer:
                    numDataPoints = loadedInformation.TDS.output.developerInstances.names.Length;
                    break;                    
                case RecommenderType.Component:
                    numDataPoints = loadedInformation.TDS.components.names.Length;
                    break;                    
            }

            // Update the series in  the graph
            frequencyChart.Series["frequencyCutoff"].Points.Clear();
            for (int i = 0; i < numDataPoints; i++)
            {
                frequencyChart.Series["frequencyCutoff"].Points.Add(frequencySelectBar.Value);
            }

            buildClassesTextBox();

            isBuilt = false;
            isLoadedConfig = false;
        }

        // Run anytime that the text in the heuristics boxes is changed
        private void heuristicBox_TextChanged(object sender, EventArgs e)
        {
            // Arguments
            string[] names = new string[int.Parse(numOfHeuristicsBox.Text)];
            HeuristicEnum[] selectedHeuristics = new HeuristicEnum[int.Parse(numOfHeuristicsBox.Text)];

            for (int i = 0; i < names.Length && i < loadedInformation.TDS.paths.pathNames.Length; i++)
            {
                names[i] = loadedInformation.TDS.paths.pathNames[i];
                
                if(heuristicsElements[i].selectionBox.SelectedItem == null)
                    selectedHeuristics[i] = (HeuristicEnum)heuristicsElements[i].selectionBox.Items[0];
                else
                selectedHeuristics[i] = (HeuristicEnum)heuristicsElements[i].selectionBox.SelectedItem;
            }

            if (otherHeuristicBox.SelectedItem == null)
                otherHeuristicBox.SelectedItem = otherHeuristicBox.Items[0];            

            // Update the savedRecommender info
            loadedInformation.TDS.updateOutputs(names, selectedHeuristics, (HeuristicEnum)otherHeuristicBox.SelectedItem, loadedInformation.TDS, System.IO.Path.GetDirectoryName(currentFilePath));

            // Rebuild elements which may have changed
            buildFrequencyChart();
            buildClassesTextBox(); // Allow user to select recommender type

            isBuilt = false;
            isLoadedConfig = false;
        }

        // History labels
        private void historyLabel_Click(object sender, EventArgs e)
        {
            // Figure out which history element to pull.
            Configuration oldConfiguration = new Configuration();
            switch (((Label)sender).Name.ToString())
            {
                case "historyLabel1":
                    oldConfiguration = loadedInformation.history[0];
                    break;
                case "historyLabel2":
                    oldConfiguration = loadedInformation.history[1];
                    break;
                case "historyLabel3":
                    oldConfiguration = loadedInformation.history[2];
                    break;
                case "historyLabel4":
                    oldConfiguration = loadedInformation.history[3];
                    break;
                case "historyLabel5":
                    oldConfiguration = loadedInformation.history[4];
                    break;
                case "historyLabel6":
                    oldConfiguration = loadedInformation.history[5];
                    break;
                case "historyLabel7":
                    oldConfiguration = loadedInformation.history[6];
                    break;
                case "historyLabel8":
                    oldConfiguration = loadedInformation.history[7];
                    break;
                case "historyLabel9":
                    oldConfiguration = loadedInformation.history[8];
                    break;
                case "historyLabel10":
                    oldConfiguration = loadedInformation.history[9];
                    break;
            }
            loadConfiguration(oldConfiguration);
            tabControl1.SelectedIndex = 0;
        }

        private void historyLabel_MouseEnter(object sender, EventArgs e)
        {
            ((Label)sender).BackColor = DefaultForeColor;
            ((Label)sender).ForeColor = Color.White;
        }

        private void historyLabel_MouseLeave(object sender, EventArgs e)
        {
            ((Label)sender).BackColor = Color.White;
            ((Label)sender).ForeColor = DefaultForeColor;
        }

        private void setHistoryLabelVisibility()
        {
            Label[] historyLabels = new Label[10];
            historyLabels[0] = historyLabel1; historyLabels[1] = historyLabel2; historyLabels[2] = historyLabel3;
            historyLabels[3] = historyLabel4; historyLabels[4] = historyLabel5; historyLabels[5] = historyLabel6;
            historyLabels[6] = historyLabel7; historyLabels[7] = historyLabel8; historyLabels[8] = historyLabel9;
            historyLabels[9] = historyLabel10;

            for (int i = 0; i < loadedInformation.history.Count; i++)
            {
                historyLabels[i].Text = $@"History {i + 1} ({loadedInformation.history[i].confType}, {loadedInformation.history[i].algorithm})";
                historyLabels[i].Visible = true;
            }

            for (int i = loadedInformation.history.Count; i < historyLabels.Length; i++)
            {
                historyLabels[i].Visible = false;
            }
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void buildRecommenders()
        {
            attempt++;
            loadedInformation.attempt = attempt;
            SurveyPage.attempt = attempt;
            button1.Enabled = false;
            tabControl1.SelectedIndex = 1;
            MLAlgorithm.Types algorithm;

            // Clear sample recommendations
            sampleRecommendations.Clear();

            if ((MLAlgorithm.Types)mlAlgorithmSelect.SelectedItem == MLAlgorithm.Types.Auto)
            {
                autoAlgorithm();
                return;
            }
            else
            {
                algorithm = (MLAlgorithm.Types)mlAlgorithmSelect.SelectedItem;
                loadedInformation.algorithm = algorithm;
            }

            if (isLoadedConfig)
            {
                appendToStatus("Building " + loadedInformation.history.Count.ToString() + " recommender(s).");

                if (loadedInformation.history.Count > 5)
                {
                    appendToStatus("This could take a while...");
                }

                for (int iii = 0; iii < loadedInformation.history.Count; iii++)
                {
                    loadConfiguration(loadedInformation.history[iii]);
                    appendToStatus("Building recommender " + (iii + 1) + "...");
                    doWork(loadedInformation);
                }
            }
            else
            {
                appendToStatus("Building recommender...");
                doWork(loadedInformation);                
                loadedInformation.newRecommender(frequencySelectBar.Value, currentHeuristics, (HeuristicEnum)otherHeuristicBox.SelectedItem, algorithm, recType);
                setHistoryLabelVisibility();
            }
        }

        private void autoAlgorithm()
        {
            attempt++;
            loadedInformation.attempt = attempt;
            Dictionary<MLAlgorithm.Types, MLAlgorithm> testRecommenders = new Dictionary<MLAlgorithm.Types, MLAlgorithm>();
            MLAlgorithm.Types algorithm = MLAlgorithm.Types.SVM;

            stopwatch.Reset(); stopwatch.Start();
            foreach (MLAlgorithm.Types comboBoxAlgorithm in mlAlgorithmSelect.Items)
            {
                mlAlgorithmSelect.SelectedItem = comboBoxAlgorithm;

                if ((MLAlgorithm.Types)mlAlgorithmSelect.SelectedItem == MLAlgorithm.Types.Auto)
                {
                    continue;
                }
                else
                {
                    algorithm = (MLAlgorithm.Types)mlAlgorithmSelect.SelectedItem;
                    SavedRecommender tempInformation = Serializer.DeserializeRecommender(currentFilePath);
                    tempInformation.algorithm = algorithm;

                    if (isLoadedConfig)
                    {
                        appendToStatus("Building " + tempInformation.history.Count.ToString() + " recommender(s).");

                        if (tempInformation.history.Count > 5)
                        {
                            appendToStatus("This could take a while...");
                        }

                        for (int iii = 0; iii < tempInformation.history.Count; iii++)
                        {
                            loadConfiguration(tempInformation.history[iii]);
                            appendToStatus("Building recommender " + (iii + 1) + "...");
                            testRecommenders.Add(algorithm, doAutoWork(tempInformation));
                        }
                    }
                    else
                    {
                        appendToStatus("Building recommender...");
                        testRecommenders.Add(algorithm, doAutoWork(tempInformation));
                    }
                }
            }

            // Retrieves the best algorithm based on the calculated results and generates the
            // corresponding recommender.
            
            loadedInformation.algorithm = getBestAlgorithm(testRecommenders);
            doAutoWork(loadedInformation);

            stopwatch.Stop();
            appendToStatus("Recommender completed in " + stopwatch.ElapsedMilliseconds / 1000 + " seconds.\n");

            // Add precision and recall points to the graph from runtime changes.
            // Top 5
            top5Chart.Series["Avg. Precision"].Points.Add(classifier.averageMetrics.precision[2] * 100);
            top5Chart.Series["Avg. Recall"].Points.Add(classifier.averageMetrics.recall[2] * 100);
            top5Chart.Series["Avg. F1"].Points.Add(classifier.averageMetrics.f1[2] * 100);

            // Top 3
            top3Chart.Series["Avg. Precision"].Points.Add(classifier.averageMetrics.precision[1] * 100);
            top3Chart.Series["Avg. Recall"].Points.Add(classifier.averageMetrics.recall[1] * 100);
            top3Chart.Series["Avg. F1"].Points.Add(classifier.averageMetrics.f1[1] * 100);

            // Top Recommendation.
            top1Chart.Series["Avg. Precision"].Points.Add(classifier.averageMetrics.precision[0] * 100);
            top1Chart.Series["Avg. Recall"].Points.Add(classifier.averageMetrics.recall[0] * 100);
            top1Chart.Series["Avg. F1"].Points.Add(classifier.averageMetrics.f1[0] * 100);

            GenerateSampleReports(NUM_SAMPLE_REPORTS, 5, recType);

            SurveyPage.recommenderCreated(frequencySelectBar.Value, currentHeuristics, (HeuristicEnum)otherHeuristicBox.SelectedItem, classifier.averageMetrics);

            button1.Enabled = true;
            tabControl1.SelectedIndex = 1;

            loadedInformation.newRecommender(frequencySelectBar.Value, currentHeuristics, (HeuristicEnum)otherHeuristicBox.SelectedItem, loadedInformation.algorithm, recType);
            setHistoryLabelVisibility();
            // Resets algorithm so it doesn't get left at the last index in the combo box.
            mlAlgorithmSelect.SelectedItem = MLAlgorithm.Types.Auto;
        }

        private MLAlgorithm.Types getBestAlgorithm(Dictionary<MLAlgorithm.Types, MLAlgorithm> algorithms)
        {
            MLAlgorithm.Types bestAlg = algorithms.First().Key;

            // Compares each subsequent value to bestAlg and updates it if there is a better one.
            for (int iii = 1; iii < algorithms.Keys.Count; iii++)
            {
                switch(algorithms[bestAlg].averageMetrics.compare(algorithms[(MLAlgorithm.Types)Enum.GetValues(typeof(MLAlgorithm.Types)).GetValue(iii)].averageMetrics))
                {
                    case 1:
                        continue;
                    case -1:
                    default:
                        MLAlgorithm bestAlgVal = algorithms[(MLAlgorithm.Types)Enum.GetValues(typeof(MLAlgorithm.Types)).GetValue(iii)];
                        bestAlg = algorithms.Where(keyPair => keyPair.Value.Equals(bestAlgVal)).Select(keyPair => keyPair.Key).First();
                        break;
                }
            }

            return bestAlg;
        }

        private MLAlgorithm doAutoWork(SavedRecommender input)
        {
            isBuilt = true;
            Refresh();

            currentHeuristics = new List<HeuristicEnum>();
            for (int i = 0; i < int.Parse(numOfHeuristicsBox.Text); i++)
            {
                currentHeuristics.Add((HeuristicEnum)heuristicsElements[i].selectionBox.SelectedItem);
            }

            SavedRecommender data = input;

            // Restore original features
            //data.TDS.input = trainingFeatures;
            //data.VDS.input = validationFeatures;

            // Filter data
            InputOutputPack filteredData = Parser.frequencyFiltering(recType, data.TDS, data.VDS, data.currentConfiguration.frequencyCutoff);

            // Compute TF-IDF for features
            Parser.calculateTfIdf(ref data.TDS.input.inputArray);
            Parser.calculateTfIdf(ref data.VDS.input.inputArray);

            classifier = MLAlgorithm.getClassifier(recType, input.algorithm, filteredData, this);

            return classifier;
        }

        // Start the background process as soon as the button is clicked
        private void buildRecommender_Click(object sender, EventArgs e)
        {
            buildRecommenders();
        }

        private void doWork(SavedRecommender input)
        {
            stopwatch.Reset(); stopwatch.Start();
            isBuilt = true;
            Refresh();

            currentHeuristics = new List<HeuristicEnum>();
            for (int i = 0; i < int.Parse(numOfHeuristicsBox.Text); i++)
            {
                currentHeuristics.Add((HeuristicEnum)heuristicsElements[i].selectionBox.SelectedItem);
            }

            setHistoryLabelVisibility();

            SavedRecommender data = input;
            
            // Restore original features
            data.TDS.input = trainingFeatures;
            data.VDS.input = validationFeatures;

            // Filter data
            InputOutputPack filteredData = Parser.frequencyFiltering(recType, data.TDS, data.VDS, data.currentConfiguration.frequencyCutoff);

            // Compute TF-IDF for features
            Parser.calculateTfIdf(ref data.TDS.input.inputArray);
            Parser.calculateTfIdf(ref data.VDS.input.inputArray);

            classifier = MLAlgorithm.getClassifier(recType, input.algorithm, filteredData, this);
            
            stopwatch.Stop();
            appendToStatus("Recommender completed in " + stopwatch.ElapsedMilliseconds / 1000 + " seconds.\n");

            // Add precision and recall points to the graph from runtime changes.
            // Top 5
            top5Chart.Series["Avg. Precision"].Points.Add(classifier.averageMetrics.precision[2] * 100);
            top5Chart.Series["Avg. Recall"].Points.Add(classifier.averageMetrics.recall[2] * 100);
            top5Chart.Series["Avg. F1"].Points.Add(classifier.averageMetrics.f1[2] * 100);

            // Top 3
            top3Chart.Series["Avg. Precision"].Points.Add(classifier.averageMetrics.precision[1] * 100);
            top3Chart.Series["Avg. Recall"].Points.Add(classifier.averageMetrics.recall[1] * 100);
            top3Chart.Series["Avg. F1"].Points.Add(classifier.averageMetrics.f1[1] * 100);

            // Top Recommendation.
            top1Chart.Series["Avg. Precision"].Points.Add(classifier.averageMetrics.precision[0] * 100);
            top1Chart.Series["Avg. Recall"].Points.Add(classifier.averageMetrics.recall[0] * 100);
            top1Chart.Series["Avg. F1"].Points.Add(classifier.averageMetrics.f1[0] * 100);

            GenerateSampleReports(NUM_SAMPLE_REPORTS, 5, recType);
            
            SurveyPage.recommenderCreated(frequencySelectBar.Value, currentHeuristics, (HeuristicEnum)otherHeuristicBox.SelectedItem, classifier.averageMetrics);

            button1.Enabled = true;
            tabControl1.SelectedIndex = 1;
        }

        private void GenerateSampleReports(int numReports, int numRecommendations, RecommenderType type)
        {
            // Show a sample of recommmendations
            List<string> recommendations = new List<string>();

            char[] trim = { ',', ' ' };

            Dictionary<string, string[][]> sample = new Dictionary<string, string[][]>();

            try
            {
                sample = classifier.getSampleRecommendations(NUM_SAMPLE_REPORTS, 5, recType);
            }
            catch
            {
                return;
            }

            foreach (var recommendation in sample)
            {
                // Output report id
                recommendations.Add(recommendation.Key);

                // Output expected recommendations
                string text = "\t Expected:";
                var expected = sample[recommendation.Key][0];
                foreach (string name in expected)
                {
                    text += name + ", ";
                }
                // Remove last comma
                text = text.TrimEnd(trim);
                recommendations.Add(text);

                // Output predictions
                text = "\t Predicted:";
                var predictions = sample[recommendation.Key][1].Take(5);
                foreach (string pred in predictions)
                {
                    text += pred + ", ";
                }
                // Remove last comma
                text = text.TrimEnd(trim);
                recommendations.Add(text);
            }


            string[] sampleText = recommendations.ToArray();
            sampleRecommendations.Lines = sampleText;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Only done with Save because the savePath includes the filename by default.
            SaveRecommender(System.IO.Path.GetDirectoryName(savePath));
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofdLoadLocation = new OpenFileDialog();
            ofdLoadLocation.InitialDirectory = System.IO.Path.GetFullPath("../../recommenders");
            ofdLoadLocation.Filter = "CASTR files (*.cstr)|*.cstr|All files (*.*)|*.*";

            if (ofdLoadLocation.ShowDialog() == DialogResult.OK)
            {
                CreationAssistant loaded = new CreationAssistant(ofdLoadLocation.FileName);
                loaded.Show();
                this.Close();
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbdSaveAsLocation = new FolderBrowserDialog();
            string srcPath = System.IO.Path.GetDirectoryName(savePath);
            string trainingDataFile = "TDS.xml";
            string validationDataFile = "VDS.xml";

            if (fbdSaveAsLocation.ShowDialog() == DialogResult.OK)
            {
                // Updates savePath so all future saves will go to this path.
                savePath = fbdSaveAsLocation.SelectedPath;

                // Copies TDS.xml and VDS.xml files to new save location.
                srcPath = System.IO.Path.Combine(srcPath, trainingDataFile);
                savePath = System.IO.Path.Combine(savePath, trainingDataFile);
                System.IO.File.Copy(srcPath, savePath, true);
                // Uses GetDirectoryName to trim off "TDS.xml".
                srcPath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(srcPath), validationDataFile);
                savePath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(savePath), validationDataFile);
                System.IO.File.Copy(srcPath, savePath, true);

                SaveRecommender(System.IO.Path.GetDirectoryName(savePath));
            }
        }

        private void SaveRecommender(string filePath)
        {
            if (!isBuilt)
            {
                buildRecommenders();
            }

            Serializer.SerializeRecommender(System.IO.Path.Combine(filePath, $@"recommender_data{loadedInformation.history.Count}.xml"), loadedInformation);
            classifier.saveMachine(filePath);
            if (System.IO.File.Exists(filePath + "/classifier.mchn"))
            {
                MessageBox.Show("Data saved successfully.", "Success");
            }
            else
            {
                foreach (string file in System.IO.Directory.GetFiles(filePath))
                {
                    System.IO.File.Delete(file);
                }

                System.IO.Directory.Delete(filePath);
            }
        }

        private void autoConfigButton_Click(object sender, EventArgs e)
        {
            AutomaticConfiguration config = new AutomaticConfiguration(recType, loadedInformation, System.IO.Path.GetDirectoryName(currentFilePath), this);

            // Switch tab
            tabControl1.SelectedIndex = 1;

            if (config.execute())
            {
                loadedInformation.currentConfiguration = config.configuration;
                loadConfiguration(loadedInformation.currentConfiguration);

                // Switch tab back
                tabControl1.SelectedIndex = 0;
            }

        }

        private void recommenderSelected(object sender, EventArgs e)
        {

            recType = (RecommenderType)recommenderSelectionComboBox.SelectedItem;
            switch (recType)
            {
                case RecommenderType.Component:
                    heuristicsPanelTop.Enabled = false;
                    break;
                case RecommenderType.Developer:
                    heuristicsPanelTop.Enabled = true;
                    break;
            }

            // Clear results chart
            foreach (var s in top1Chart.Series)
                s.Points.Clear();
            foreach (var s in top3Chart.Series)
                s.Points.Clear();
            foreach (var s in top5Chart.Series)
            {
                s.Points.Clear();
                s.IsVisibleInLegend = false;
            }
            
            buildFrequencyChart();
            buildClassesTextBox();
            buildHeuristicsChart();
            tabControl1.SelectedIndex = 0;
        }

        private void chkPrecision_CheckedChanged(object sender, EventArgs e)
        {
            top1Chart.Series["Avg. Precision"].Enabled = chkPrecision.Checked;
            top3Chart.Series["Avg. Precision"].Enabled = chkPrecision.Checked;
            top5Chart.Series["Avg. Precision"].Enabled = chkPrecision.Checked;
        }

        private void chkRecall_CheckedChanged(object sender, EventArgs e)
        {
            top1Chart.Series["Avg. Recall"].Enabled = chkRecall.Checked;
            top3Chart.Series["Avg. Recall"].Enabled = chkRecall.Checked;
            top5Chart.Series["Avg. Recall"].Enabled = chkRecall.Checked;
        }

        private void chkF1_CheckedChanged(object sender, EventArgs e)
        {
            top1Chart.Series["Avg. F1"].Enabled = chkF1.Checked;
            top3Chart.Series["Avg. F1"].Enabled = chkF1.Checked;
            top5Chart.Series["Avg. F1"].Enabled = chkF1.Checked;
        }

        private void chkAllData_CheckedChanged(object sender, EventArgs e)
        {
            chkPrecision.Checked = chkAllData.Checked;
            chkRecall.Checked = chkAllData.Checked;
            chkF1.Checked = chkAllData.Checked;

            chkPrecision.Enabled = !chkAllData.Checked;
            chkRecall.Enabled = !chkAllData.Checked;
            chkF1.Enabled = !chkAllData.Checked;
        }

        private void chkShowXLabels_CheckedChanged(object sender, EventArgs e)
        {
            frequencyChart.ChartAreas[0].AxisX.LabelStyle.Enabled = chkShowXLabels.Checked;
        }

        private void disableTooltipsToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            tooGraphs.Active = !disableTooltipsToolStripMenuItem.Checked;
        }

        private void btnApplyNumSamples_Click(object sender, EventArgs e)
        {
            int showRecommendationsValue;

            bool success = Int32.TryParse(cbShowRecommendations.Text, out showRecommendationsValue);

            if (success && showRecommendationsValue > 0)
            {
                NUM_SAMPLE_REPORTS = showRecommendationsValue;

                GenerateSampleReports(NUM_SAMPLE_REPORTS, 5, recType);
            }
            else
            {
                MessageBox.Show("Number of recommendations must be a valid integer that is greater than zero.");

                cbShowRecommendations.Text = NUM_SAMPLE_REPORTS.ToString();
            }
        }
    }
}