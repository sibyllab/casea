﻿namespace CASEA.src.ui_forms
{
    partial class SurveyPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.q1 = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.q2 = new System.Windows.Forms.GroupBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.q3 = new System.Windows.Forms.GroupBox();
            this.radioButton25 = new System.Windows.Forms.RadioButton();
            this.radioButton24 = new System.Windows.Forms.RadioButton();
            this.radioButton23 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.label17 = new System.Windows.Forms.Label();
            this.q12 = new System.Windows.Forms.TextBox();
            this.q7 = new System.Windows.Forms.GroupBox();
            this.radioButton13 = new System.Windows.Forms.RadioButton();
            this.radioButton14 = new System.Windows.Forms.RadioButton();
            this.radioButton26 = new System.Windows.Forms.RadioButton();
            this.radioButton27 = new System.Windows.Forms.RadioButton();
            this.radioButton28 = new System.Windows.Forms.RadioButton();
            this.q8 = new System.Windows.Forms.GroupBox();
            this.radioButton15 = new System.Windows.Forms.RadioButton();
            this.radioButton16 = new System.Windows.Forms.RadioButton();
            this.radioButton29 = new System.Windows.Forms.RadioButton();
            this.radioButton30 = new System.Windows.Forms.RadioButton();
            this.radioButton31 = new System.Windows.Forms.RadioButton();
            this.q9 = new System.Windows.Forms.GroupBox();
            this.radioButton17 = new System.Windows.Forms.RadioButton();
            this.radioButton18 = new System.Windows.Forms.RadioButton();
            this.radioButton32 = new System.Windows.Forms.RadioButton();
            this.radioButton33 = new System.Windows.Forms.RadioButton();
            this.radioButton34 = new System.Windows.Forms.RadioButton();
            this.q10 = new System.Windows.Forms.GroupBox();
            this.radioButton19 = new System.Windows.Forms.RadioButton();
            this.radioButton20 = new System.Windows.Forms.RadioButton();
            this.radioButton35 = new System.Windows.Forms.RadioButton();
            this.radioButton36 = new System.Windows.Forms.RadioButton();
            this.radioButton37 = new System.Windows.Forms.RadioButton();
            this.q11 = new System.Windows.Forms.GroupBox();
            this.radioButton21 = new System.Windows.Forms.RadioButton();
            this.radioButton22 = new System.Windows.Forms.RadioButton();
            this.radioButton38 = new System.Windows.Forms.RadioButton();
            this.radioButton39 = new System.Windows.Forms.RadioButton();
            this.radioButton40 = new System.Windows.Forms.RadioButton();
            this.q4 = new System.Windows.Forms.GroupBox();
            this.radioButton44 = new System.Windows.Forms.RadioButton();
            this.radioButton45 = new System.Windows.Forms.RadioButton();
            this.radioButton46 = new System.Windows.Forms.RadioButton();
            this.radioButton47 = new System.Windows.Forms.RadioButton();
            this.radioButton48 = new System.Windows.Forms.RadioButton();
            this.q5 = new System.Windows.Forms.GroupBox();
            this.radioButton9 = new System.Windows.Forms.RadioButton();
            this.radioButton10 = new System.Windows.Forms.RadioButton();
            this.radioButton49 = new System.Windows.Forms.RadioButton();
            this.radioButton50 = new System.Windows.Forms.RadioButton();
            this.radioButton51 = new System.Windows.Forms.RadioButton();
            this.q6 = new System.Windows.Forms.GroupBox();
            this.radioButton11 = new System.Windows.Forms.RadioButton();
            this.radioButton12 = new System.Windows.Forms.RadioButton();
            this.radioButton52 = new System.Windows.Forms.RadioButton();
            this.radioButton53 = new System.Windows.Forms.RadioButton();
            this.radioButton54 = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.q9a = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.q6a = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.q5a = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.q1.SuspendLayout();
            this.q2.SuspendLayout();
            this.q3.SuspendLayout();
            this.q7.SuspendLayout();
            this.q8.SuspendLayout();
            this.q9.SuspendLayout();
            this.q10.SuspendLayout();
            this.q11.SuspendLayout();
            this.q4.SuspendLayout();
            this.q5.SuspendLayout();
            this.q6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bell MT", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(30, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(655, 42);
            this.label1.TabIndex = 0;
            this.label1.Text = "CASEA User Experience Questionnaire";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Gill Sans MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 23);
            this.label2.TabIndex = 0;
            this.label2.Text = "BACKGROUND";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(24, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(449, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "1. Have you participated in a user study for a software product prior to today?";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(21, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(386, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "2. Do you have any prior experience with undergraduate research?";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(21, 182);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(346, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "3. How would you describe your familiarity with this project?";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Gill Sans MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 251);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(215, 23);
            this.label6.TabIndex = 4;
            this.label6.Text = "HANDS ON EXPERIENCE";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(21, 274);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(446, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "4. How would you describe your experience with bug/issue tracking systems?";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(21, 343);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(463, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "5. How would you describe your experience in assisting an open source project?";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(21, 481);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(569, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "6. How would you describe your experience in testing new software, such as alpha " +
                "or beta testing?";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Gill Sans MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 685);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(229, 23);
            this.label10.TabIndex = 8;
            this.label10.Text = "TECHNICAL KNOWLEDGE";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(21, 616);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(381, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "7. How would you describe your familiarity with the bug life cycle?";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(21, 708);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(440, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "8. How would you describe your familiarity with machine learning algorithms?";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(21, 777);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(449, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "9. How would you describe your familiarity with classifiers and recommenders?";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(21, 912);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(464, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "10. How would you describe your familiarty with user interface design principles?" +
                "";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(21, 1073);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(737, 13);
            this.label15.TabIndex = 13;
            this.label15.Text = "12. Is there anything else about your background or experience you wish to share " +
                "that you think would be relevant to this study?";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(21, 981);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(355, 13);
            this.label16.TabIndex = 14;
            this.label16.Text = "11. How would you describe your familiarity with data mining?";
            // 
            // q1
            // 
            this.q1.Controls.Add(this.radioButton2);
            this.q1.Controls.Add(this.radioButton1);
            this.q1.Location = new System.Drawing.Point(24, 60);
            this.q1.Name = "q1";
            this.q1.Size = new System.Drawing.Size(102, 50);
            this.q1.TabIndex = 15;
            this.q1.TabStop = false;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(55, 19);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(39, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "No";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(6, 19);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(43, 17);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Yes";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // q2
            // 
            this.q2.Controls.Add(this.radioButton3);
            this.q2.Controls.Add(this.radioButton4);
            this.q2.Location = new System.Drawing.Point(24, 129);
            this.q2.Name = "q2";
            this.q2.Size = new System.Drawing.Size(102, 50);
            this.q2.TabIndex = 16;
            this.q2.TabStop = false;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(55, 19);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(39, 17);
            this.radioButton3.TabIndex = 1;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "No";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(6, 19);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(43, 17);
            this.radioButton4.TabIndex = 0;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Yes";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // q3
            // 
            this.q3.Controls.Add(this.radioButton25);
            this.q3.Controls.Add(this.radioButton24);
            this.q3.Controls.Add(this.radioButton23);
            this.q3.Controls.Add(this.radioButton5);
            this.q3.Controls.Add(this.radioButton6);
            this.q3.Location = new System.Drawing.Point(24, 198);
            this.q3.Name = "q3";
            this.q3.Size = new System.Drawing.Size(478, 50);
            this.q3.TabIndex = 17;
            this.q3.TabStop = false;
            // 
            // radioButton25
            // 
            this.radioButton25.AutoSize = true;
            this.radioButton25.Location = new System.Drawing.Point(384, 19);
            this.radioButton25.Name = "radioButton25";
            this.radioButton25.Size = new System.Drawing.Size(87, 17);
            this.radioButton25.TabIndex = 4;
            this.radioButton25.TabStop = true;
            this.radioButton25.Text = "No Familiarity";
            this.radioButton25.UseVisualStyleBackColor = true;
            this.radioButton25.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton24
            // 
            this.radioButton24.AutoSize = true;
            this.radioButton24.Location = new System.Drawing.Point(303, 19);
            this.radioButton24.Name = "radioButton24";
            this.radioButton24.Size = new System.Drawing.Size(75, 17);
            this.radioButton24.TabIndex = 3;
            this.radioButton24.TabStop = true;
            this.radioButton24.Text = "Heard of It";
            this.radioButton24.UseVisualStyleBackColor = true;
            this.radioButton24.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton23
            // 
            this.radioButton23.AutoSize = true;
            this.radioButton23.Location = new System.Drawing.Point(202, 19);
            this.radioButton23.Name = "radioButton23";
            this.radioButton23.Size = new System.Drawing.Size(95, 17);
            this.radioButton23.TabIndex = 2;
            this.radioButton23.TabStop = true;
            this.radioButton23.Text = "Little Familiarity";
            this.radioButton23.UseVisualStyleBackColor = true;
            this.radioButton23.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(96, 19);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(100, 17);
            this.radioButton5.TabIndex = 1;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "Some Familiarity";
            this.radioButton5.UseVisualStyleBackColor = true;
            this.radioButton5.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(6, 19);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(84, 17);
            this.radioButton6.TabIndex = 0;
            this.radioButton6.TabStop = true;
            this.radioButton6.Text = "Very Familiar";
            this.radioButton6.UseVisualStyleBackColor = true;
            this.radioButton6.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Gill Sans MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(6, 1050);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(252, 23);
            this.label17.TabIndex = 26;
            this.label17.Text = "ADDITIONAL INFORMATION";
            // 
            // q12
            // 
            this.q12.Location = new System.Drawing.Point(24, 1089);
            this.q12.Multiline = true;
            this.q12.Name = "q12";
            this.q12.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.q12.Size = new System.Drawing.Size(259, 93);
            this.q12.TabIndex = 27;
            // 
            // q7
            // 
            this.q7.Controls.Add(this.radioButton13);
            this.q7.Controls.Add(this.radioButton14);
            this.q7.Controls.Add(this.radioButton26);
            this.q7.Controls.Add(this.radioButton27);
            this.q7.Controls.Add(this.radioButton28);
            this.q7.Location = new System.Drawing.Point(24, 632);
            this.q7.Name = "q7";
            this.q7.Size = new System.Drawing.Size(478, 50);
            this.q7.TabIndex = 28;
            this.q7.TabStop = false;
            // 
            // radioButton13
            // 
            this.radioButton13.AutoSize = true;
            this.radioButton13.Location = new System.Drawing.Point(384, 19);
            this.radioButton13.Name = "radioButton13";
            this.radioButton13.Size = new System.Drawing.Size(87, 17);
            this.radioButton13.TabIndex = 4;
            this.radioButton13.TabStop = true;
            this.radioButton13.Text = "No Familiarity";
            this.radioButton13.UseVisualStyleBackColor = true;
            this.radioButton13.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton14
            // 
            this.radioButton14.AutoSize = true;
            this.radioButton14.Location = new System.Drawing.Point(303, 19);
            this.radioButton14.Name = "radioButton14";
            this.radioButton14.Size = new System.Drawing.Size(75, 17);
            this.radioButton14.TabIndex = 3;
            this.radioButton14.TabStop = true;
            this.radioButton14.Text = "Heard of It";
            this.radioButton14.UseVisualStyleBackColor = true;
            this.radioButton14.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton26
            // 
            this.radioButton26.AutoSize = true;
            this.radioButton26.Location = new System.Drawing.Point(202, 19);
            this.radioButton26.Name = "radioButton26";
            this.radioButton26.Size = new System.Drawing.Size(95, 17);
            this.radioButton26.TabIndex = 2;
            this.radioButton26.TabStop = true;
            this.radioButton26.Text = "Little Familiarity";
            this.radioButton26.UseVisualStyleBackColor = true;
            this.radioButton26.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton27
            // 
            this.radioButton27.AutoSize = true;
            this.radioButton27.Location = new System.Drawing.Point(96, 19);
            this.radioButton27.Name = "radioButton27";
            this.radioButton27.Size = new System.Drawing.Size(100, 17);
            this.radioButton27.TabIndex = 1;
            this.radioButton27.TabStop = true;
            this.radioButton27.Text = "Some Familiarity";
            this.radioButton27.UseVisualStyleBackColor = true;
            this.radioButton27.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton28
            // 
            this.radioButton28.AutoSize = true;
            this.radioButton28.Location = new System.Drawing.Point(6, 19);
            this.radioButton28.Name = "radioButton28";
            this.radioButton28.Size = new System.Drawing.Size(84, 17);
            this.radioButton28.TabIndex = 0;
            this.radioButton28.TabStop = true;
            this.radioButton28.Text = "Very Familiar";
            this.radioButton28.UseVisualStyleBackColor = true;
            this.radioButton28.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // q8
            // 
            this.q8.Controls.Add(this.radioButton15);
            this.q8.Controls.Add(this.radioButton16);
            this.q8.Controls.Add(this.radioButton29);
            this.q8.Controls.Add(this.radioButton30);
            this.q8.Controls.Add(this.radioButton31);
            this.q8.Location = new System.Drawing.Point(24, 724);
            this.q8.Name = "q8";
            this.q8.Size = new System.Drawing.Size(478, 50);
            this.q8.TabIndex = 18;
            this.q8.TabStop = false;
            // 
            // radioButton15
            // 
            this.radioButton15.AutoSize = true;
            this.radioButton15.Location = new System.Drawing.Point(384, 19);
            this.radioButton15.Name = "radioButton15";
            this.radioButton15.Size = new System.Drawing.Size(87, 17);
            this.radioButton15.TabIndex = 4;
            this.radioButton15.TabStop = true;
            this.radioButton15.Text = "No Familiarity";
            this.radioButton15.UseVisualStyleBackColor = true;
            this.radioButton15.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton16
            // 
            this.radioButton16.AutoSize = true;
            this.radioButton16.Location = new System.Drawing.Point(303, 19);
            this.radioButton16.Name = "radioButton16";
            this.radioButton16.Size = new System.Drawing.Size(75, 17);
            this.radioButton16.TabIndex = 3;
            this.radioButton16.TabStop = true;
            this.radioButton16.Text = "Heard of It";
            this.radioButton16.UseVisualStyleBackColor = true;
            this.radioButton16.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton29
            // 
            this.radioButton29.AutoSize = true;
            this.radioButton29.Location = new System.Drawing.Point(202, 19);
            this.radioButton29.Name = "radioButton29";
            this.radioButton29.Size = new System.Drawing.Size(95, 17);
            this.radioButton29.TabIndex = 2;
            this.radioButton29.TabStop = true;
            this.radioButton29.Text = "Little Familiarity";
            this.radioButton29.UseVisualStyleBackColor = true;
            this.radioButton29.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton30
            // 
            this.radioButton30.AutoSize = true;
            this.radioButton30.Location = new System.Drawing.Point(96, 19);
            this.radioButton30.Name = "radioButton30";
            this.radioButton30.Size = new System.Drawing.Size(100, 17);
            this.radioButton30.TabIndex = 1;
            this.radioButton30.TabStop = true;
            this.radioButton30.Text = "Some Familiarity";
            this.radioButton30.UseVisualStyleBackColor = true;
            this.radioButton30.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton31
            // 
            this.radioButton31.AutoSize = true;
            this.radioButton31.Location = new System.Drawing.Point(6, 19);
            this.radioButton31.Name = "radioButton31";
            this.radioButton31.Size = new System.Drawing.Size(84, 17);
            this.radioButton31.TabIndex = 0;
            this.radioButton31.TabStop = true;
            this.radioButton31.Text = "Very Familiar";
            this.radioButton31.UseVisualStyleBackColor = true;
            this.radioButton31.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // q9
            // 
            this.q9.Controls.Add(this.radioButton17);
            this.q9.Controls.Add(this.radioButton18);
            this.q9.Controls.Add(this.radioButton32);
            this.q9.Controls.Add(this.radioButton33);
            this.q9.Controls.Add(this.radioButton34);
            this.q9.Location = new System.Drawing.Point(24, 792);
            this.q9.Name = "q9";
            this.q9.Size = new System.Drawing.Size(478, 50);
            this.q9.TabIndex = 18;
            this.q9.TabStop = false;
            // 
            // radioButton17
            // 
            this.radioButton17.AutoSize = true;
            this.radioButton17.Location = new System.Drawing.Point(384, 19);
            this.radioButton17.Name = "radioButton17";
            this.radioButton17.Size = new System.Drawing.Size(87, 17);
            this.radioButton17.TabIndex = 4;
            this.radioButton17.TabStop = true;
            this.radioButton17.Text = "No Familiarity";
            this.radioButton17.UseVisualStyleBackColor = true;
            this.radioButton17.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton18
            // 
            this.radioButton18.AutoSize = true;
            this.radioButton18.Location = new System.Drawing.Point(303, 19);
            this.radioButton18.Name = "radioButton18";
            this.radioButton18.Size = new System.Drawing.Size(75, 17);
            this.radioButton18.TabIndex = 3;
            this.radioButton18.TabStop = true;
            this.radioButton18.Text = "Heard of It";
            this.radioButton18.UseVisualStyleBackColor = true;
            this.radioButton18.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton32
            // 
            this.radioButton32.AutoSize = true;
            this.radioButton32.Location = new System.Drawing.Point(202, 19);
            this.radioButton32.Name = "radioButton32";
            this.radioButton32.Size = new System.Drawing.Size(95, 17);
            this.radioButton32.TabIndex = 2;
            this.radioButton32.TabStop = true;
            this.radioButton32.Text = "Little Familiarity";
            this.radioButton32.UseVisualStyleBackColor = true;
            this.radioButton32.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton33
            // 
            this.radioButton33.AutoSize = true;
            this.radioButton33.Location = new System.Drawing.Point(96, 19);
            this.radioButton33.Name = "radioButton33";
            this.radioButton33.Size = new System.Drawing.Size(100, 17);
            this.radioButton33.TabIndex = 1;
            this.radioButton33.TabStop = true;
            this.radioButton33.Text = "Some Familiarity";
            this.radioButton33.UseVisualStyleBackColor = true;
            this.radioButton33.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton34
            // 
            this.radioButton34.AutoSize = true;
            this.radioButton34.Location = new System.Drawing.Point(6, 19);
            this.radioButton34.Name = "radioButton34";
            this.radioButton34.Size = new System.Drawing.Size(84, 17);
            this.radioButton34.TabIndex = 0;
            this.radioButton34.TabStop = true;
            this.radioButton34.Text = "Very Familiar";
            this.radioButton34.UseVisualStyleBackColor = true;
            this.radioButton34.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // q10
            // 
            this.q10.Controls.Add(this.radioButton19);
            this.q10.Controls.Add(this.radioButton20);
            this.q10.Controls.Add(this.radioButton35);
            this.q10.Controls.Add(this.radioButton36);
            this.q10.Controls.Add(this.radioButton37);
            this.q10.Location = new System.Drawing.Point(24, 929);
            this.q10.Name = "q10";
            this.q10.Size = new System.Drawing.Size(478, 50);
            this.q10.TabIndex = 18;
            this.q10.TabStop = false;
            // 
            // radioButton19
            // 
            this.radioButton19.AutoSize = true;
            this.radioButton19.Location = new System.Drawing.Point(384, 19);
            this.radioButton19.Name = "radioButton19";
            this.radioButton19.Size = new System.Drawing.Size(87, 17);
            this.radioButton19.TabIndex = 4;
            this.radioButton19.TabStop = true;
            this.radioButton19.Text = "No Familiarity";
            this.radioButton19.UseVisualStyleBackColor = true;
            this.radioButton19.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton20
            // 
            this.radioButton20.AutoSize = true;
            this.radioButton20.Location = new System.Drawing.Point(303, 19);
            this.radioButton20.Name = "radioButton20";
            this.radioButton20.Size = new System.Drawing.Size(75, 17);
            this.radioButton20.TabIndex = 3;
            this.radioButton20.TabStop = true;
            this.radioButton20.Text = "Heard of It";
            this.radioButton20.UseVisualStyleBackColor = true;
            this.radioButton20.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton35
            // 
            this.radioButton35.AutoSize = true;
            this.radioButton35.Location = new System.Drawing.Point(202, 19);
            this.radioButton35.Name = "radioButton35";
            this.radioButton35.Size = new System.Drawing.Size(95, 17);
            this.radioButton35.TabIndex = 2;
            this.radioButton35.TabStop = true;
            this.radioButton35.Text = "Little Familiarity";
            this.radioButton35.UseVisualStyleBackColor = true;
            this.radioButton35.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton36
            // 
            this.radioButton36.AutoSize = true;
            this.radioButton36.Location = new System.Drawing.Point(96, 19);
            this.radioButton36.Name = "radioButton36";
            this.radioButton36.Size = new System.Drawing.Size(100, 17);
            this.radioButton36.TabIndex = 1;
            this.radioButton36.TabStop = true;
            this.radioButton36.Text = "Some Familiarity";
            this.radioButton36.UseVisualStyleBackColor = true;
            this.radioButton36.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton37
            // 
            this.radioButton37.AutoSize = true;
            this.radioButton37.Location = new System.Drawing.Point(6, 19);
            this.radioButton37.Name = "radioButton37";
            this.radioButton37.Size = new System.Drawing.Size(84, 17);
            this.radioButton37.TabIndex = 0;
            this.radioButton37.TabStop = true;
            this.radioButton37.Text = "Very Familiar";
            this.radioButton37.UseVisualStyleBackColor = true;
            this.radioButton37.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // q11
            // 
            this.q11.Controls.Add(this.radioButton21);
            this.q11.Controls.Add(this.radioButton22);
            this.q11.Controls.Add(this.radioButton38);
            this.q11.Controls.Add(this.radioButton39);
            this.q11.Controls.Add(this.radioButton40);
            this.q11.Location = new System.Drawing.Point(24, 1000);
            this.q11.Name = "q11";
            this.q11.Size = new System.Drawing.Size(478, 50);
            this.q11.TabIndex = 18;
            this.q11.TabStop = false;
            // 
            // radioButton21
            // 
            this.radioButton21.AutoSize = true;
            this.radioButton21.Location = new System.Drawing.Point(384, 19);
            this.radioButton21.Name = "radioButton21";
            this.radioButton21.Size = new System.Drawing.Size(87, 17);
            this.radioButton21.TabIndex = 4;
            this.radioButton21.TabStop = true;
            this.radioButton21.Text = "No Familiarity";
            this.radioButton21.UseVisualStyleBackColor = true;
            this.radioButton21.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton22
            // 
            this.radioButton22.AutoSize = true;
            this.radioButton22.Location = new System.Drawing.Point(303, 19);
            this.radioButton22.Name = "radioButton22";
            this.radioButton22.Size = new System.Drawing.Size(75, 17);
            this.radioButton22.TabIndex = 3;
            this.radioButton22.TabStop = true;
            this.radioButton22.Text = "Heard of It";
            this.radioButton22.UseVisualStyleBackColor = true;
            this.radioButton22.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton38
            // 
            this.radioButton38.AutoSize = true;
            this.radioButton38.Location = new System.Drawing.Point(202, 19);
            this.radioButton38.Name = "radioButton38";
            this.radioButton38.Size = new System.Drawing.Size(95, 17);
            this.radioButton38.TabIndex = 2;
            this.radioButton38.TabStop = true;
            this.radioButton38.Text = "Little Familiarity";
            this.radioButton38.UseVisualStyleBackColor = true;
            this.radioButton38.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton39
            // 
            this.radioButton39.AutoSize = true;
            this.radioButton39.Location = new System.Drawing.Point(96, 19);
            this.radioButton39.Name = "radioButton39";
            this.radioButton39.Size = new System.Drawing.Size(100, 17);
            this.radioButton39.TabIndex = 1;
            this.radioButton39.TabStop = true;
            this.radioButton39.Text = "Some Familiarity";
            this.radioButton39.UseVisualStyleBackColor = true;
            this.radioButton39.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton40
            // 
            this.radioButton40.AutoSize = true;
            this.radioButton40.Location = new System.Drawing.Point(6, 19);
            this.radioButton40.Name = "radioButton40";
            this.radioButton40.Size = new System.Drawing.Size(84, 17);
            this.radioButton40.TabIndex = 0;
            this.radioButton40.TabStop = true;
            this.radioButton40.Text = "Very Familiar";
            this.radioButton40.UseVisualStyleBackColor = true;
            this.radioButton40.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // q4
            // 
            this.q4.Controls.Add(this.radioButton44);
            this.q4.Controls.Add(this.radioButton45);
            this.q4.Controls.Add(this.radioButton46);
            this.q4.Controls.Add(this.radioButton47);
            this.q4.Controls.Add(this.radioButton48);
            this.q4.Location = new System.Drawing.Point(24, 290);
            this.q4.Name = "q4";
            this.q4.Size = new System.Drawing.Size(553, 50);
            this.q4.TabIndex = 29;
            this.q4.TabStop = false;
            // 
            // radioButton44
            // 
            this.radioButton44.AutoSize = true;
            this.radioButton44.Location = new System.Drawing.Point(445, 19);
            this.radioButton44.Name = "radioButton44";
            this.radioButton44.Size = new System.Drawing.Size(95, 17);
            this.radioButton44.TabIndex = 4;
            this.radioButton44.TabStop = true;
            this.radioButton44.Text = "No Experience";
            this.radioButton44.UseVisualStyleBackColor = true;
            this.radioButton44.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton45
            // 
            this.radioButton45.AutoSize = true;
            this.radioButton45.Location = new System.Drawing.Point(343, 19);
            this.radioButton45.Name = "radioButton45";
            this.radioButton45.Size = new System.Drawing.Size(96, 17);
            this.radioButton45.TabIndex = 3;
            this.radioButton45.TabStop = true;
            this.radioButton45.Text = "Heard of Them";
            this.radioButton45.UseVisualStyleBackColor = true;
            this.radioButton45.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton46
            // 
            this.radioButton46.AutoSize = true;
            this.radioButton46.Location = new System.Drawing.Point(234, 19);
            this.radioButton46.Name = "radioButton46";
            this.radioButton46.Size = new System.Drawing.Size(103, 17);
            this.radioButton46.TabIndex = 2;
            this.radioButton46.TabStop = true;
            this.radioButton46.Text = "Little Experience";
            this.radioButton46.UseVisualStyleBackColor = true;
            this.radioButton46.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton47
            // 
            this.radioButton47.AutoSize = true;
            this.radioButton47.Location = new System.Drawing.Point(120, 19);
            this.radioButton47.Name = "radioButton47";
            this.radioButton47.Size = new System.Drawing.Size(108, 17);
            this.radioButton47.TabIndex = 1;
            this.radioButton47.TabStop = true;
            this.radioButton47.Text = "Some Experience";
            this.radioButton47.UseVisualStyleBackColor = true;
            this.radioButton47.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton48
            // 
            this.radioButton48.AutoSize = true;
            this.radioButton48.Location = new System.Drawing.Point(6, 19);
            this.radioButton48.Name = "radioButton48";
            this.radioButton48.Size = new System.Drawing.Size(108, 17);
            this.radioButton48.TabIndex = 0;
            this.radioButton48.TabStop = true;
            this.radioButton48.Text = "Very Experienced";
            this.radioButton48.UseVisualStyleBackColor = true;
            this.radioButton48.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // q5
            // 
            this.q5.Controls.Add(this.radioButton9);
            this.q5.Controls.Add(this.radioButton10);
            this.q5.Controls.Add(this.radioButton49);
            this.q5.Controls.Add(this.radioButton50);
            this.q5.Controls.Add(this.radioButton51);
            this.q5.Location = new System.Drawing.Point(24, 359);
            this.q5.Name = "q5";
            this.q5.Size = new System.Drawing.Size(553, 50);
            this.q5.TabIndex = 30;
            this.q5.TabStop = false;
            // 
            // radioButton9
            // 
            this.radioButton9.AutoSize = true;
            this.radioButton9.Location = new System.Drawing.Point(445, 19);
            this.radioButton9.Name = "radioButton9";
            this.radioButton9.Size = new System.Drawing.Size(95, 17);
            this.radioButton9.TabIndex = 4;
            this.radioButton9.TabStop = true;
            this.radioButton9.Text = "No Experience";
            this.radioButton9.UseVisualStyleBackColor = true;
            this.radioButton9.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton10
            // 
            this.radioButton10.AutoSize = true;
            this.radioButton10.Location = new System.Drawing.Point(343, 19);
            this.radioButton10.Name = "radioButton10";
            this.radioButton10.Size = new System.Drawing.Size(96, 17);
            this.radioButton10.TabIndex = 3;
            this.radioButton10.TabStop = true;
            this.radioButton10.Text = "Heard of Them";
            this.radioButton10.UseVisualStyleBackColor = true;
            this.radioButton10.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton49
            // 
            this.radioButton49.AutoSize = true;
            this.radioButton49.Location = new System.Drawing.Point(234, 19);
            this.radioButton49.Name = "radioButton49";
            this.radioButton49.Size = new System.Drawing.Size(103, 17);
            this.radioButton49.TabIndex = 2;
            this.radioButton49.TabStop = true;
            this.radioButton49.Text = "Little Experience";
            this.radioButton49.UseVisualStyleBackColor = true;
            this.radioButton49.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton50
            // 
            this.radioButton50.AutoSize = true;
            this.radioButton50.Location = new System.Drawing.Point(120, 19);
            this.radioButton50.Name = "radioButton50";
            this.radioButton50.Size = new System.Drawing.Size(108, 17);
            this.radioButton50.TabIndex = 1;
            this.radioButton50.TabStop = true;
            this.radioButton50.Text = "Some Experience";
            this.radioButton50.UseVisualStyleBackColor = true;
            this.radioButton50.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton51
            // 
            this.radioButton51.AutoSize = true;
            this.radioButton51.Location = new System.Drawing.Point(6, 19);
            this.radioButton51.Name = "radioButton51";
            this.radioButton51.Size = new System.Drawing.Size(108, 17);
            this.radioButton51.TabIndex = 0;
            this.radioButton51.TabStop = true;
            this.radioButton51.Text = "Very Experienced";
            this.radioButton51.UseVisualStyleBackColor = true;
            this.radioButton51.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // q6
            // 
            this.q6.Controls.Add(this.radioButton11);
            this.q6.Controls.Add(this.radioButton12);
            this.q6.Controls.Add(this.radioButton52);
            this.q6.Controls.Add(this.radioButton53);
            this.q6.Controls.Add(this.radioButton54);
            this.q6.Location = new System.Drawing.Point(24, 497);
            this.q6.Name = "q6";
            this.q6.Size = new System.Drawing.Size(553, 50);
            this.q6.TabIndex = 30;
            this.q6.TabStop = false;
            // 
            // radioButton11
            // 
            this.radioButton11.AutoSize = true;
            this.radioButton11.Location = new System.Drawing.Point(445, 19);
            this.radioButton11.Name = "radioButton11";
            this.radioButton11.Size = new System.Drawing.Size(95, 17);
            this.radioButton11.TabIndex = 4;
            this.radioButton11.TabStop = true;
            this.radioButton11.Text = "No Experience";
            this.radioButton11.UseVisualStyleBackColor = true;
            this.radioButton11.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton12
            // 
            this.radioButton12.AutoSize = true;
            this.radioButton12.Location = new System.Drawing.Point(343, 19);
            this.radioButton12.Name = "radioButton12";
            this.radioButton12.Size = new System.Drawing.Size(96, 17);
            this.radioButton12.TabIndex = 3;
            this.radioButton12.TabStop = true;
            this.radioButton12.Text = "Heard of Them";
            this.radioButton12.UseVisualStyleBackColor = true;
            this.radioButton12.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton52
            // 
            this.radioButton52.AutoSize = true;
            this.radioButton52.Location = new System.Drawing.Point(234, 19);
            this.radioButton52.Name = "radioButton52";
            this.radioButton52.Size = new System.Drawing.Size(103, 17);
            this.radioButton52.TabIndex = 2;
            this.radioButton52.TabStop = true;
            this.radioButton52.Text = "Little Experience";
            this.radioButton52.UseVisualStyleBackColor = true;
            this.radioButton52.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton53
            // 
            this.radioButton53.AutoSize = true;
            this.radioButton53.Location = new System.Drawing.Point(120, 19);
            this.radioButton53.Name = "radioButton53";
            this.radioButton53.Size = new System.Drawing.Size(108, 17);
            this.radioButton53.TabIndex = 1;
            this.radioButton53.TabStop = true;
            this.radioButton53.Text = "Some Experience";
            this.radioButton53.UseVisualStyleBackColor = true;
            this.radioButton53.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // radioButton54
            // 
            this.radioButton54.AutoSize = true;
            this.radioButton54.Location = new System.Drawing.Point(6, 19);
            this.radioButton54.Name = "radioButton54";
            this.radioButton54.Size = new System.Drawing.Size(108, 17);
            this.radioButton54.TabIndex = 0;
            this.radioButton54.TabStop = true;
            this.radioButton54.Text = "Very Experienced";
            this.radioButton54.UseVisualStyleBackColor = true;
            this.radioButton54.CheckedChanged += new System.EventHandler(this.radioButton_Checked);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.q9a);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.q6a);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.q5a);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.q6);
            this.groupBox1.Controls.Add(this.q5);
            this.groupBox1.Controls.Add(this.q4);
            this.groupBox1.Controls.Add(this.q11);
            this.groupBox1.Controls.Add(this.q10);
            this.groupBox1.Controls.Add(this.q9);
            this.groupBox1.Controls.Add(this.q8);
            this.groupBox1.Controls.Add(this.q7);
            this.groupBox1.Controls.Add(this.q12);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.q3);
            this.groupBox1.Controls.Add(this.q2);
            this.groupBox1.Controls.Add(this.q1);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(13, 55);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(699, 1235);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(271, 1200);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(151, 23);
            this.button1.TabIndex = 37;
            this.button1.Text = "Submit Survey";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // q9a
            // 
            this.q9a.Location = new System.Drawing.Point(30, 861);
            this.q9a.Multiline = true;
            this.q9a.Name = "q9a";
            this.q9a.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.q9a.Size = new System.Drawing.Size(259, 44);
            this.q9a.TabIndex = 36;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(27, 845);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(178, 13);
            this.label20.TabIndex = 35;
            this.label20.Text = "Briefly describe your familiarty, if any.";
            // 
            // q6a
            // 
            this.q6a.Location = new System.Drawing.Point(30, 566);
            this.q6a.Multiline = true;
            this.q6a.Name = "q6a";
            this.q6a.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.q6a.Size = new System.Drawing.Size(259, 44);
            this.q6a.TabIndex = 34;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(27, 550);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(224, 13);
            this.label19.TabIndex = 33;
            this.label19.Text = "Briefly describe your testing experience, if any.";
            // 
            // q5a
            // 
            this.q5a.Location = new System.Drawing.Point(30, 432);
            this.q5a.Multiline = true;
            this.q5a.Name = "q5a";
            this.q5a.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.q5a.Size = new System.Drawing.Size(259, 44);
            this.q5a.TabIndex = 32;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(27, 416);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(333, 13);
            this.label18.TabIndex = 31;
            this.label18.Text = "List the open source project(s), if any, and describe your involvement.";
            // 
            // SurveyPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(734, 608);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Name = "SurveyPage";
            this.Text = "SurveyPage";
            this.q1.ResumeLayout(false);
            this.q1.PerformLayout();
            this.q2.ResumeLayout(false);
            this.q2.PerformLayout();
            this.q3.ResumeLayout(false);
            this.q3.PerformLayout();
            this.q7.ResumeLayout(false);
            this.q7.PerformLayout();
            this.q8.ResumeLayout(false);
            this.q8.PerformLayout();
            this.q9.ResumeLayout(false);
            this.q9.PerformLayout();
            this.q10.ResumeLayout(false);
            this.q10.PerformLayout();
            this.q11.ResumeLayout(false);
            this.q11.PerformLayout();
            this.q4.ResumeLayout(false);
            this.q4.PerformLayout();
            this.q5.ResumeLayout(false);
            this.q5.PerformLayout();
            this.q6.ResumeLayout(false);
            this.q6.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox q1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.GroupBox q2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.GroupBox q3;
        private System.Windows.Forms.RadioButton radioButton25;
        private System.Windows.Forms.RadioButton radioButton24;
        private System.Windows.Forms.RadioButton radioButton23;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox q12;
        private System.Windows.Forms.GroupBox q7;
        private System.Windows.Forms.RadioButton radioButton13;
        private System.Windows.Forms.RadioButton radioButton14;
        private System.Windows.Forms.RadioButton radioButton26;
        private System.Windows.Forms.RadioButton radioButton27;
        private System.Windows.Forms.RadioButton radioButton28;
        private System.Windows.Forms.GroupBox q8;
        private System.Windows.Forms.RadioButton radioButton15;
        private System.Windows.Forms.RadioButton radioButton16;
        private System.Windows.Forms.RadioButton radioButton29;
        private System.Windows.Forms.RadioButton radioButton30;
        private System.Windows.Forms.RadioButton radioButton31;
        private System.Windows.Forms.GroupBox q9;
        private System.Windows.Forms.RadioButton radioButton17;
        private System.Windows.Forms.RadioButton radioButton18;
        private System.Windows.Forms.RadioButton radioButton32;
        private System.Windows.Forms.RadioButton radioButton33;
        private System.Windows.Forms.RadioButton radioButton34;
        private System.Windows.Forms.GroupBox q10;
        private System.Windows.Forms.RadioButton radioButton19;
        private System.Windows.Forms.RadioButton radioButton20;
        private System.Windows.Forms.RadioButton radioButton35;
        private System.Windows.Forms.RadioButton radioButton36;
        private System.Windows.Forms.RadioButton radioButton37;
        private System.Windows.Forms.GroupBox q11;
        private System.Windows.Forms.RadioButton radioButton21;
        private System.Windows.Forms.RadioButton radioButton22;
        private System.Windows.Forms.RadioButton radioButton38;
        private System.Windows.Forms.RadioButton radioButton39;
        private System.Windows.Forms.RadioButton radioButton40;
        private System.Windows.Forms.GroupBox q4;
        private System.Windows.Forms.RadioButton radioButton44;
        private System.Windows.Forms.RadioButton radioButton45;
        private System.Windows.Forms.RadioButton radioButton46;
        private System.Windows.Forms.RadioButton radioButton47;
        private System.Windows.Forms.RadioButton radioButton48;
        private System.Windows.Forms.GroupBox q5;
        private System.Windows.Forms.RadioButton radioButton9;
        private System.Windows.Forms.RadioButton radioButton10;
        private System.Windows.Forms.RadioButton radioButton49;
        private System.Windows.Forms.RadioButton radioButton50;
        private System.Windows.Forms.RadioButton radioButton51;
        private System.Windows.Forms.GroupBox q6;
        private System.Windows.Forms.RadioButton radioButton11;
        private System.Windows.Forms.RadioButton radioButton12;
        private System.Windows.Forms.RadioButton radioButton52;
        private System.Windows.Forms.RadioButton radioButton53;
        private System.Windows.Forms.RadioButton radioButton54;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox q6a;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox q5a;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox q9a;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button button1;
    }
}