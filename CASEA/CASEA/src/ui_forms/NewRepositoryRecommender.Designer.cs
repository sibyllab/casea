﻿namespace CASEA.src.ui_forms
{
    partial class NewRepositoryRecommender
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewRepositoryRecommender));
            this.label1 = new System.Windows.Forms.Label();
            this.urlTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.creationDatePicker = new System.Windows.Forms.DateTimePicker();
            this.createButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.recommenderNameTextBox = new System.Windows.Forms.TextBox();
            this.nameLabel = new System.Windows.Forms.Label();
            this.btnDestination = new System.Windows.Forms.Button();
            this.destinationPathTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.numberOfReportsUpDown = new System.Windows.Forms.NumericUpDown();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.verifyUrlButton = new System.Windows.Forms.Button();
            this.productDropDown = new System.Windows.Forms.ComboBox();
            this.productLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblProgress = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfReportsUpDown)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Repository URL";
            // 
            // urlTextBox
            // 
            this.urlTextBox.Location = new System.Drawing.Point(94, 20);
            this.urlTextBox.Name = "urlTextBox";
            this.urlTextBox.Size = new System.Drawing.Size(221, 20);
            this.urlTextBox.TabIndex = 1;
            this.urlTextBox.TextChanged += new System.EventHandler(this.OnChange);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Earliest Report Date ";
            // 
            // creationDatePicker
            // 
            this.creationDatePicker.Location = new System.Drawing.Point(117, 20);
            this.creationDatePicker.Name = "creationDatePicker";
            this.creationDatePicker.Size = new System.Drawing.Size(198, 20);
            this.creationDatePicker.TabIndex = 3;
            // 
            // createButton
            // 
            this.createButton.Enabled = false;
            this.createButton.Location = new System.Drawing.Point(129, 346);
            this.createButton.Name = "createButton";
            this.createButton.Size = new System.Drawing.Size(193, 23);
            this.createButton.TabIndex = 4;
            this.createButton.Text = "Create Recommender";
            this.createButton.UseVisualStyleBackColor = true;
            this.createButton.Click += new System.EventHandler(this.CreateButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Maximum Reports";
            // 
            // recommenderNameTextBox
            // 
            this.recommenderNameTextBox.Location = new System.Drawing.Point(118, 26);
            this.recommenderNameTextBox.Name = "recommenderNameTextBox";
            this.recommenderNameTextBox.Size = new System.Drawing.Size(193, 20);
            this.recommenderNameTextBox.TabIndex = 8;
            this.recommenderNameTextBox.TextChanged += new System.EventHandler(this.OnChange);
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(7, 29);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(107, 13);
            this.nameLabel.TabIndex = 7;
            this.nameLabel.Text = "Recommender Name";
            // 
            // btnDestination
            // 
            this.btnDestination.Location = new System.Drawing.Point(322, 50);
            this.btnDestination.Name = "btnDestination";
            this.btnDestination.Size = new System.Drawing.Size(76, 23);
            this.btnDestination.TabIndex = 15;
            this.btnDestination.Text = "Browse...";
            this.btnDestination.UseVisualStyleBackColor = true;
            this.btnDestination.Click += new System.EventHandler(this.btnDestination_Click_1);
            // 
            // destinationPathTextBox
            // 
            this.destinationPathTextBox.Location = new System.Drawing.Point(118, 52);
            this.destinationPathTextBox.Name = "destinationPathTextBox";
            this.destinationPathTextBox.Size = new System.Drawing.Size(193, 20);
            this.destinationPathTextBox.TabIndex = 14;
            this.destinationPathTextBox.TextChanged += new System.EventHandler(this.OnChange);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(52, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Destination";
            // 
            // numberOfReportsUpDown
            // 
            this.numberOfReportsUpDown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numberOfReportsUpDown.Location = new System.Drawing.Point(117, 46);
            this.numberOfReportsUpDown.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numberOfReportsUpDown.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numberOfReportsUpDown.Name = "numberOfReportsUpDown";
            this.numberOfReportsUpDown.Size = new System.Drawing.Size(52, 20);
            this.numberOfReportsUpDown.TabIndex = 19;
            this.numberOfReportsUpDown.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(11, 317);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(408, 23);
            this.progressBar.TabIndex = 20;
            this.progressBar.Visible = false;
            // 
            // verifyUrlButton
            // 
            this.verifyUrlButton.Location = new System.Drawing.Point(321, 18);
            this.verifyUrlButton.Name = "verifyUrlButton";
            this.verifyUrlButton.Size = new System.Drawing.Size(76, 23);
            this.verifyUrlButton.TabIndex = 21;
            this.verifyUrlButton.Text = "Connect";
            this.verifyUrlButton.UseVisualStyleBackColor = true;
            this.verifyUrlButton.Click += new System.EventHandler(this.VerifyUrlButton_Click);
            // 
            // productDropDown
            // 
            this.productDropDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.productDropDown.FormattingEnabled = true;
            this.productDropDown.Location = new System.Drawing.Point(94, 52);
            this.productDropDown.Name = "productDropDown";
            this.productDropDown.Size = new System.Drawing.Size(221, 21);
            this.productDropDown.TabIndex = 22;
            this.productDropDown.SelectedValueChanged += new System.EventHandler(this.OnChange);
            // 
            // productLabel
            // 
            this.productLabel.AutoSize = true;
            this.productLabel.Location = new System.Drawing.Point(44, 55);
            this.productLabel.Name = "productLabel";
            this.productLabel.Size = new System.Drawing.Size(44, 13);
            this.productLabel.TabIndex = 23;
            this.productLabel.Text = "Product";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.productDropDown);
            this.groupBox1.Controls.Add(this.productLabel);
            this.groupBox1.Controls.Add(this.urlTextBox);
            this.groupBox1.Controls.Add(this.verifyUrlButton);
            this.groupBox1.Location = new System.Drawing.Point(14, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(406, 85);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Project Connection Information";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.creationDatePicker);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.numberOfReportsUpDown);
            this.groupBox2.Location = new System.Drawing.Point(14, 114);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(406, 73);
            this.groupBox2.TabIndex = 25;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Report Selection";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.nameLabel);
            this.groupBox3.Controls.Add(this.recommenderNameTextBox);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.destinationPathTextBox);
            this.groupBox3.Controls.Add(this.btnDestination);
            this.groupBox3.Location = new System.Drawing.Point(12, 203);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(407, 81);
            this.groupBox3.TabIndex = 26;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Local Storage";
            // 
            // lblProgress
            // 
            this.lblProgress.AutoSize = true;
            this.lblProgress.Location = new System.Drawing.Point(11, 300);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(0, 13);
            this.lblProgress.TabIndex = 27;
            this.lblProgress.Visible = false;
            // 
            // NewRepositoryRecommender
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(447, 375);
            this.Controls.Add(this.lblProgress);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.createButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NewRepositoryRecommender";
            this.Text = "Recommender from Web Interface";
            ((System.ComponentModel.ISupportInitialize)(this.numberOfReportsUpDown)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox urlTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker creationDatePicker;
        private System.Windows.Forms.Button createButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox recommenderNameTextBox;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Button btnDestination;
        private System.Windows.Forms.TextBox destinationPathTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.FolderBrowserDialog folderBrowser;
        private System.Windows.Forms.NumericUpDown numberOfReportsUpDown;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Button verifyUrlButton;
        private System.Windows.Forms.ComboBox productDropDown;
        private System.Windows.Forms.Label productLabel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblProgress;
    }
}