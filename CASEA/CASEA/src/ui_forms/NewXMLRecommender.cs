/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Windows.Forms;
using CASEA.src.functions;
using CASEA.src.classifier;
using CASEA.src.save_structure;

namespace CASEA.src.ui_forms
{
    public partial class NewXMLRecommender : Form
    {
        public NewXMLRecommender()
        {
            InitializeComponent();

            openFileDialog1.InitialDirectory = System.IO.Path.GetFullPath("../../assets/combined_reports");
            folderBrowserDialog1.SelectedPath = System.IO.Path.GetFullPath("../../recommenders");
        }

        // set the TDS
        private void btnTDS_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                TDSPath.Text = openFileDialog1.FileName;
            }
        }

        // set the VDS
        private void btnVDS_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                VDSPath.Text = openFileDialog1.FileName;
            }
        }

        // set the Destination path
        private void btnDestination_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                DestinationPath.Text = folderBrowserDialog1.SelectedPath;
            }
        }
        // Accept the paths and go
        private void btnOK_Click(object sender, EventArgs e)
        {
            // Create the new recommender if one of the same name doesn't exist.                 
            string saveDirectory = DestinationPath.Text + "/" + recommenderName.Text;
            if (!System.IO.Directory.Exists(saveDirectory))
            {
                System.IO.Directory.CreateDirectory(saveDirectory);
            }

            // Create the object to serialize
            TrainingData TDS = XMLParser.parseTDS(TDSPath.Text);
            ValidationData VDS = XMLParser.parseVDS(TDS, VDSPath.Text);

            SavedRecommender saveObject = new SavedRecommender(recommenderName.Text, TDS, VDS, new Configuration());
            Serializer.SerializeRecommender(saveDirectory + "/recommender_data1.xml", saveObject);

            // Save XML docs
            XDocument TDSDocument = XDocument.Load(TDSPath.Text);
            XDocument VDSDocument = XDocument.Load(VDSPath.Text);
            TDSDocument.Save(saveDirectory + "/TDS.xml");
            VDSDocument.Save(saveDirectory + "/VDS.xml");
            try
            {                
                // Compute TF-IDF for features
                Parser.calculateTfIdf(ref TDS.input.inputArray);
                Parser.calculateTfIdf(ref VDS.input.inputArray);

                MLAlgorithm classifier = new SVMClassifier(RecommenderType.Developer, DBParser.frequencyFiltering(RecommenderType.Developer, TDS, VDS, 5));
                classifier.saveMachine(saveDirectory);

                if (System.IO.File.Exists(saveDirectory + "/classifier.mchn"))
                {
                    MessageBox.Show("Recommender has been created.", "Success");
                    Close();
                }
                else
                {
                    foreach (string file in System.IO.Directory.GetFiles(saveDirectory))
                    {
                        System.IO.File.Delete(file);
                    }

                    System.IO.Directory.Delete(saveDirectory);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
                return;
            }
        }
    }
}