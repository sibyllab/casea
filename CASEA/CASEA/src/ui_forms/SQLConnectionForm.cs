﻿/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace CASEA.src.ui_forms
{
    public partial class SQLConnectionForm : Form
    {
        string connectionString;
        MySqlConnection connection;
        //NewRepositoryRecommender recommenderForm;

        public SQLConnectionForm()
        {
            InitializeComponent();
        }

        public SQLConnectionForm(Form parent)
        {
            InitializeComponent();
            //recommenderForm = parent as NewRepositoryRecommender;
        }

        // Connects to a server by providing an IP address.
        private void btnIPConnect_Click(object sender, EventArgs e)
        {
            connectionString = $@"Server={txtIPAddress.Text.Trim()};Port={txtIPPort.Text.Trim()};Database={txtIPDBName.Text.Trim()};Uid={txtIPUserID.Text.Trim()};Pwd={txtIPPassword.Text.Trim()};Connection Timeout={txtConnectTimeout.Text.Trim()};default command timeout={txtCommandTimeout.Text.Trim()}";

            if (CheckConnection())
            {
                //recommenderForm.usingMySQLCon = true;
                //recommenderForm.connectionString = connectionString;
                QueryBuilder qryForm = new QueryBuilder(connectionString);
                qryForm.Show();
                Close();
            }
        }

        // Tests the database connection to ensure it is valid.
        private bool CheckConnection()
        {
            connection = new MySqlConnection(connectionString);
            try
            {
                connection.Open();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Connection Error");
                return false;
            }

            connection.Close();
            return true;
        }

        private void OpenConnection()
        {
            if (connection != null)
            {
                connection = new MySqlConnection(connectionString);
            }

            if (connection.State != System.Data.ConnectionState.Open)
            {
                connection.Open();
            }
        }

        private void CloseConnection()
        {
            if (connection != null)
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }
    }
}
