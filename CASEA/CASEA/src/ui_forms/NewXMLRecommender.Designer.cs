﻿namespace CASEA.src.ui_forms
{
    partial class NewXMLRecommender
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewXMLRecommender));
            this.nameLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.recommenderName = new System.Windows.Forms.TextBox();
            this.TDSPath = new System.Windows.Forms.TextBox();
            this.VDSPath = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnTDS = new System.Windows.Forms.Button();
            this.btnVDS = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnDestination = new System.Windows.Forms.Button();
            this.DestinationPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(9, 18);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(107, 13);
            this.nameLabel.TabIndex = 0;
            this.nameLabel.Text = "Recommender Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Training Data Set";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Validation Data Set";
            // 
            // recommenderName
            // 
            this.recommenderName.Location = new System.Drawing.Point(122, 15);
            this.recommenderName.Name = "recommenderName";
            this.recommenderName.Size = new System.Drawing.Size(161, 20);
            this.recommenderName.TabIndex = 3;
            // 
            // TDSPath
            // 
            this.TDSPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TDSPath.Location = new System.Drawing.Point(109, 35);
            this.TDSPath.Name = "TDSPath";
            this.TDSPath.Size = new System.Drawing.Size(161, 20);
            this.TDSPath.TabIndex = 4;
            // 
            // VDSPath
            // 
            this.VDSPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VDSPath.Location = new System.Drawing.Point(109, 63);
            this.VDSPath.Name = "VDSPath";
            this.VDSPath.Size = new System.Drawing.Size(161, 20);
            this.VDSPath.TabIndex = 5;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnOK.Location = new System.Drawing.Point(122, 181);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(128, 23);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "Create Recommender";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnTDS
            // 
            this.btnTDS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTDS.Location = new System.Drawing.Point(276, 34);
            this.btnTDS.Name = "btnTDS";
            this.btnTDS.Size = new System.Drawing.Size(75, 23);
            this.btnTDS.TabIndex = 8;
            this.btnTDS.Text = "Browse...";
            this.btnTDS.UseVisualStyleBackColor = true;
            this.btnTDS.Click += new System.EventHandler(this.btnTDS_Click);
            // 
            // btnVDS
            // 
            this.btnVDS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVDS.Location = new System.Drawing.Point(276, 61);
            this.btnVDS.Name = "btnVDS";
            this.btnVDS.Size = new System.Drawing.Size(75, 23);
            this.btnVDS.TabIndex = 9;
            this.btnVDS.Text = "Browse...";
            this.btnVDS.UseVisualStyleBackColor = true;
            this.btnVDS.Click += new System.EventHandler(this.btnVDS_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnDestination
            // 
            this.btnDestination.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDestination.Location = new System.Drawing.Point(289, 149);
            this.btnDestination.Name = "btnDestination";
            this.btnDestination.Size = new System.Drawing.Size(75, 23);
            this.btnDestination.TabIndex = 12;
            this.btnDestination.Text = "Browse...";
            this.btnDestination.UseVisualStyleBackColor = true;
            this.btnDestination.Click += new System.EventHandler(this.btnDestination_Click);
            // 
            // DestinationPath
            // 
            this.DestinationPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DestinationPath.Location = new System.Drawing.Point(122, 151);
            this.DestinationPath.Name = "DestinationPath";
            this.DestinationPath.Size = new System.Drawing.Size(161, 20);
            this.DestinationPath.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 154);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Save Location";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.TDSPath);
            this.groupBox1.Controls.Add(this.btnTDS);
            this.groupBox1.Controls.Add(this.btnVDS);
            this.groupBox1.Controls.Add(this.VDSPath);
            this.groupBox1.Location = new System.Drawing.Point(13, 44);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(360, 94);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data Files";
            // 
            // NewXMLRecommender
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(385, 216);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnDestination);
            this.Controls.Add(this.DestinationPath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.recommenderName);
            this.Controls.Add(this.nameLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NewXMLRecommender";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Recommender from Data Files";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox recommenderName;
        private System.Windows.Forms.TextBox TDSPath;
        private System.Windows.Forms.TextBox VDSPath;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnTDS;
        private System.Windows.Forms.Button btnVDS;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnDestination;
        private System.Windows.Forms.TextBox DestinationPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}