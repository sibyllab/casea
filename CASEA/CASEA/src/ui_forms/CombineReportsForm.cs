﻿/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using System.Windows.Forms;

namespace CASEA.src.ui_forms
{
    public partial class CombineReportsForm : Form
    {
        public CombineReportsForm()
        {
            InitializeComponent();

            openXmlFile.InitialDirectory = System.IO.Path.GetFullPath("../../assets/uncombined_reports");
        }

        // All of the buttons select reports for their respective labels
        private void btnTDSReport_Click(object sender, EventArgs e)
        {
            if (openXmlFile.ShowDialog() == DialogResult.OK)
            {
                TDSReport.Text = openXmlFile.FileName;
            }
        }

        private void btnTDSComment_Click(object sender, EventArgs e)
        {
            if (openXmlFile.ShowDialog() == DialogResult.OK)
            {
                TDSComment.Text = openXmlFile.FileName;
            }
        }

        private void btnTDSHistory_Click(object sender, EventArgs e)
        {
            if (openXmlFile.ShowDialog() == DialogResult.OK)
            {
                TDSHistory.Text = openXmlFile.FileName;
            }
        }

        private void btnVDSReport_Click(object sender, EventArgs e)
        {
            if (openXmlFile.ShowDialog() == DialogResult.OK)
            {
                VDSReport.Text = openXmlFile.FileName;
            }
        }

        private void btnVDSComment_Click(object sender, EventArgs e)
        {
            if (openXmlFile.ShowDialog() == DialogResult.OK)
            {
                VDSComment.Text = openXmlFile.FileName;
            }
        }

        private void btnVDSHistory_Click(object sender, EventArgs e)
        {
            if (openXmlFile.ShowDialog() == DialogResult.OK)
            {
                VDSHistory.Text = openXmlFile.FileName;
            }
        }

        private void btnDupReport_Click(object sender, EventArgs e)
        {
            if (openXmlFile.ShowDialog() == DialogResult.OK)
            {
                DupReport.Text = openXmlFile.FileName;
            }
        }

        private void btnDupComment_Click(object sender, EventArgs e)
        {
            if (openXmlFile.ShowDialog() == DialogResult.OK)
            {
                DupComment.Text = openXmlFile.FileName;
            }
        }

        private void btnDupHistory_Click(object sender, EventArgs e)
        {
            if (openXmlFile.ShowDialog() == DialogResult.OK)
            {
                DupHistory.Text = openXmlFile.FileName;
            }
        }

        private void btnDestination_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = System.IO.Path.GetFullPath("../../assets/combined_reports");
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                DestinationPath.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        // Combine the reports and save to the assets combined_reports folder
        private void btnOK_Click(object sender, EventArgs e)
        {
            // Duplicate Lists
            XDocument dupReports = XDocument.Load(DupReport.Text);
            XDocument dupComments = XDocument.Load(DupComment.Text);
            XDocument dupHistories = XDocument.Load(DupHistory.Text);

            // Create TDS
            XDocument TDS = XDocument.Load(TDSReport.Text); 
            XDocument TDSComments = XDocument.Load(TDSComment.Text); 
            XDocument TDSHistories = XDocument.Load(TDSHistory.Text);

            // Construct every <bugreport> element in the TDS.
            int index = 0;
            foreach(XElement bugreport in TDS.Elements().Elements()) // 2 elements goes down to the "bugreport" level.
            {
                // Add history tag as a child of <bugreport>
                bugreport.Add(TDSHistories.Elements().Elements().ElementAt(index));
                
                // Append comments to reports
                foreach (XElement comment in TDSComments.Elements().Elements()) // 2 elements to reach the comments children
                {
                    if (comment.Element("bug_id").Value == bugreport.Element("reportid").Value)
                    {
                        //bugreport.Element("bug_text").Value += comment.Element("text").Value;

                        // Add firstresponder tag
                        XElement firstresponder = new XElement("firstresponder", comment.Element("creator").Value);
                        bugreport.Element("assigned").AddAfterSelf(firstresponder);

                        break;
                    }
                }

                // Append duplicate summaries and comments to the bugreport bug_text
                while (bugreport.Element("duplicates").Value != "-1")
                {
                    foreach (XElement duplicate in dupReports.Elements().Elements())
                    {
                        // Find the original report
                        if (duplicate.Element("reportid").Value == bugreport.Element("duplicates").Value)
                        {

                            // Set all of the elements to those of the dup report. 
                            bugreport.Element("duplicates").Value = duplicate.Element("duplicates").Value;                            
                            bugreport.Element("assigned").Value = duplicate.Element("assigned").Value;
                            bugreport.Element("reporter").Value = duplicate.Element("reporter").Value;
                            bugreport.Element("bug_text").Value += duplicate.Element("bug_text").Value;

                            // Retrieve and append duplicate comments
                            foreach (XElement comment in dupComments.Elements().Elements())
                            {
                                if (comment.Element("bug_id").Value == duplicate.Element("reportid").Value)
                                {
                                    //bugreport.Element("bug_text").Value += comment.Element("text").Value;

                                    // rebuild firstresponder
                                    bugreport.Element("firstresponder").Value = comment.Element("creator").Value;
                                    break;
                                }
                            }

                            // Change history to the original history
                            foreach (XElement history in dupHistories.Elements().Elements())
                            {
                                if (history.Element("id").Value == duplicate.Element("reportid").Value)
                                {
                                    bugreport.Element("bughistory").ReplaceAll(history.Elements());
                                    break;
                                }
                            }

                            break;
                        }
                    }
                }

                // Create resolver, fixedby and pathgroup tags
                XElement pathgroup = new XElement("pathgroup", "N");
                bugreport.Element("assigned").AddBeforeSelf(pathgroup);
                foreach (XElement eventdata in bugreport.Element("bughistory").Elements("event"))
                {
                    string eventstring = eventdata.Element("added").Value;
                    switch (eventstring)
                    {
                        case "ASSIGNED" :
                            bugreport.Element("pathgroup").Value += "A";
                            break;
                        case "FIXED" :
                            bugreport.Element("pathgroup").Value += "F";
                            break;
                        case "VERIFIED" :
                            bugreport.Element("pathgroup").Value += "V";
                            break;
                        case "CLOSED" :
                            bugreport.Element("pathgroup").Value += "C";
                            break;
                        case "REOPENED" :
                            bugreport.Element("pathgroup").Value += "R";
                            break;
                        case "WORKSFORME" :
                            bugreport.Element("pathgroup").Value += "M";
                            break;
                        case "WONTFIX" :
                            bugreport.Element("pathgroup").Value += "X";
                            break;
                        default :
                            break;
                    }

                    // Create or update the resolver tag
                    if (eventdata.Element("what").Value == "Status" && eventdata.Element("added").Value == "RESOLVED")
                    {
                        // Create or update the resolver tag
                        if (bugreport.Element("resolver") != null)
                        {
                            bugreport.Element("resolver").Value = eventdata.Element("name").Value;
                        }
                        else
                        {
                            XElement resolver = new XElement("resolver", eventdata.Element("name").Value);
                            bugreport.Element("assigned").AddAfterSelf(resolver);
                        }

                        continue;
                    }

                    // Create or update the fixedby tag
                    if (eventdata.Element("what").Value == "Resolution" && eventdata.Element("added").Value == "FIXED")
                    {
                        if (bugreport.Element("fixedby") != null)
                        {
                            bugreport.Element("fixedby").Value = eventdata.Element("name").Value;
                        }
                        else
                        {
                            XElement resolver = new XElement("fixedby", eventdata.Element("name").Value);
                            bugreport.Element("assigned").AddAfterSelf(resolver);
                        }

                        continue;
                    }
                }

                index++; // Simply used to easily set the histories.
            }
            // Save the TDS.
            TDS.Save(DestinationPath.Text + "/TDS.xml");
            
            // Construct the VDS
            XDocument VDS = XDocument.Load(VDSReport.Text);
            XDocument VDSComments = XDocument.Load(VDSComment.Text);
            XDocument VDSHistories = XDocument.Load(VDSHistory.Text);

            
            // Construct every <bugreport> element in the VDS.
            index = 0;
            foreach (XElement bugreport in VDS.Elements().Elements())
            {                
                // Append comments to the summaries
                //foreach(XElement comment in VDSComments.Elements().Elements())
                //{
                //    if (comment.Element("bug_id").Value == bugreport.Element("reportid").Value)
                //    {
                //        bugreport.Element("bug_text").Value += comment.Element("text").Value;
                //        break;
                //    }
                //}
                
                // Append duplicate summaries and comments to the bugreport bug_text
                while (bugreport.Element("duplicates").Value != "-1")
                {
                    foreach (XElement duplicate in dupReports.Elements().Elements())
                    {
                        // Find the original report
                        if (duplicate.Element("reportid").Value == bugreport.Element("duplicates").Value)
                        {
                            bugreport.Element("duplicates").Value = duplicate.Element("duplicates").Value;
                            bugreport.Element("bug_text").Value += duplicate.Element("bug_text").Value;

                            // Retrieve and append duplicate comments
                            //foreach (XElement comment in dupComments.Elements().Elements())
                            //{
                            //    if (comment.Element("bug_id").Value == duplicate.Element("reportid").Value)
                            //    {
                            //        bugreport.Element("bug_text").Value += comment.Element("text").Value;
                            //        break;
                            //    }
                            //}
                            //break;
                        }
                    }
                }
            }
            // Save the Validation data set.
            VDS.Save(DestinationPath.Text + "/VDS.xml");

            // Close the window
            Close();
        }
    }
}
