/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using CASEA.src.functions;
using CASEA.src.classifier;
using CASEA.src.save_structure;
using CASEA.src.repository;
using System.Threading.Tasks;

namespace CASEA.src.ui_forms
{
    public partial class QueryBuilder : Form
    {
        string connectionString;
        string query = "";
        DataSet products;

        // Default constructor (Not used)
        public QueryBuilder()
        {
            InitializeComponent();
        }

        public QueryBuilder(string dbCon)
        {
            InitializeComponent();
            connectionString = dbCon;
            query = "SELECT id, description FROM products ORDER BY description";

            MySqlDataAdapter adapter = null;

            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    adapter = new MySqlDataAdapter(query, connection);
                    products = new DataSet();

                    adapter.Fill(products);
                }

                foreach (DataRow row in products.Tables[0].Rows)
                {
                    cmbProjectName.Items.Add(row["description"]);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
                chkSpecifyProject.Enabled = false;
                return;
            }

            // Initializes check list with ASSIGNED, FIXED, CLOSED, and VERIFIED checked.
            for (int iii = 0; iii <= 3; iii++)
            {
                chklResolution.SetItemChecked(iii, true);
            }

            folderBrowser.SelectedPath = System.IO.Path.GetFullPath("../../recommenders");
        }


        async private void btnGenerate_Click(object sender, EventArgs e)
        {
            // Default initialization.
            int[] bugIDs = new int[1];
            int numReports = 0;

            // Queries the database for bug IDs and fills the bugIDs array with them
            // for random selection later.
            if (chkSelectNumReports.Checked)
            {
                try
                {
                    numReports = Convert.ToInt32(txtNumReports.Text);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");
                    return;
                }
            }

            if (radDateRange.Checked)
            {
                query = CreateDateBasedQuery(datStart.Value, datEnd.Value);
            }
            else if (radLastYear.Checked)
            {
                query = CreateDateBasedQuery(DateTime.Now.AddYears(-1), DateTime.Now);
            }
            else if (radLast5.Checked)
            {
                query = CreateDateBasedQuery(DateTime.Now.AddYears(-5), DateTime.Now);
            }
            else if (radLast10.Checked)
            {
                query = CreateDateBasedQuery(DateTime.Now.AddYears(-10), DateTime.Now);
            }

            if (query != "")
            {
                MySqlDataAdapter adapter = null;
                DataSet results;

                try
                {
                    using (MySqlConnection connection = new MySqlConnection(connectionString))
                    {
                        connection.Open();
                        MySqlCommand cmd = new MySqlCommand(query, connection);
                        adapter = new MySqlDataAdapter(query, connection);
                        results = new DataSet();

                        adapter.Fill(results);
                    }
                    if (chkSelectNumReports.Checked)
                    {
                        bugIDs = new int[numReports];
                        Random r = new Random();

                        for (int iii = 0; iii < numReports; iii++)
                        {
                            // Prevents crashing if the user wants more reports than the query returns.
                            if (results.Tables[0].Rows.Count == 0)
                            {
                                // Reassigns bugIDs to be empty so that no future queries will run.
                                bugIDs = new int[0];
                                break;
                            }

                            int index = r.Next(results.Tables[0].Rows.Count);
                            bugIDs[iii] = ((int)results.Tables[0].Rows[index]["bug_id"]);
                            results.Tables[0].Rows.RemoveAt(index);
                        }
                    }
                    else
                    {
                        bugIDs = new int[results.Tables[0].Rows.Count];

                        for (int iii = 0; iii < results.Tables[0].Rows.Count; iii++)
                        {
                            bugIDs[iii] = ((int)results.Tables[0].Rows[iii]["bug_id"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");
                    return;
                }
            }

            if (bugIDs.Length > 0)
            {
                query = "SELECT bugs.bug_id, assigned_to, bug_severity, bug_status, creation_ts, bugs.short_desc, reporter, resolution, who, fieldid, added, comments, name AS component " +
                        "FROM bugs " +
                            "INNER JOIN bugs_activity " +
                                "ON bugs.bug_id = bugs_activity.bug_id " +
                            "INNER JOIN bugs_fulltext " +
                                "ON bugs.bug_id = bugs_fulltext.bug_id " +
                            "INNER JOIN components " +
                                "ON bugs.component_id = components.id ";

                query = AddIDs(query, bugIDs);

                query += "ORDER BY bug_id ASC";

                //thread = new BackgroundWorker();
                //thread.DoWork += thread_DoWork;
                //thread.RunWorkerCompleted += thread_RunWorkerCompleted;
                btnGenerate.Enabled = false;
                await DoWork();
                //thread.RunWorkerAsync();
            }
            else
            {
                MessageBox.Show("No results returned.  Please try modifying your query.", "No Results");
            }
        }

        //private void thread_DoWork(object sender, DoWorkEventArgs e)
        async Task DoWork()
        {
            if (query != "")
            {
                MySqlDataAdapter adapter = null;
                DataSet results;
                DataTable trainingData = new BugReportTable();
                DataTable validationData = new BugReportTable();

                try
                {
                    using (MySqlConnection connection = new MySqlConnection(connectionString))
                    {
                        connection.Open();
                        MySqlCommand cmd = new MySqlCommand(query, connection);
                        adapter = new MySqlDataAdapter(query, connection);
                        results = new DataSet();

                        await adapter.FillAsync(results);
                    }
                    int reportNumber = 0;

                    for (int iii = 0; iii < results.Tables[0].Rows.Count; iii++)
                    {
                        object[] dataRow = new object[14];
                        dataRow[0] = results.Tables[0].Rows[iii]["bug_id"];
                        dataRow[1] = results.Tables[0].Rows[iii]["resolution"];
                        dataRow[2] = results.Tables[0].Rows[iii]["assigned_to"];
                        dataRow[3] = results.Tables[0].Rows[iii]["bug_severity"];
                        dataRow[4] = "";
                        dataRow[5] = "";
                        dataRow[6] = results.Tables[0].Rows[iii]["who"];
                        dataRow[7] = results.Tables[0].Rows[iii]["short_desc"] + " " + results.Tables[0].Rows[iii]["comments"];
                        dataRow[8] = results.Tables[0].Rows[iii]["bug_status"];
                        dataRow[9] = results.Tables[0].Rows[iii]["creation_ts"];
                        dataRow[10] = results.Tables[0].Rows[iii]["reporter"];
                        dataRow[11] = results.Tables[0].Rows[iii]["resolution"];
                        dataRow[12] = results.Tables[0].Rows[iii]["component"];
                        dataRow[13] = "";

                        int bugID = (int)dataRow[0];

                        // Loops through each activity record for a bug and pulls out information
                        // pertaining to fixedby and resolver.
                        while ((int)results.Tables[0].Rows[iii]["bug_id"] == bugID)
                        {
                            if ((int)results.Tables[0].Rows[iii]["fieldid"] == 11 && results.Tables[0].Rows[iii]["added"].ToString() == "FIXED")
                            {
                                dataRow[4] = results.Tables[0].Rows[iii]["who"].ToString();
                            }
                            else if ((int)results.Tables[0].Rows[iii]["fieldid"] == 8 && results.Tables[0].Rows[iii]["added"].ToString() == "RESOLVED")
                            {
                                dataRow[5] = results.Tables[0].Rows[iii]["who"].ToString();
                            }

                            // Increments index to access next update for the current report.
                            iii++;

                            // Prevents incrementing the index past the last record.
                            if (iii == results.Tables[0].Rows.Count)
                            {
                                break;
                            }
                            else
                            {
                                continue;
                            }
                        }

                        /* Decrements index so that it will point to the first update for the
                           next bug report when the loop occurs and auto-increments the index.
                           Otherwise the index gets incremented in the while loop to point to 
                           the first update for the next report and then gets incremented again
                           by the for loop.  Kinda funky.
                        */
                        iii--;
                        reportNumber++;

                        if (reportNumber % 10 == 0)
                        {
                            validationData.Rows.Add(dataRow);
                        }
                        else
                        {
                            trainingData.Rows.Add(dataRow);
                        }
                    }

                    string saveDirectory = txtDestinationPath.Text + "/" + txtRecommenderName.Text;

                    if (!System.IO.Directory.Exists(saveDirectory))
                    {
                        System.IO.Directory.CreateDirectory(saveDirectory);
                    }

                    Task<TrainingData> TDS = DBParser.parseTDS(trainingData);
                    Task<ValidationData> VDS = DBParser.parseVDS(await TDS, validationData);

                    // Compute TF-IDF for features
                    //Parser.calculateTfIdf(ref TDS.input.inputArray);
                    //Parser.calculateTfIdf(ref VDS.input.inputArray);

                    // Train the classifier with an FC of 5
                    MLAlgorithm classifier = new SVMClassifier(RecommenderType.Developer, DBParser.frequencyFiltering(RecommenderType.Developer, await TDS, await VDS, 5));

                    SavedRecommender saveObject = new SavedRecommender(txtRecommenderName.Text, await TDS, await VDS, new Configuration());

                    // Save XML docs
                    trainingData.WriteXml(saveDirectory + "/TDS.xml");
                    validationData.WriteXml(saveDirectory + "/VDS.xml");

                    // Serialize the savedRecommender object and save the machine
                    Serializer.SerializeRecommender(saveDirectory + "/recommender_data1.xml", saveObject);
                    classifier.saveMachine(saveDirectory);

                    if (System.IO.File.Exists(saveDirectory + "/classifier.mchn"))
                    {
                        MessageBox.Show("Recommender has been created.", "Success");
                    }
                    else
                    {
                        foreach (string file in System.IO.Directory.GetFiles(saveDirectory))
                        {
                            System.IO.File.Delete(file);
                        }

                        System.IO.Directory.Delete(saveDirectory);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");
                    return;
                }
            }

            btnGenerate.Enabled = true;
        }

        //private void thread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        //{
        //    btnGenerate.Enabled = true;
        //}

        public string CreateDateBasedQuery(DateTime startDate, DateTime endDate)
        {
            string dateQuery = "";

            if (chkSpecifyProject.Checked && cmbProjectName.SelectedIndex >= 0)
            {
                dateQuery += $@"SELECT bug_id FROM bugs WHERE (creation_ts BETWEEN '{String.Format("{0:yyyy-MM-dd}", startDate)}' AND '{String.Format("{0:yyyy-MM-dd}", endDate)} 23:59:59') AND product_id = {products.Tables[0].Rows[cmbProjectName.SelectedIndex]["id"]} ";
            }
            else
            {
                dateQuery += $@"SELECT bug_id FROM bugs WHERE (creation_ts BETWEEN '{String.Format("{0:yyyy-MM-dd}", startDate)}' AND '{String.Format("{0:yyyy-MM-dd}", endDate)} 23:59:59') ";
            }

            dateQuery = AddResolution(dateQuery);
            dateQuery += "ORDER BY bug_id ASC";

            return dateQuery;
        }

        // Adds the selected resolution data to the query.
        private string AddResolution(string query)
        {
            string resolutions = "";

            for (int iii = 0; iii < chklResolution.Items.Count; iii++)
            {
                if (chklResolution.GetItemChecked(iii))
                {
                    resolutions += "\'" + chklResolution.Items[iii] + "\',";
                }
            }

            // Prevents an empty IN statement if no resolutions are selected.
            if (resolutions.Length > 0)
            {
                // Removes trailing ','.
                if (resolutions[resolutions.Length - 1] == ',')
                {
                    resolutions = resolutions.Remove(resolutions.Length - 1);
                }

                query += "AND (resolution IN (" + resolutions + ")) ";
            }

            return query;
        }

        // Appends bug_id information to the query.
        // Basically the same structure as addResolution
        private string AddIDs(string query, int[] bugIDs)
        {
            string ids = "";

            for (int iii = 0; iii < bugIDs.Length; iii++)
            {
                ids += bugIDs[iii] + ",";
            }

            if (ids.Length > 0)
            {
                if (ids[ids.Length - 1] == ',')
                {
                    ids = ids.Remove(ids.Length - 1);
                }

                query += "WHERE bugs.bug_id IN (" + ids + ")";
            }

            return query;
        }

        private void chkSelectNumReports_CheckedChanged(object sender, EventArgs e)
        {
            txtNumReports.Enabled = chkSelectNumReports.Checked;
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                txtDestinationPath.Text = folderBrowser.SelectedPath;
            }
        }

        private void chkSpecifyProject_CheckedChanged(object sender, EventArgs e)
        {
            cmbProjectName.Enabled = chkSpecifyProject.Checked;
        }
    }
}