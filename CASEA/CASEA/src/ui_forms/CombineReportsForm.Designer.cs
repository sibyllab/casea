﻿namespace CASEA.src.ui_forms
{
    partial class CombineReportsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TDSReport = new System.Windows.Forms.TextBox();
            this.TDSComment = new System.Windows.Forms.TextBox();
            this.TDSHistory = new System.Windows.Forms.TextBox();
            this.VDSHistory = new System.Windows.Forms.TextBox();
            this.VDSComment = new System.Windows.Forms.TextBox();
            this.VDSReport = new System.Windows.Forms.TextBox();
            this.DupHistory = new System.Windows.Forms.TextBox();
            this.DupComment = new System.Windows.Forms.TextBox();
            this.DupReport = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnTDSReport = new System.Windows.Forms.Button();
            this.btnTDSComment = new System.Windows.Forms.Button();
            this.btnTDSHistory = new System.Windows.Forms.Button();
            this.btnVDSReport = new System.Windows.Forms.Button();
            this.btnVDSComment = new System.Windows.Forms.Button();
            this.btnVDSHistory = new System.Windows.Forms.Button();
            this.btnDupReport = new System.Windows.Forms.Button();
            this.btnDupComment = new System.Windows.Forms.Button();
            this.btnDupHistory = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.openXmlFile = new System.Windows.Forms.OpenFileDialog();
            this.label13 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.DestinationPath = new System.Windows.Forms.TextBox();
            this.btnDestination = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Training Data Set";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 160);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Validation Data Set";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 311);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Duplicate Reports";
            // 
            // TDSReport
            // 
            this.TDSReport.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TDSReport.Location = new System.Drawing.Point(32, 53);
            this.TDSReport.Name = "TDSReport";
            this.TDSReport.Size = new System.Drawing.Size(451, 20);
            this.TDSReport.TabIndex = 3;
            // 
            // TDSComment
            // 
            this.TDSComment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TDSComment.Location = new System.Drawing.Point(32, 95);
            this.TDSComment.Name = "TDSComment";
            this.TDSComment.Size = new System.Drawing.Size(451, 20);
            this.TDSComment.TabIndex = 4;
            // 
            // TDSHistory
            // 
            this.TDSHistory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TDSHistory.Location = new System.Drawing.Point(32, 137);
            this.TDSHistory.Name = "TDSHistory";
            this.TDSHistory.Size = new System.Drawing.Size(451, 20);
            this.TDSHistory.TabIndex = 5;
            // 
            // VDSHistory
            // 
            this.VDSHistory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VDSHistory.Location = new System.Drawing.Point(32, 288);
            this.VDSHistory.Name = "VDSHistory";
            this.VDSHistory.Size = new System.Drawing.Size(451, 20);
            this.VDSHistory.TabIndex = 8;
            // 
            // VDSComment
            // 
            this.VDSComment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VDSComment.Location = new System.Drawing.Point(32, 246);
            this.VDSComment.Name = "VDSComment";
            this.VDSComment.Size = new System.Drawing.Size(451, 20);
            this.VDSComment.TabIndex = 7;
            // 
            // VDSReport
            // 
            this.VDSReport.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VDSReport.Location = new System.Drawing.Point(32, 204);
            this.VDSReport.Name = "VDSReport";
            this.VDSReport.Size = new System.Drawing.Size(451, 20);
            this.VDSReport.TabIndex = 6;
            // 
            // DupHistory
            // 
            this.DupHistory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DupHistory.Location = new System.Drawing.Point(32, 439);
            this.DupHistory.Name = "DupHistory";
            this.DupHistory.Size = new System.Drawing.Size(451, 20);
            this.DupHistory.TabIndex = 11;
            // 
            // DupComment
            // 
            this.DupComment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DupComment.Location = new System.Drawing.Point(32, 397);
            this.DupComment.Name = "DupComment";
            this.DupComment.Size = new System.Drawing.Size(451, 20);
            this.DupComment.TabIndex = 10;
            // 
            // DupReport
            // 
            this.DupReport.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DupReport.Location = new System.Drawing.Point(32, 355);
            this.DupReport.Name = "DupReport";
            this.DupReport.Size = new System.Drawing.Size(451, 20);
            this.DupReport.TabIndex = 9;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnOK.Location = new System.Drawing.Point(252, 518);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 12;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnTDSReport
            // 
            this.btnTDSReport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTDSReport.Location = new System.Drawing.Point(491, 51);
            this.btnTDSReport.Name = "btnTDSReport";
            this.btnTDSReport.Size = new System.Drawing.Size(75, 23);
            this.btnTDSReport.TabIndex = 13;
            this.btnTDSReport.Text = "Browse...";
            this.btnTDSReport.UseVisualStyleBackColor = true;
            this.btnTDSReport.Click += new System.EventHandler(this.btnTDSReport_Click);
            // 
            // btnTDSComment
            // 
            this.btnTDSComment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTDSComment.Location = new System.Drawing.Point(491, 93);
            this.btnTDSComment.Name = "btnTDSComment";
            this.btnTDSComment.Size = new System.Drawing.Size(75, 23);
            this.btnTDSComment.TabIndex = 14;
            this.btnTDSComment.Text = "Browse...";
            this.btnTDSComment.UseVisualStyleBackColor = true;
            this.btnTDSComment.Click += new System.EventHandler(this.btnTDSComment_Click);
            // 
            // btnTDSHistory
            // 
            this.btnTDSHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTDSHistory.Location = new System.Drawing.Point(491, 135);
            this.btnTDSHistory.Name = "btnTDSHistory";
            this.btnTDSHistory.Size = new System.Drawing.Size(75, 23);
            this.btnTDSHistory.TabIndex = 15;
            this.btnTDSHistory.Text = "Browse...";
            this.btnTDSHistory.UseVisualStyleBackColor = true;
            this.btnTDSHistory.Click += new System.EventHandler(this.btnTDSHistory_Click);
            // 
            // btnVDSReport
            // 
            this.btnVDSReport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVDSReport.Location = new System.Drawing.Point(491, 202);
            this.btnVDSReport.Name = "btnVDSReport";
            this.btnVDSReport.Size = new System.Drawing.Size(75, 23);
            this.btnVDSReport.TabIndex = 16;
            this.btnVDSReport.Text = "Browse...";
            this.btnVDSReport.UseVisualStyleBackColor = true;
            this.btnVDSReport.Click += new System.EventHandler(this.btnVDSReport_Click);
            // 
            // btnVDSComment
            // 
            this.btnVDSComment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVDSComment.Location = new System.Drawing.Point(491, 244);
            this.btnVDSComment.Name = "btnVDSComment";
            this.btnVDSComment.Size = new System.Drawing.Size(75, 23);
            this.btnVDSComment.TabIndex = 17;
            this.btnVDSComment.Text = "Browse...";
            this.btnVDSComment.UseVisualStyleBackColor = true;
            this.btnVDSComment.Click += new System.EventHandler(this.btnVDSComment_Click);
            // 
            // btnVDSHistory
            // 
            this.btnVDSHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVDSHistory.Location = new System.Drawing.Point(491, 286);
            this.btnVDSHistory.Name = "btnVDSHistory";
            this.btnVDSHistory.Size = new System.Drawing.Size(75, 23);
            this.btnVDSHistory.TabIndex = 18;
            this.btnVDSHistory.Text = "Browse...";
            this.btnVDSHistory.UseVisualStyleBackColor = true;
            this.btnVDSHistory.Click += new System.EventHandler(this.btnVDSHistory_Click);
            // 
            // btnDupReport
            // 
            this.btnDupReport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDupReport.Location = new System.Drawing.Point(491, 353);
            this.btnDupReport.Name = "btnDupReport";
            this.btnDupReport.Size = new System.Drawing.Size(75, 23);
            this.btnDupReport.TabIndex = 19;
            this.btnDupReport.Text = "Browse...";
            this.btnDupReport.UseVisualStyleBackColor = true;
            this.btnDupReport.Click += new System.EventHandler(this.btnDupReport_Click);
            // 
            // btnDupComment
            // 
            this.btnDupComment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDupComment.Location = new System.Drawing.Point(491, 395);
            this.btnDupComment.Name = "btnDupComment";
            this.btnDupComment.Size = new System.Drawing.Size(75, 23);
            this.btnDupComment.TabIndex = 20;
            this.btnDupComment.Text = "Browse...";
            this.btnDupComment.UseVisualStyleBackColor = true;
            this.btnDupComment.Click += new System.EventHandler(this.btnDupComment_Click);
            // 
            // btnDupHistory
            // 
            this.btnDupHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDupHistory.Location = new System.Drawing.Point(491, 437);
            this.btnDupHistory.Name = "btnDupHistory";
            this.btnDupHistory.Size = new System.Drawing.Size(75, 23);
            this.btnDupHistory.TabIndex = 21;
            this.btnDupHistory.Text = "Browse...";
            this.btnDupHistory.UseVisualStyleBackColor = true;
            this.btnDupHistory.Click += new System.EventHandler(this.btnDupHistory_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Training Reports";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Training Comments";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 118);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Training History";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(32, 272);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 13);
            this.label7.TabIndex = 27;
            this.label7.Text = "Validation Histories";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(32, 230);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "Validation Comments";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(32, 188);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "Validation Reports";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(32, 423);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 13);
            this.label10.TabIndex = 30;
            this.label10.Text = "Duplicate Histories";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(32, 381);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(104, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "Duplicate Comments";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(32, 339);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(92, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "Duplicate Reports";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(12, 462);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(106, 13);
            this.label13.TabIndex = 31;
            this.label13.Text = "DestinationFolder";
            // 
            // DestinationPath
            // 
            this.DestinationPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DestinationPath.Location = new System.Drawing.Point(32, 492);
            this.DestinationPath.Name = "DestinationPath";
            this.DestinationPath.Size = new System.Drawing.Size(451, 20);
            this.DestinationPath.TabIndex = 32;
            // 
            // btnDestination
            // 
            this.btnDestination.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDestination.Location = new System.Drawing.Point(491, 490);
            this.btnDestination.Name = "btnDestination";
            this.btnDestination.Size = new System.Drawing.Size(75, 23);
            this.btnDestination.TabIndex = 33;
            this.btnDestination.Text = "Browse...";
            this.btnDestination.UseVisualStyleBackColor = true;
            this.btnDestination.Click += new System.EventHandler(this.btnDestination_Click);
            // 
            // CombineReportsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(578, 553);
            this.Controls.Add(this.btnDestination);
            this.Controls.Add(this.DestinationPath);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnDupHistory);
            this.Controls.Add(this.btnDupComment);
            this.Controls.Add(this.btnDupReport);
            this.Controls.Add(this.btnVDSHistory);
            this.Controls.Add(this.btnVDSComment);
            this.Controls.Add(this.btnVDSReport);
            this.Controls.Add(this.btnTDSHistory);
            this.Controls.Add(this.btnTDSComment);
            this.Controls.Add(this.btnTDSReport);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.DupHistory);
            this.Controls.Add(this.DupComment);
            this.Controls.Add(this.DupReport);
            this.Controls.Add(this.VDSHistory);
            this.Controls.Add(this.VDSComment);
            this.Controls.Add(this.VDSReport);
            this.Controls.Add(this.TDSHistory);
            this.Controls.Add(this.TDSComment);
            this.Controls.Add(this.TDSReport);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "CombineReportsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CombineReportsForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TDSReport;
        private System.Windows.Forms.TextBox TDSComment;
        private System.Windows.Forms.TextBox TDSHistory;
        private System.Windows.Forms.TextBox VDSHistory;
        private System.Windows.Forms.TextBox VDSComment;
        private System.Windows.Forms.TextBox VDSReport;
        private System.Windows.Forms.TextBox DupHistory;
        private System.Windows.Forms.TextBox DupComment;
        private System.Windows.Forms.TextBox DupReport;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnTDSReport;
        private System.Windows.Forms.Button btnTDSComment;
        private System.Windows.Forms.Button btnTDSHistory;
        private System.Windows.Forms.Button btnVDSReport;
        private System.Windows.Forms.Button btnVDSComment;
        private System.Windows.Forms.Button btnVDSHistory;
        private System.Windows.Forms.Button btnDupReport;
        private System.Windows.Forms.Button btnDupComment;
        private System.Windows.Forms.Button btnDupHistory;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.OpenFileDialog openXmlFile;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TextBox DestinationPath;
        private System.Windows.Forms.Button btnDestination;
    }
}