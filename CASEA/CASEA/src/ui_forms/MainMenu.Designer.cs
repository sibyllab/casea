﻿namespace CASEA.src.ui_forms
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            this.btnCreationAssistant = new System.Windows.Forms.Button();
            this.btnNewFrmRep = new System.Windows.Forms.Button();
            this.btnNewFrmXML = new System.Windows.Forms.Button();
            this.loadRecommenderDialog = new System.Windows.Forms.OpenFileDialog();
            this.btnNewFrmDatabase = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnNewFromBitBucket = new System.Windows.Forms.Button();
            this.btnViewLog = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCreationAssistant
            // 
            this.btnCreationAssistant.Location = new System.Drawing.Point(23, 23);
            this.btnCreationAssistant.Name = "btnCreationAssistant";
            this.btnCreationAssistant.Size = new System.Drawing.Size(191, 41);
            this.btnCreationAssistant.TabIndex = 0;
            this.btnCreationAssistant.Text = "Triage Recommender Configuration";
            this.btnCreationAssistant.UseVisualStyleBackColor = true;
            this.btnCreationAssistant.Click += new System.EventHandler(this.btnCreationAssistant_Click);
            // 
            // btnNewFrmRep
            // 
            this.btnNewFrmRep.Location = new System.Drawing.Point(11, 19);
            this.btnNewFrmRep.Name = "btnNewFrmRep";
            this.btnNewFrmRep.Size = new System.Drawing.Size(191, 41);
            this.btnNewFrmRep.TabIndex = 1;
            this.btnNewFrmRep.Text = "Create from Web Interface";
            this.btnNewFrmRep.UseVisualStyleBackColor = true;
            this.btnNewFrmRep.Click += new System.EventHandler(this.btnNewFrmRep_Click);
            // 
            // btnNewFrmXML
            // 
            this.btnNewFrmXML.Location = new System.Drawing.Point(11, 111);
            this.btnNewFrmXML.Name = "btnNewFrmXML";
            this.btnNewFrmXML.Size = new System.Drawing.Size(191, 41);
            this.btnNewFrmXML.TabIndex = 2;
            this.btnNewFrmXML.Text = "Create from Data Files";
            this.btnNewFrmXML.UseVisualStyleBackColor = true;
            this.btnNewFrmXML.Click += new System.EventHandler(this.btnNewFrmXML_Click);
            // 
            // loadRecommenderDialog
            // 
            this.loadRecommenderDialog.FileName = "openFileDialog1";
            this.loadRecommenderDialog.Filter = "CASTR files (*.cstr)|*.cstr|All files (*.*)|*.*";
            // 
            // btnNewFrmDatabase
            // 
            this.btnNewFrmDatabase.Location = new System.Drawing.Point(11, 66);
            this.btnNewFrmDatabase.Name = "btnNewFrmDatabase";
            this.btnNewFrmDatabase.Size = new System.Drawing.Size(191, 39);
            this.btnNewFrmDatabase.TabIndex = 4;
            this.btnNewFrmDatabase.Text = "Create from Database";
            this.btnNewFrmDatabase.UseVisualStyleBackColor = true;
            this.btnNewFrmDatabase.Click += new System.EventHandler(this.btnNewFrmDatabase_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnNewFromBitBucket);
            this.groupBox1.Controls.Add(this.btnNewFrmRep);
            this.groupBox1.Controls.Add(this.btnNewFrmXML);
            this.groupBox1.Controls.Add(this.btnNewFrmDatabase);
            this.groupBox1.Location = new System.Drawing.Point(12, 87);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(212, 206);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data Collection";
            // 
            // btnNewFromBitBucket
            // 
            this.btnNewFromBitBucket.Location = new System.Drawing.Point(11, 158);
            this.btnNewFromBitBucket.Name = "btnNewFromBitBucket";
            this.btnNewFromBitBucket.Size = new System.Drawing.Size(191, 41);
            this.btnNewFromBitBucket.TabIndex = 5;
            this.btnNewFromBitBucket.Text = "Create from BitBucket Repository";
            this.btnNewFromBitBucket.UseVisualStyleBackColor = true;
            this.btnNewFromBitBucket.Click += new System.EventHandler(this.btnNewFromBitBucket_Click);
            // 
            // btnViewLog
            // 
            this.btnViewLog.Location = new System.Drawing.Point(23, 299);
            this.btnViewLog.Name = "btnViewLog";
            this.btnViewLog.Size = new System.Drawing.Size(191, 41);
            this.btnViewLog.TabIndex = 6;
            this.btnViewLog.Text = "View Log";
            this.btnViewLog.UseVisualStyleBackColor = true;
            this.btnViewLog.Click += new System.EventHandler(this.btnViewLog_Click);
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(236, 364);
            this.Controls.Add(this.btnViewLog);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnCreationAssistant);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CASTR";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCreationAssistant;
        private System.Windows.Forms.Button btnNewFrmRep;
        private System.Windows.Forms.Button btnNewFrmXML;
        private System.Windows.Forms.OpenFileDialog loadRecommenderDialog;
        private System.Windows.Forms.Button btnNewFrmDatabase;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnNewFromBitBucket;
        private System.Windows.Forms.Button btnViewLog;
    }
}