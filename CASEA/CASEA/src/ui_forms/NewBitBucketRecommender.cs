﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CASEA.src.functions;
using CASEA.src.classifier;
using CASEA.src.save_structure;
using CASEA.src.repository;
using CASEA.src.repository.bitbucket_repository;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Threading.Tasks;

namespace CASEA.src.ui_forms
{
    public partial class NewBitbucketRecommender : Form
    {
        public NewBitbucketRecommender()
        {
            InitializeComponent();

            // Points to root folder of project.
            openFileDialog1.InitialDirectory = System.IO.Path.GetFullPath("../../../../");
            folderBrowserDialog1.SelectedPath = System.IO.Path.GetFullPath("../../recommenders");
        }

        private void btnSource_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtSourcePath.Text = openFileDialog1.FileName;
            }
        }

        private void btnDestination_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtDestinationPath.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            if (txtSourcePath.Text == "" || txtDestinationPath.Text == "")
            {
                MessageBox.Show("You need to provide a source path for the JSON file and a destination path for the recommender.", "Error");
                return;
            }

            try
            {
                // Reads in BitBucket issue file (in .json format).
                FileStream stream = new FileStream(txtSourcePath.Text, FileMode.Open);
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(BitbucketRepository));

                stream.Position = 0;
                // All the data that is read in gets stored in arrays in the BitBucketRepository object.
                BitbucketRepository repo = (BitbucketRepository)ser.ReadObject(stream);
                repo.Sort();
                createDataSets(repo);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async void createDataSets(BitbucketRepository bbr)
        {
            DataTable trainingData = new BugReportTable();
            DataTable validationData = new BugReportTable();

            int reportNumber = 0;

            for (int iii = 0; iii < bbr.issues.Length; iii++)
            {
                // Some fields are left null because Bitbucket does
                // not supply information for all fields.
                // See BugReportTable.cs for column names.
                object[] dataRow = new object[14];
                dataRow[0] = bbr.issues[iii].id;
                dataRow[1] = bbr.issues[iii].status;
                dataRow[2] = bbr.issues[iii].assignee;
                // Assignee always needs a value.
                if (dataRow[2] == null)
                {
                    dataRow[2] = "";
                }

                dataRow[3] = bbr.issues[iii].priority;
                // Fixedby (never assigned).
                dataRow[4] = "";
                // Resolver
                dataRow[5] = "";
                // First responder
                dataRow[6] = "";
                dataRow[7] = bbr.issues[iii].content;
                dataRow[8] = bbr.issues[iii].status;
                dataRow[9] = bbr.issues[iii].created_on;
                dataRow[10] = bbr.issues[iii].reporter;
                dataRow[11] = bbr.issues[iii].status;
                dataRow[12] = bbr.issues[iii].component;
                // Duplicates (never assigned).
                dataRow[13] = "";

                int bugID = (int)dataRow[0];
                
                for (int jjj = 0; jjj < bbr.logs.Length; jjj++)
                {
                    // Assumes the array is sorted by bugID.
                    if (bbr.logs[jjj].issue < bugID)
                    {
                        continue;
                    }
                    else if (bbr.logs[jjj].issue > bugID)
                    {
                        break;
                    }

                    // Assigning firstresponder.
                    if((string)dataRow[4] == "")
                    {
                        dataRow[6] = bbr.logs[jjj].user;
                    }

                    // Assigning resolver.
                    if (bbr.logs[jjj].field == "status" && bbr.logs[jjj].changed_to == "resolved")
                    {
                        dataRow[5] = bbr.logs[jjj].user;
                    }
                }

                reportNumber++;

                // 10% of reports go into validation set.
                if (reportNumber % 10 == 0)
                {
                    validationData.Rows.Add(dataRow);
                }
                else
                {
                    trainingData.Rows.Add(dataRow);
                }
            }

            string saveFile = txtDestinationPath.Text + "/" + txtRecommenderName.Text;

            if (!System.IO.Directory.Exists(saveFile))
            {
                System.IO.Directory.CreateDirectory(saveFile);
            }

            Task<TrainingData> TDS = DBParser.parseTDS(trainingData);
            Task<ValidationData> VDS = DBParser.parseVDS(await TDS, validationData);

            // Train the classifier with an FC of 5
            MLAlgorithm classifier = new SVMClassifier(RecommenderType.Developer, DBParser.frequencyFiltering(RecommenderType.Developer, await TDS, await VDS, 5));

            SavedRecommender saveObject = new SavedRecommender(txtRecommenderName.Text, await TDS, await VDS, new Configuration());

            // Save XML docs
            trainingData.WriteXml(saveFile + "/TDS.xml");
            validationData.WriteXml(saveFile + "/VDS.xml");

            // Serialize the savedRecommender object and save the machine
            Serializer.SerializeRecommender(saveFile + "/recommender_data1.xml", saveObject);
            classifier.saveMachine(saveFile);

            if (System.IO.File.Exists(saveFile + "/classifier.mchn"))
            {
                MessageBox.Show("Recommender has been created.", "Success");
                Close();
            }
            else
            {
                foreach (string file in System.IO.Directory.GetFiles(saveFile))
                {
                    System.IO.File.Delete(file);
                }

                System.IO.Directory.Delete(saveFile);
            }

        }
    }
}
