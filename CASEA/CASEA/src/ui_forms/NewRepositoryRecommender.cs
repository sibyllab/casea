﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/

using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using CASEA.src.classifier;
using CASEA.src.repository;
using CASEA.src.save_structure;
using CASEA.src.functions;
using MySql.Data.MySqlClient;

namespace CASEA.src.ui_forms
{
    public partial class NewRepositoryRecommender : Form
    {
        private const string ValidationDatasetFile = "VDS.xml";
        private const string TrainingDatasetFile = "TDS.xml";

        private string DestinationPath => Path.Combine(destinationPathTextBox.Text, recommenderNameTextBox.Text);
        private BugzillaRepository _repository;

        public NewRepositoryRecommender()
        {
            InitializeComponent();

            var userDocuments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            folderBrowser.SelectedPath = System.IO.Path.GetFullPath("../../recommenders");
            creationDatePicker.Value = DateTime.Now.AddMonths(-6);
        }

        void OnChange(object sender, EventArgs e)
        {
            createButton.Enabled = destinationPathTextBox.Text != ""
                                   && recommenderNameTextBox.Text != ""
                                   && urlTextBox.Text != ""
                                   && !string.IsNullOrEmpty(productDropDown.SelectedValue?.ToString())
                                   && _repository != null;
        }

        private async void VerifyUrlButton_Click(object sender, EventArgs e)
        {
            string url = urlTextBox.Text;
            if (url.EndsWith("jsonrpc.cgi") == false)
                url += "/jsonrpc.cgi";
            lblProgress.Show();
            lblProgress.Text = "Connecting to " + url + "...";
            _repository = new BugzillaRepository(url);

            try
            {
                progressBar.Show();
                progressBar.Value = 100;

                var products = await _repository.GetProductsAsync();
                productDropDown.DataSource = products;
            }
            catch (Exception exception)
            {
                MessageBox.Show($"There was an error connecting to {urlTextBox.Text}.\n{exception}", "Failure",
                    MessageBoxButtons.OK);
            }
            finally
            {
                lblProgress.Hide();
                progressBar.Hide();
            }
        }

        private async void CreateButton_Click(object sender, EventArgs e)
        {
            try
            {
                SetControlsEnabled(false);
                progressBar.Value = 0;
                progressBar.Show();
                lblProgress.Show();

                lblProgress.Text = "Creating directory...";
                Directory.CreateDirectory(DestinationPath);
                progressBar.Value = 10;

                lblProgress.Text = "Retrieving reports from " + urlTextBox.Text + "...";
                List<BugReport> reports =
                    await _repository.GetCompleteReportsAsync(productDropDown.SelectedValue?.ToString(), 
                    creationDatePicker.Value,
                    (int) numberOfReportsUpDown.Value);

                progressBar.Value = 50;

                lblProgress.Text = "Creating and training recommender...";
                await CreateAndTrainRecommenderAsync(reports);

                progressBar.Value = 100;

                MessageBox.Show($"The recommender has been successfully saved to {DestinationPath}", "Success",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);

                Close();
            }
            catch (Exception ex)
            {
                var result = MessageBox.Show($"Unable to complete operation: {ex}", "Failure",
                    MessageBoxButtons.RetryCancel);
                if (result == DialogResult.Retry) createButton.PerformClick();
            }
            finally
            {
                progressBar.Hide();
                lblProgress.Hide();
                SetControlsEnabled(true);
            }
        }

        private void SetControlsEnabled(bool enabled)
        {
            foreach (Control control in Controls)
                control.Enabled = enabled;
        }

        private Task CreateAndTrainRecommenderAsync(IList<BugReport> reports)
        {
            return Task.Factory.StartNew(() => CreateAndTrainRecommender(reports));
        }

        private void CreateAndTrainRecommender(IList<BugReport> reports)
        {
            SaveReports(reports);

            // Parse training and validation data from the reports
            var trainingData = XMLParser.parseTDS(Path.Combine(DestinationPath, TrainingDatasetFile));
            var validationData = XMLParser.parseVDS(trainingData,
                Path.Combine(DestinationPath, ValidationDatasetFile));

            // Train the classifier with an FC of 5
            InputOutputPack filteredMachineIO = XMLParser.frequencyFiltering(RecommenderType.Developer, trainingData,
                validationData, 5);

            // Compute TF-IDF for features
            Parser.calculateTfIdf(ref filteredMachineIO.TDS.input.inputArray);
            Parser.calculateTfIdf(ref filteredMachineIO.VDS.input.inputArray);

            MLAlgorithm classifier = new SVMClassifier(RecommenderType.Developer, filteredMachineIO);

            // TODO: Train on average number of reports fixed
            // Serialize the savedRecommender object and save the classifier
            var saveObject = new SavedRecommender(recommenderNameTextBox.Text, trainingData, validationData,
                new Configuration());
            Serializer.SerializeRecommender(Path.Combine(DestinationPath, "recommender.xml"), saveObject);
            classifier.saveMachine(DestinationPath);

            if (System.IO.File.Exists(DestinationPath + "/classifier.mchn"))
            {
                MessageBox.Show("Data saved successfully.", "Success");
            }
            else
            {
                foreach (string file in System.IO.Directory.GetFiles(DestinationPath))
                {
                    System.IO.File.Delete(file);
                }

                System.IO.Directory.Delete(DestinationPath);
            }
        }

        private void SaveReports(IList<BugReport> reports)
        {
            var validationDataSet = new XElement("Root");
            var trainingDataSet = new XElement("Root");
            foreach (var report in reports)
            {
                report.populateReport();
                if (reports.IndexOf(report)%10 == 0)
                    validationDataSet.Add(report.report);
                else
                    trainingDataSet.Add(report.report);
            }
            validationDataSet.Save(Path.Combine(DestinationPath, ValidationDatasetFile));
            trainingDataSet.Save(Path.Combine(DestinationPath, TrainingDatasetFile));
        }

        // set the Destination path
        private void btnDestination_Click_1(object sender, EventArgs e)
        {
            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                destinationPathTextBox.Text = folderBrowser.SelectedPath;
            }
        }
    }
}
