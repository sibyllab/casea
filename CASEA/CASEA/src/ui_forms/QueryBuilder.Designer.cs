﻿namespace CASEA.src.ui_forms
{
    partial class QueryBuilder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QueryBuilder));
            this.pnlSelection = new System.Windows.Forms.Panel();
            this.radLast10 = new System.Windows.Forms.RadioButton();
            this.radLast5 = new System.Windows.Forms.RadioButton();
            this.radLastYear = new System.Windows.Forms.RadioButton();
            this.radDateRange = new System.Windows.Forms.RadioButton();
            this.datEnd = new System.Windows.Forms.DateTimePicker();
            this.datStart = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.chklResolution = new System.Windows.Forms.CheckedListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtNumReports = new System.Windows.Forms.TextBox();
            this.chkSelectNumReports = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRecommenderName = new System.Windows.Forms.TextBox();
            this.txtDestinationPath = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.chkSpecifyProject = new System.Windows.Forms.CheckBox();
            this.cmbProjectName = new System.Windows.Forms.ComboBox();
            this.pnlSelection.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSelection
            // 
            this.pnlSelection.Controls.Add(this.radLast10);
            this.pnlSelection.Controls.Add(this.radLast5);
            this.pnlSelection.Controls.Add(this.radLastYear);
            this.pnlSelection.Controls.Add(this.radDateRange);
            this.pnlSelection.Controls.Add(this.datEnd);
            this.pnlSelection.Controls.Add(this.datStart);
            this.pnlSelection.Controls.Add(this.label1);
            this.pnlSelection.Location = new System.Drawing.Point(197, 32);
            this.pnlSelection.Name = "pnlSelection";
            this.pnlSelection.Size = new System.Drawing.Size(497, 126);
            this.pnlSelection.TabIndex = 7;
            // 
            // radLast10
            // 
            this.radLast10.AutoSize = true;
            this.radLast10.Location = new System.Drawing.Point(3, 99);
            this.radLast10.Name = "radLast10";
            this.radLast10.Size = new System.Drawing.Size(90, 17);
            this.radLast10.TabIndex = 10;
            this.radLast10.Text = "Last 10 Years";
            this.radLast10.UseVisualStyleBackColor = true;
            // 
            // radLast5
            // 
            this.radLast5.AutoSize = true;
            this.radLast5.Location = new System.Drawing.Point(3, 69);
            this.radLast5.Name = "radLast5";
            this.radLast5.Size = new System.Drawing.Size(84, 17);
            this.radLast5.TabIndex = 9;
            this.radLast5.Text = "Last 5 Years";
            this.radLast5.UseVisualStyleBackColor = true;
            // 
            // radLastYear
            // 
            this.radLastYear.AutoSize = true;
            this.radLastYear.Location = new System.Drawing.Point(3, 39);
            this.radLastYear.Name = "radLastYear";
            this.radLastYear.Size = new System.Drawing.Size(70, 17);
            this.radLastYear.TabIndex = 8;
            this.radLastYear.Text = "Last Year";
            this.radLastYear.UseVisualStyleBackColor = true;
            // 
            // radDateRange
            // 
            this.radDateRange.AutoSize = true;
            this.radDateRange.Checked = true;
            this.radDateRange.Location = new System.Drawing.Point(3, 9);
            this.radDateRange.Name = "radDateRange";
            this.radDateRange.Size = new System.Drawing.Size(86, 17);
            this.radDateRange.TabIndex = 7;
            this.radDateRange.TabStop = true;
            this.radDateRange.Text = "Date Range:";
            this.radDateRange.UseVisualStyleBackColor = true;
            // 
            // datEnd
            // 
            this.datEnd.Location = new System.Drawing.Point(312, 9);
            this.datEnd.Name = "datEnd";
            this.datEnd.Size = new System.Drawing.Size(182, 20);
            this.datEnd.TabIndex = 6;
            this.datEnd.Value = new System.DateTime(2001, 10, 31, 0, 0, 0, 0);
            // 
            // datStart
            // 
            this.datStart.Location = new System.Drawing.Point(95, 9);
            this.datStart.Name = "datStart";
            this.datStart.Size = new System.Drawing.Size(189, 20);
            this.datStart.TabIndex = 5;
            this.datStart.Value = new System.DateTime(2001, 10, 1, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(290, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "to";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "Report Resolution";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(193, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "Pull reports from:";
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(280, 316);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(147, 23);
            this.btnGenerate.TabIndex = 10;
            this.btnGenerate.Text = "Create Recommender";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // chklResolution
            // 
            this.chklResolution.FormattingEnabled = true;
            this.chklResolution.Items.AddRange(new object[] {
            "ASSIGNED",
            "FIXED",
            "VERIFIED",
            "CLOSED",
            "REOPENED",
            "WORKSFORME",
            "WONTFIX"});
            this.chklResolution.Location = new System.Drawing.Point(16, 34);
            this.chklResolution.Name = "chklResolution";
            this.chklResolution.Size = new System.Drawing.Size(153, 124);
            this.chklResolution.TabIndex = 9;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cmbProjectName);
            this.panel1.Controls.Add(this.chkSpecifyProject);
            this.panel1.Controls.Add(this.txtNumReports);
            this.panel1.Controls.Add(this.chkSelectNumReports);
            this.panel1.Location = new System.Drawing.Point(197, 164);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(497, 88);
            this.panel1.TabIndex = 11;
            // 
            // txtNumReports
            // 
            this.txtNumReports.Enabled = false;
            this.txtNumReports.Location = new System.Drawing.Point(179, 16);
            this.txtNumReports.Name = "txtNumReports";
            this.txtNumReports.Size = new System.Drawing.Size(151, 20);
            this.txtNumReports.TabIndex = 1;
            // 
            // chkSelectNumReports
            // 
            this.chkSelectNumReports.AutoSize = true;
            this.chkSelectNumReports.Location = new System.Drawing.Point(4, 18);
            this.chkSelectNumReports.Name = "chkSelectNumReports";
            this.chkSelectNumReports.Size = new System.Drawing.Size(176, 17);
            this.chkSelectNumReports.TabIndex = 0;
            this.chkSelectNumReports.Text = "Max number of reports to return:";
            this.chkSelectNumReports.UseVisualStyleBackColor = true;
            this.chkSelectNumReports.CheckedChanged += new System.EventHandler(this.chkSelectNumReports_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 261);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Recommender Name:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 293);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Destination Path:";
            // 
            // txtRecommenderName
            // 
            this.txtRecommenderName.Location = new System.Drawing.Point(128, 258);
            this.txtRecommenderName.Name = "txtRecommenderName";
            this.txtRecommenderName.Size = new System.Drawing.Size(443, 20);
            this.txtRecommenderName.TabIndex = 2;
            // 
            // txtDestinationPath
            // 
            this.txtDestinationPath.Location = new System.Drawing.Point(106, 290);
            this.txtDestinationPath.Name = "txtDestinationPath";
            this.txtDestinationPath.Size = new System.Drawing.Size(465, 20);
            this.txtDestinationPath.TabIndex = 3;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(596, 288);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(98, 23);
            this.btnBrowse.TabIndex = 4;
            this.btnBrowse.Text = "Browse...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // chkSpecifyProject
            // 
            this.chkSpecifyProject.AutoSize = true;
            this.chkSpecifyProject.Location = new System.Drawing.Point(3, 51);
            this.chkSpecifyProject.Name = "chkSpecifyProject";
            this.chkSpecifyProject.Size = new System.Drawing.Size(176, 17);
            this.chkSpecifyProject.TabIndex = 2;
            this.chkSpecifyProject.Text = "Pull data from a specific project:";
            this.chkSpecifyProject.UseVisualStyleBackColor = true;
            this.chkSpecifyProject.CheckedChanged += new System.EventHandler(this.chkSpecifyProject_CheckedChanged);
            // 
            // cmbProjectName
            // 
            this.cmbProjectName.Enabled = false;
            this.cmbProjectName.FormattingEnabled = true;
            this.cmbProjectName.Location = new System.Drawing.Point(179, 49);
            this.cmbProjectName.Name = "cmbProjectName";
            this.cmbProjectName.Size = new System.Drawing.Size(151, 21);
            this.cmbProjectName.TabIndex = 3;
            // 
            // QueryBuilder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 351);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtDestinationPath);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtRecommenderName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pnlSelection);
            this.Controls.Add(this.chklResolution);
            this.Controls.Add(this.label5);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "QueryBuilder";
            this.Text = "Query Builder";
            this.pnlSelection.ResumeLayout(false);
            this.pnlSelection.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnlSelection;
        private System.Windows.Forms.DateTimePicker datEnd;
        private System.Windows.Forms.DateTimePicker datStart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.CheckedListBox chklResolution;
        private System.Windows.Forms.RadioButton radLast10;
        private System.Windows.Forms.RadioButton radLast5;
        private System.Windows.Forms.RadioButton radLastYear;
        private System.Windows.Forms.RadioButton radDateRange;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtNumReports;
        private System.Windows.Forms.CheckBox chkSelectNumReports;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtDestinationPath;
        private System.Windows.Forms.TextBox txtRecommenderName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.FolderBrowserDialog folderBrowser;
        private System.Windows.Forms.ComboBox cmbProjectName;
        private System.Windows.Forms.CheckBox chkSpecifyProject;
    }
}