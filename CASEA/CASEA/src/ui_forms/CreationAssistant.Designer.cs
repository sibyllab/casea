using System;
using CASEA.src.functions;
namespace CASEA.src.ui_forms
{
    partial class CreationAssistant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title3 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title4 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series12 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title5 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreationAssistant));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.configurationTab = new System.Windows.Forms.TabPage();
            this.autoConfigButton = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.mlAlgorithmSelect = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.heuristicsPanelTop = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.heuristicsChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.heuristicsOptionsPanel = new System.Windows.Forms.Panel();
            this.otherSettingsLabel = new System.Windows.Forms.Label();
            this.numOfHeuristicsBox = new System.Windows.Forms.ComboBox();
            this.numHeuristicsLabel = new System.Windows.Forms.Label();
            this.otherHeuristicBox = new System.Windows.Forms.ComboBox();
            this.otherLabel = new System.Windows.Forms.Label();
            this.heuristicsPanel = new System.Windows.Forms.Panel();
            this.percentageLabel10 = new System.Windows.Forms.Label();
            this.percentageLabel9 = new System.Windows.Forms.Label();
            this.percentageLabel8 = new System.Windows.Forms.Label();
            this.percentageLabel7 = new System.Windows.Forms.Label();
            this.percentageLabel6 = new System.Windows.Forms.Label();
            this.percentageLabel5 = new System.Windows.Forms.Label();
            this.percentageLabel4 = new System.Windows.Forms.Label();
            this.percentageLabel3 = new System.Windows.Forms.Label();
            this.percentageLabel2 = new System.Windows.Forms.Label();
            this.percentageLabel1 = new System.Windows.Forms.Label();
            this.pathPercentageLabel = new System.Windows.Forms.Label();
            this.pathLabel10 = new System.Windows.Forms.Label();
            this.pathLabel9 = new System.Windows.Forms.Label();
            this.pathLabel8 = new System.Windows.Forms.Label();
            this.pathLabel7 = new System.Windows.Forms.Label();
            this.pathLabel6 = new System.Windows.Forms.Label();
            this.pathLabel5 = new System.Windows.Forms.Label();
            this.pathLabel4 = new System.Windows.Forms.Label();
            this.pathLabel3 = new System.Windows.Forms.Label();
            this.pathLabel2 = new System.Windows.Forms.Label();
            this.pathLabel1 = new System.Windows.Forms.Label();
            this.pathGroupLabel = new System.Windows.Forms.Label();
            this.dataSourceLabel = new System.Windows.Forms.Label();
            this.heuristicsBox10 = new System.Windows.Forms.ComboBox();
            this.heuristicsBox9 = new System.Windows.Forms.ComboBox();
            this.heuristicsBox8 = new System.Windows.Forms.ComboBox();
            this.heuristicsBox7 = new System.Windows.Forms.ComboBox();
            this.heuristicsBox6 = new System.Windows.Forms.ComboBox();
            this.heuristicsBox5 = new System.Windows.Forms.ComboBox();
            this.heuristicsBox4 = new System.Windows.Forms.ComboBox();
            this.heuristicsBox3 = new System.Windows.Forms.ComboBox();
            this.heuristicsBox2 = new System.Windows.Forms.ComboBox();
            this.heuristicsBox1 = new System.Windows.Forms.ComboBox();
            this.heuristicsLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkShowXLabels = new System.Windows.Forms.CheckBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.frequencyChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.fcValue = new System.Windows.Forms.Label();
            this.frequencySelectBar = new System.Windows.Forms.TrackBar();
            this.fcLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.classesBox = new System.Windows.Forms.RichTextBox();
            this.analysisTab = new System.Windows.Forms.TabPage();
            this.chkAllData = new System.Windows.Forms.CheckBox();
            this.chkF1 = new System.Windows.Forms.CheckBox();
            this.chkRecall = new System.Windows.Forms.CheckBox();
            this.chkPrecision = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnApplyNumSamples = new System.Windows.Forms.Button();
            this.cbShowRecommendations = new System.Windows.Forms.ComboBox();
            this.lblShowRecommendations = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.sampleRecommendations = new System.Windows.Forms.TextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.top1Chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.top3Chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.top5Chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.historyChart = new System.Windows.Forms.Panel();
            this.historyLabel10 = new System.Windows.Forms.Label();
            this.historyLabel9 = new System.Windows.Forms.Label();
            this.historyLabel8 = new System.Windows.Forms.Label();
            this.historyLabel7 = new System.Windows.Forms.Label();
            this.historyLabel6 = new System.Windows.Forms.Label();
            this.historyLabel5 = new System.Windows.Forms.Label();
            this.historyLabel4 = new System.Windows.Forms.Label();
            this.historyLabel3 = new System.Windows.Forms.Label();
            this.historyLabel2 = new System.Windows.Forms.Label();
            this.historyLabel1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.statusBox = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backgroundProcess = new System.ComponentModel.BackgroundWorker();
            this.label8 = new System.Windows.Forms.Label();
            this.recommenderSelectionComboBox = new System.Windows.Forms.ComboBox();
            this.frequencyChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chkShowXLabels = new System.Windows.Forms.CheckBox();
            this.tooGraphs = new System.Windows.Forms.ToolTip(this.components);
            this.disableTooltipsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1.SuspendLayout();
            this.configurationTab.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.configurationTab.SuspendLayout();
            this.heuristicsPanelTop.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.heuristicsChart)).BeginInit();
            this.heuristicsOptionsPanel.SuspendLayout();
            this.heuristicsPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frequencyChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frequencySelectBar)).BeginInit();
            this.panel3.SuspendLayout();
            this.analysisTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.top1Chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.top3Chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.top5Chart)).BeginInit();
            this.historyChart.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.configurationTab);
            this.tabControl1.Controls.Add(this.analysisTab);
            this.tabControl1.Location = new System.Drawing.Point(0, 27);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1087, 558);
            this.tabControl1.TabIndex = 0;
            // 
            // configurationTab
            // 
            this.configurationTab.BackColor = System.Drawing.SystemColors.Control;
            this.configurationTab.Controls.Add(this.autoConfigButton);
            this.configurationTab.Controls.Add(this.label7);
            this.configurationTab.Controls.Add(this.mlAlgorithmSelect);
            this.configurationTab.Controls.Add(this.button1);
            this.configurationTab.Controls.Add(this.heuristicsPanelTop);
            this.configurationTab.Controls.Add(this.panel1);
            this.configurationTab.Location = new System.Drawing.Point(4, 22);
            this.configurationTab.Name = "configurationTab";
            this.configurationTab.Padding = new System.Windows.Forms.Padding(3);
            this.configurationTab.Size = new System.Drawing.Size(1079, 532);
            this.configurationTab.TabIndex = 1;
            this.configurationTab.Text = "Configuration";
            // 
            // autoConfigButton
            // 
            this.autoConfigButton.Location = new System.Drawing.Point(8, 505);
            this.autoConfigButton.Name = "autoConfigButton";
            this.autoConfigButton.Size = new System.Drawing.Size(135, 23);
            this.autoConfigButton.TabIndex = 5;
            this.autoConfigButton.Text = "Automatic Configuration";
            this.autoConfigButton.UseVisualStyleBackColor = true;
            this.autoConfigButton.Click += new System.EventHandler(this.autoConfigButton_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(721, 514);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Algorithm";
            // 
            // mlAlgorithmSelect
            // 
            this.mlAlgorithmSelect.FormattingEnabled = true;
            this.mlAlgorithmSelect.Location = new System.Drawing.Point(777, 508);
            this.mlAlgorithmSelect.Name = "mlAlgorithmSelect";
            this.mlAlgorithmSelect.Size = new System.Drawing.Size(121, 21);
            this.mlAlgorithmSelect.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(904, 506);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(175, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Build Recommender";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.buildRecommender_Click);
            // 
            // heuristicsPanelTop
            // 
            this.heuristicsPanelTop.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.heuristicsPanelTop.BackColor = System.Drawing.SystemColors.Control;
            this.heuristicsPanelTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.heuristicsPanelTop.Controls.Add(this.panel4);
            this.heuristicsPanelTop.Controls.Add(this.heuristicsOptionsPanel);
            this.heuristicsPanelTop.Controls.Add(this.heuristicsPanel);
            this.heuristicsPanelTop.Controls.Add(this.heuristicsLabel);
            this.heuristicsPanelTop.Location = new System.Drawing.Point(0, 0);
            this.heuristicsPanelTop.Name = "heuristicsPanelTop";
            this.heuristicsPanelTop.Size = new System.Drawing.Size(1078, 235);
            this.heuristicsPanelTop.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.heuristicsChart);
            this.panel4.Location = new System.Drawing.Point(614, 19);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(456, 210);
            this.panel4.TabIndex = 5;
            // 
            // heuristicsChart
            // 
            chartArea1.Name = "ChartArea1";
            this.heuristicsChart.ChartAreas.Add(chartArea1);
            this.heuristicsChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.heuristicsChart.Legends.Add(legend1);
            this.heuristicsChart.Location = new System.Drawing.Point(0, 0);
            this.heuristicsChart.Name = "heuristicsChart";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series1.Legend = "Legend1";
            series1.Name = "categoryChart";
            this.heuristicsChart.Series.Add(series1);
            this.heuristicsChart.Size = new System.Drawing.Size(454, 208);
            this.heuristicsChart.TabIndex = 0;
            this.heuristicsChart.Text = "chart2";
            title1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title1.Name = "Title";
            title1.Text = "Resolution Distribution";
            this.heuristicsChart.Titles.Add(title1);
            // 
            // heuristicsOptionsPanel
            // 
            this.heuristicsOptionsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.heuristicsOptionsPanel.AutoScroll = true;
            this.heuristicsOptionsPanel.BackColor = System.Drawing.Color.White;
            this.heuristicsOptionsPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.heuristicsOptionsPanel.Controls.Add(this.otherSettingsLabel);
            this.heuristicsOptionsPanel.Controls.Add(this.numOfHeuristicsBox);
            this.heuristicsOptionsPanel.Controls.Add(this.numHeuristicsLabel);
            this.heuristicsOptionsPanel.Controls.Add(this.otherHeuristicBox);
            this.heuristicsOptionsPanel.Controls.Add(this.otherLabel);
            this.heuristicsOptionsPanel.Location = new System.Drawing.Point(8, 21);
            this.heuristicsOptionsPanel.Name = "heuristicsOptionsPanel";
            this.heuristicsOptionsPanel.Size = new System.Drawing.Size(205, 210);
            this.heuristicsOptionsPanel.TabIndex = 3;
            // 
            // otherSettingsLabel
            // 
            this.otherSettingsLabel.AutoSize = true;
            this.otherSettingsLabel.Location = new System.Drawing.Point(70, 4);
            this.otherSettingsLabel.Name = "otherSettingsLabel";
            this.otherSettingsLabel.Size = new System.Drawing.Size(74, 13);
            this.otherSettingsLabel.TabIndex = 4;
            this.otherSettingsLabel.Text = "Other Settings";
            // 
            // numOfHeuristicsBox
            // 
            this.numOfHeuristicsBox.FormattingEnabled = true;
            this.numOfHeuristicsBox.Location = new System.Drawing.Point(117, 29);
            this.numOfHeuristicsBox.Name = "numOfHeuristicsBox";
            this.numOfHeuristicsBox.Size = new System.Drawing.Size(78, 21);
            this.numOfHeuristicsBox.TabIndex = 3;
            this.numOfHeuristicsBox.Text = "5";
            this.numOfHeuristicsBox.ValueMemberChanged += new System.EventHandler(this.numOfHeuristicsBox_ValueMemberChanged);
            this.numOfHeuristicsBox.TextChanged += new System.EventHandler(this.numOfHeuristicsBox_ValueMemberChanged);
            // 
            // numHeuristicsLabel
            // 
            this.numHeuristicsLabel.AutoSize = true;
            this.numHeuristicsLabel.Location = new System.Drawing.Point(3, 32);
            this.numHeuristicsLabel.Name = "numHeuristicsLabel";
            this.numHeuristicsLabel.Size = new System.Drawing.Size(108, 13);
            this.numHeuristicsLabel.TabIndex = 2;
            this.numHeuristicsLabel.Text = "Number of Heuristics:";
            // 
            // otherHeuristicBox
            // 
            this.otherHeuristicBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.otherHeuristicBox.FormattingEnabled = true;
            this.otherHeuristicBox.Location = new System.Drawing.Point(89, 53);
            this.otherHeuristicBox.Name = "otherHeuristicBox";
            this.otherHeuristicBox.Size = new System.Drawing.Size(106, 21);
            this.otherHeuristicBox.TabIndex = 1;
            this.otherHeuristicBox.TextChanged += new System.EventHandler(this.heuristicBox_TextChanged);
            // 
            // otherLabel
            // 
            this.otherLabel.AutoSize = true;
            this.otherLabel.Location = new System.Drawing.Point(3, 56);
            this.otherLabel.Name = "otherLabel";
            this.otherLabel.Size = new System.Drawing.Size(85, 13);
            this.otherLabel.TabIndex = 0;
            this.otherLabel.Text = "Other Heuristics:";
            // 
            // heuristicsPanel
            // 
            this.heuristicsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.heuristicsPanel.AutoScroll = true;
            this.heuristicsPanel.BackColor = System.Drawing.Color.White;
            this.heuristicsPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.heuristicsPanel.Controls.Add(this.percentageLabel10);
            this.heuristicsPanel.Controls.Add(this.percentageLabel9);
            this.heuristicsPanel.Controls.Add(this.percentageLabel8);
            this.heuristicsPanel.Controls.Add(this.percentageLabel7);
            this.heuristicsPanel.Controls.Add(this.percentageLabel6);
            this.heuristicsPanel.Controls.Add(this.percentageLabel5);
            this.heuristicsPanel.Controls.Add(this.percentageLabel4);
            this.heuristicsPanel.Controls.Add(this.percentageLabel3);
            this.heuristicsPanel.Controls.Add(this.percentageLabel2);
            this.heuristicsPanel.Controls.Add(this.percentageLabel1);
            this.heuristicsPanel.Controls.Add(this.pathPercentageLabel);
            this.heuristicsPanel.Controls.Add(this.pathLabel10);
            this.heuristicsPanel.Controls.Add(this.pathLabel9);
            this.heuristicsPanel.Controls.Add(this.pathLabel8);
            this.heuristicsPanel.Controls.Add(this.pathLabel7);
            this.heuristicsPanel.Controls.Add(this.pathLabel6);
            this.heuristicsPanel.Controls.Add(this.pathLabel5);
            this.heuristicsPanel.Controls.Add(this.pathLabel4);
            this.heuristicsPanel.Controls.Add(this.pathLabel3);
            this.heuristicsPanel.Controls.Add(this.pathLabel2);
            this.heuristicsPanel.Controls.Add(this.pathLabel1);
            this.heuristicsPanel.Controls.Add(this.pathGroupLabel);
            this.heuristicsPanel.Controls.Add(this.dataSourceLabel);
            this.heuristicsPanel.Controls.Add(this.heuristicsBox10);
            this.heuristicsPanel.Controls.Add(this.heuristicsBox9);
            this.heuristicsPanel.Controls.Add(this.heuristicsBox8);
            this.heuristicsPanel.Controls.Add(this.heuristicsBox7);
            this.heuristicsPanel.Controls.Add(this.heuristicsBox6);
            this.heuristicsPanel.Controls.Add(this.heuristicsBox5);
            this.heuristicsPanel.Controls.Add(this.heuristicsBox4);
            this.heuristicsPanel.Controls.Add(this.heuristicsBox3);
            this.heuristicsPanel.Controls.Add(this.heuristicsBox2);
            this.heuristicsPanel.Controls.Add(this.heuristicsBox1);
            this.heuristicsPanel.Location = new System.Drawing.Point(219, 19);
            this.heuristicsPanel.Name = "heuristicsPanel";
            this.heuristicsPanel.Size = new System.Drawing.Size(389, 210);
            this.heuristicsPanel.TabIndex = 1;
            // 
            // percentageLabel10
            // 
            this.percentageLabel10.AutoSize = true;
            this.percentageLabel10.Location = new System.Drawing.Point(145, 278);
            this.percentageLabel10.Name = "percentageLabel10";
            this.percentageLabel10.Size = new System.Drawing.Size(15, 13);
            this.percentageLabel10.TabIndex = 33;
            this.percentageLabel10.Text = "%";
            this.percentageLabel10.Visible = false;
            // 
            // percentageLabel9
            // 
            this.percentageLabel9.AutoSize = true;
            this.percentageLabel9.Location = new System.Drawing.Point(145, 251);
            this.percentageLabel9.Name = "percentageLabel9";
            this.percentageLabel9.Size = new System.Drawing.Size(15, 13);
            this.percentageLabel9.TabIndex = 32;
            this.percentageLabel9.Text = "%";
            this.percentageLabel9.Visible = false;
            // 
            // percentageLabel8
            // 
            this.percentageLabel8.AutoSize = true;
            this.percentageLabel8.Location = new System.Drawing.Point(145, 226);
            this.percentageLabel8.Name = "percentageLabel8";
            this.percentageLabel8.Size = new System.Drawing.Size(15, 13);
            this.percentageLabel8.TabIndex = 31;
            this.percentageLabel8.Text = "%";
            this.percentageLabel8.Visible = false;
            // 
            // percentageLabel7
            // 
            this.percentageLabel7.AutoSize = true;
            this.percentageLabel7.Location = new System.Drawing.Point(145, 199);
            this.percentageLabel7.Name = "percentageLabel7";
            this.percentageLabel7.Size = new System.Drawing.Size(15, 13);
            this.percentageLabel7.TabIndex = 30;
            this.percentageLabel7.Text = "%";
            this.percentageLabel7.Visible = false;
            // 
            // percentageLabel6
            // 
            this.percentageLabel6.AutoSize = true;
            this.percentageLabel6.Location = new System.Drawing.Point(145, 171);
            this.percentageLabel6.Name = "percentageLabel6";
            this.percentageLabel6.Size = new System.Drawing.Size(15, 13);
            this.percentageLabel6.TabIndex = 29;
            this.percentageLabel6.Text = "%";
            this.percentageLabel6.Visible = false;
            // 
            // percentageLabel5
            // 
            this.percentageLabel5.AutoSize = true;
            this.percentageLabel5.Location = new System.Drawing.Point(145, 144);
            this.percentageLabel5.Name = "percentageLabel5";
            this.percentageLabel5.Size = new System.Drawing.Size(15, 13);
            this.percentageLabel5.TabIndex = 28;
            this.percentageLabel5.Text = "%";
            this.percentageLabel5.Visible = false;
            // 
            // percentageLabel4
            // 
            this.percentageLabel4.AutoSize = true;
            this.percentageLabel4.Location = new System.Drawing.Point(145, 117);
            this.percentageLabel4.Name = "percentageLabel4";
            this.percentageLabel4.Size = new System.Drawing.Size(15, 13);
            this.percentageLabel4.TabIndex = 27;
            this.percentageLabel4.Text = "%";
            this.percentageLabel4.Visible = false;
            // 
            // percentageLabel3
            // 
            this.percentageLabel3.AutoSize = true;
            this.percentageLabel3.Location = new System.Drawing.Point(145, 90);
            this.percentageLabel3.Name = "percentageLabel3";
            this.percentageLabel3.Size = new System.Drawing.Size(15, 13);
            this.percentageLabel3.TabIndex = 26;
            this.percentageLabel3.Text = "%";
            this.percentageLabel3.Visible = false;
            // 
            // percentageLabel2
            // 
            this.percentageLabel2.AutoSize = true;
            this.percentageLabel2.Location = new System.Drawing.Point(145, 62);
            this.percentageLabel2.Name = "percentageLabel2";
            this.percentageLabel2.Size = new System.Drawing.Size(15, 13);
            this.percentageLabel2.TabIndex = 25;
            this.percentageLabel2.Text = "%";
            this.percentageLabel2.Visible = false;
            // 
            // percentageLabel1
            // 
            this.percentageLabel1.AutoSize = true;
            this.percentageLabel1.Location = new System.Drawing.Point(145, 35);
            this.percentageLabel1.Name = "percentageLabel1";
            this.percentageLabel1.Size = new System.Drawing.Size(15, 13);
            this.percentageLabel1.TabIndex = 24;
            this.percentageLabel1.Text = "%";
            this.percentageLabel1.Visible = false;
            // 
            // pathPercentageLabel
            // 
            this.pathPercentageLabel.AutoSize = true;
            this.pathPercentageLabel.Location = new System.Drawing.Point(129, 4);
            this.pathPercentageLabel.Name = "pathPercentageLabel";
            this.pathPercentageLabel.Size = new System.Drawing.Size(59, 13);
            this.pathPercentageLabel.TabIndex = 23;
            this.pathPercentageLabel.Text = "Distribution";
            // 
            // pathLabel10
            // 
            this.pathLabel10.AutoSize = true;
            this.pathLabel10.Location = new System.Drawing.Point(39, 278);
            this.pathLabel10.Name = "pathLabel10";
            this.pathLabel10.Size = new System.Drawing.Size(28, 13);
            this.pathLabel10.TabIndex = 22;
            this.pathLabel10.Text = "path";
            this.pathLabel10.Visible = false;
            // 
            // pathLabel9
            // 
            this.pathLabel9.AutoSize = true;
            this.pathLabel9.Location = new System.Drawing.Point(39, 251);
            this.pathLabel9.Name = "pathLabel9";
            this.pathLabel9.Size = new System.Drawing.Size(28, 13);
            this.pathLabel9.TabIndex = 21;
            this.pathLabel9.Text = "path";
            this.pathLabel9.Visible = false;
            // 
            // pathLabel8
            // 
            this.pathLabel8.AutoSize = true;
            this.pathLabel8.Location = new System.Drawing.Point(39, 226);
            this.pathLabel8.Name = "pathLabel8";
            this.pathLabel8.Size = new System.Drawing.Size(28, 13);
            this.pathLabel8.TabIndex = 20;
            this.pathLabel8.Text = "path";
            this.pathLabel8.Visible = false;
            // 
            // pathLabel7
            // 
            this.pathLabel7.AutoSize = true;
            this.pathLabel7.Location = new System.Drawing.Point(39, 199);
            this.pathLabel7.Name = "pathLabel7";
            this.pathLabel7.Size = new System.Drawing.Size(28, 13);
            this.pathLabel7.TabIndex = 19;
            this.pathLabel7.Text = "path";
            this.pathLabel7.Visible = false;
            // 
            // pathLabel6
            // 
            this.pathLabel6.AutoSize = true;
            this.pathLabel6.Location = new System.Drawing.Point(39, 171);
            this.pathLabel6.Name = "pathLabel6";
            this.pathLabel6.Size = new System.Drawing.Size(28, 13);
            this.pathLabel6.TabIndex = 18;
            this.pathLabel6.Text = "path";
            this.pathLabel6.Visible = false;
            // 
            // pathLabel5
            // 
            this.pathLabel5.AutoSize = true;
            this.pathLabel5.Location = new System.Drawing.Point(39, 144);
            this.pathLabel5.Name = "pathLabel5";
            this.pathLabel5.Size = new System.Drawing.Size(28, 13);
            this.pathLabel5.TabIndex = 17;
            this.pathLabel5.Text = "path";
            this.pathLabel5.Visible = false;
            // 
            // pathLabel4
            // 
            this.pathLabel4.AutoSize = true;
            this.pathLabel4.Location = new System.Drawing.Point(39, 117);
            this.pathLabel4.Name = "pathLabel4";
            this.pathLabel4.Size = new System.Drawing.Size(28, 13);
            this.pathLabel4.TabIndex = 16;
            this.pathLabel4.Text = "path";
            this.pathLabel4.Visible = false;
            // 
            // pathLabel3
            // 
            this.pathLabel3.AutoSize = true;
            this.pathLabel3.Location = new System.Drawing.Point(39, 90);
            this.pathLabel3.Name = "pathLabel3";
            this.pathLabel3.Size = new System.Drawing.Size(28, 13);
            this.pathLabel3.TabIndex = 15;
            this.pathLabel3.Text = "path";
            this.pathLabel3.Visible = false;
            // 
            // pathLabel2
            // 
            this.pathLabel2.AutoSize = true;
            this.pathLabel2.Location = new System.Drawing.Point(39, 62);
            this.pathLabel2.Name = "pathLabel2";
            this.pathLabel2.Size = new System.Drawing.Size(28, 13);
            this.pathLabel2.TabIndex = 14;
            this.pathLabel2.Text = "path";
            this.pathLabel2.Visible = false;
            // 
            // pathLabel1
            // 
            this.pathLabel1.AutoSize = true;
            this.pathLabel1.Location = new System.Drawing.Point(39, 35);
            this.pathLabel1.Name = "pathLabel1";
            this.pathLabel1.Size = new System.Drawing.Size(28, 13);
            this.pathLabel1.TabIndex = 13;
            this.pathLabel1.Text = "path";
            this.pathLabel1.Visible = false;
            // 
            // pathGroupLabel
            // 
            this.pathGroupLabel.AutoSize = true;
            this.pathGroupLabel.Location = new System.Drawing.Point(23, 4);
            this.pathGroupLabel.Name = "pathGroupLabel";
            this.pathGroupLabel.Size = new System.Drawing.Size(89, 13);
            this.pathGroupLabel.TabIndex = 12;
            this.pathGroupLabel.Text = "Resolution Group";
            // 
            // dataSourceLabel
            // 
            this.dataSourceLabel.AutoSize = true;
            this.dataSourceLabel.Location = new System.Drawing.Point(254, 4);
            this.dataSourceLabel.Name = "dataSourceLabel";
            this.dataSourceLabel.Size = new System.Drawing.Size(70, 13);
            this.dataSourceLabel.TabIndex = 11;
            this.dataSourceLabel.Text = "Label Source";
            // 
            // heuristicsBox10
            // 
            this.heuristicsBox10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.heuristicsBox10.FormattingEnabled = true;
            this.heuristicsBox10.Items.AddRange(new object[] {
            CASEA.src.functions.HeuristicEnum.AssignedTo,
            CASEA.src.functions.HeuristicEnum.FixedBy,
            CASEA.src.functions.HeuristicEnum.Resolver,
            CASEA.src.functions.HeuristicEnum.Reporter,
            CASEA.src.functions.HeuristicEnum.FirstResponder,
            CASEA.src.functions.HeuristicEnum.Unused});
            this.heuristicsBox10.Location = new System.Drawing.Point(227, 275);
            this.heuristicsBox10.Name = "heuristicsBox10";
            this.heuristicsBox10.Size = new System.Drawing.Size(121, 21);
            this.heuristicsBox10.TabIndex = 9;
            this.heuristicsBox10.Visible = false;
            this.heuristicsBox10.TextChanged += new System.EventHandler(this.heuristicBox_TextChanged);
            // 
            // heuristicsBox9
            // 
            this.heuristicsBox9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.heuristicsBox9.FormattingEnabled = true;
            this.heuristicsBox9.Items.AddRange(new object[] {
            CASEA.src.functions.HeuristicEnum.AssignedTo,
            CASEA.src.functions.HeuristicEnum.FixedBy,
            CASEA.src.functions.HeuristicEnum.Resolver,
            CASEA.src.functions.HeuristicEnum.Reporter,
            CASEA.src.functions.HeuristicEnum.FirstResponder,
            CASEA.src.functions.HeuristicEnum.Unused});
            this.heuristicsBox9.Location = new System.Drawing.Point(227, 248);
            this.heuristicsBox9.Name = "heuristicsBox9";
            this.heuristicsBox9.Size = new System.Drawing.Size(121, 21);
            this.heuristicsBox9.TabIndex = 8;
            this.heuristicsBox9.Visible = false;
            this.heuristicsBox9.TextChanged += new System.EventHandler(this.heuristicBox_TextChanged);
            // 
            // heuristicsBox8
            // 
            this.heuristicsBox8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.heuristicsBox8.FormattingEnabled = true;
            this.heuristicsBox8.Items.AddRange(new object[] {
            CASEA.src.functions.HeuristicEnum.AssignedTo,
            CASEA.src.functions.HeuristicEnum.FixedBy,
            CASEA.src.functions.HeuristicEnum.Resolver,
            CASEA.src.functions.HeuristicEnum.Reporter,
            CASEA.src.functions.HeuristicEnum.FirstResponder,
            CASEA.src.functions.HeuristicEnum.Unused});
            this.heuristicsBox8.Location = new System.Drawing.Point(227, 223);
            this.heuristicsBox8.Name = "heuristicsBox8";
            this.heuristicsBox8.Size = new System.Drawing.Size(121, 21);
            this.heuristicsBox8.TabIndex = 7;
            this.heuristicsBox8.Visible = false;
            this.heuristicsBox8.TextChanged += new System.EventHandler(this.heuristicBox_TextChanged);
            // 
            // heuristicsBox7
            // 
            this.heuristicsBox7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.heuristicsBox7.FormattingEnabled = true;
            this.heuristicsBox7.Items.AddRange(new object[] {
            CASEA.src.functions.HeuristicEnum.AssignedTo,
            CASEA.src.functions.HeuristicEnum.FixedBy,
            CASEA.src.functions.HeuristicEnum.Resolver,
            CASEA.src.functions.HeuristicEnum.Reporter,
            CASEA.src.functions.HeuristicEnum.FirstResponder,
            CASEA.src.functions.HeuristicEnum.Unused});
            this.heuristicsBox7.Location = new System.Drawing.Point(227, 196);
            this.heuristicsBox7.Name = "heuristicsBox7";
            this.heuristicsBox7.Size = new System.Drawing.Size(121, 21);
            this.heuristicsBox7.TabIndex = 6;
            this.heuristicsBox7.Visible = false;
            this.heuristicsBox7.TextChanged += new System.EventHandler(this.heuristicBox_TextChanged);
            // 
            // heuristicsBox6
            // 
            this.heuristicsBox6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.heuristicsBox6.FormattingEnabled = true;
            this.heuristicsBox6.Items.AddRange(new object[] {
            CASEA.src.functions.HeuristicEnum.AssignedTo,
            CASEA.src.functions.HeuristicEnum.FixedBy,
            CASEA.src.functions.HeuristicEnum.Resolver,
            CASEA.src.functions.HeuristicEnum.Reporter,
            CASEA.src.functions.HeuristicEnum.FirstResponder,
            CASEA.src.functions.HeuristicEnum.Unused});
            this.heuristicsBox6.Location = new System.Drawing.Point(227, 168);
            this.heuristicsBox6.Name = "heuristicsBox6";
            this.heuristicsBox6.Size = new System.Drawing.Size(121, 21);
            this.heuristicsBox6.TabIndex = 5;
            this.heuristicsBox6.Visible = false;
            this.heuristicsBox6.TextChanged += new System.EventHandler(this.heuristicBox_TextChanged);
            // 
            // heuristicsBox5
            // 
            this.heuristicsBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.heuristicsBox5.FormattingEnabled = true;
            this.heuristicsBox5.Items.AddRange(new object[] {
            CASEA.src.functions.HeuristicEnum.AssignedTo,
            CASEA.src.functions.HeuristicEnum.FixedBy,
            CASEA.src.functions.HeuristicEnum.Resolver,
            CASEA.src.functions.HeuristicEnum.Reporter,
            CASEA.src.functions.HeuristicEnum.FirstResponder,
            CASEA.src.functions.HeuristicEnum.Unused});
            this.heuristicsBox5.Location = new System.Drawing.Point(227, 141);
            this.heuristicsBox5.Name = "heuristicsBox5";
            this.heuristicsBox5.Size = new System.Drawing.Size(121, 21);
            this.heuristicsBox5.TabIndex = 4;
            this.heuristicsBox5.Visible = false;
            this.heuristicsBox5.TextChanged += new System.EventHandler(this.heuristicBox_TextChanged);
            // 
            // heuristicsBox4
            // 
            this.heuristicsBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.heuristicsBox4.FormattingEnabled = true;
            this.heuristicsBox4.Items.AddRange(new object[] {
            CASEA.src.functions.HeuristicEnum.AssignedTo,
            CASEA.src.functions.HeuristicEnum.FixedBy,
            CASEA.src.functions.HeuristicEnum.Resolver,
            CASEA.src.functions.HeuristicEnum.Reporter,
            CASEA.src.functions.HeuristicEnum.FirstResponder,
            CASEA.src.functions.HeuristicEnum.Unused});
            this.heuristicsBox4.Location = new System.Drawing.Point(227, 114);
            this.heuristicsBox4.Name = "heuristicsBox4";
            this.heuristicsBox4.Size = new System.Drawing.Size(121, 21);
            this.heuristicsBox4.TabIndex = 3;
            this.heuristicsBox4.Visible = false;
            this.heuristicsBox4.TextChanged += new System.EventHandler(this.heuristicBox_TextChanged);
            // 
            // heuristicsBox3
            // 
            this.heuristicsBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.heuristicsBox3.FormattingEnabled = true;
            this.heuristicsBox3.Items.AddRange(new object[] {
            CASEA.src.functions.HeuristicEnum.AssignedTo,
            CASEA.src.functions.HeuristicEnum.FixedBy,
            CASEA.src.functions.HeuristicEnum.Resolver,
            CASEA.src.functions.HeuristicEnum.Reporter,
            CASEA.src.functions.HeuristicEnum.FirstResponder,
            CASEA.src.functions.HeuristicEnum.Unused});
            this.heuristicsBox3.Location = new System.Drawing.Point(227, 87);
            this.heuristicsBox3.Name = "heuristicsBox3";
            this.heuristicsBox3.Size = new System.Drawing.Size(121, 21);
            this.heuristicsBox3.TabIndex = 2;
            this.heuristicsBox3.Visible = false;
            this.heuristicsBox3.TextChanged += new System.EventHandler(this.heuristicBox_TextChanged);
            // 
            // heuristicsBox2
            // 
            this.heuristicsBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.heuristicsBox2.FormattingEnabled = true;
            this.heuristicsBox2.Items.AddRange(new object[] {
            CASEA.src.functions.HeuristicEnum.AssignedTo,
            CASEA.src.functions.HeuristicEnum.FixedBy,
            CASEA.src.functions.HeuristicEnum.Resolver,
            CASEA.src.functions.HeuristicEnum.Reporter,
            CASEA.src.functions.HeuristicEnum.FirstResponder,
            CASEA.src.functions.HeuristicEnum.Unused});
            this.heuristicsBox2.Location = new System.Drawing.Point(227, 59);
            this.heuristicsBox2.Name = "heuristicsBox2";
            this.heuristicsBox2.Size = new System.Drawing.Size(121, 21);
            this.heuristicsBox2.TabIndex = 1;
            this.heuristicsBox2.Visible = false;
            this.heuristicsBox2.TextChanged += new System.EventHandler(this.heuristicBox_TextChanged);
            // 
            // heuristicsBox1
            // 
            this.heuristicsBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.heuristicsBox1.FormattingEnabled = true;
            this.heuristicsBox1.Items.AddRange(new object[] {
            CASEA.src.functions.HeuristicEnum.AssignedTo,
            CASEA.src.functions.HeuristicEnum.FixedBy,
            CASEA.src.functions.HeuristicEnum.Resolver,
            CASEA.src.functions.HeuristicEnum.Reporter,
            CASEA.src.functions.HeuristicEnum.FirstResponder,
            CASEA.src.functions.HeuristicEnum.Unused});
            this.heuristicsBox1.Location = new System.Drawing.Point(227, 32);
            this.heuristicsBox1.Name = "heuristicsBox1";
            this.heuristicsBox1.Size = new System.Drawing.Size(121, 21);
            this.heuristicsBox1.TabIndex = 0;
            this.heuristicsBox1.Visible = false;
            this.heuristicsBox1.TextChanged += new System.EventHandler(this.heuristicBox_TextChanged);
            // 
            // heuristicsLabel
            // 
            this.heuristicsLabel.AutoSize = true;
            this.heuristicsLabel.Location = new System.Drawing.Point(4, 4);
            this.heuristicsLabel.Name = "heuristicsLabel";
            this.heuristicsLabel.Size = new System.Drawing.Size(53, 13);
            this.heuristicsLabel.TabIndex = 2;
            this.heuristicsLabel.Text = "Heuristics";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.chkShowXLabels);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.fcValue);
            this.panel1.Controls.Add(this.frequencySelectBar);
            this.panel1.Controls.Add(this.fcLabel);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Location = new System.Drawing.Point(0, 237);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1078, 265);
            this.panel1.TabIndex = 0;
            // 
            // chkShowXLabels
            // 
            this.chkShowXLabels.AutoSize = true;
            this.chkShowXLabels.Location = new System.Drawing.Point(302, 21);
            this.chkShowXLabels.Name = "chkShowXLabels";
            this.chkShowXLabels.Size = new System.Drawing.Size(119, 17);
            this.chkShowXLabels.TabIndex = 1;
            this.chkShowXLabels.Text = "Show X-Axis Labels";
            this.chkShowXLabels.UseVisualStyleBackColor = true;
            this.chkShowXLabels.CheckedChanged += new System.EventHandler(this.chkShowXLabels_CheckedChanged);
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.frequencyChart);
            this.panel5.Location = new System.Drawing.Point(302, 45);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(771, 215);
            this.panel5.TabIndex = 3;
            // 
            // frequencyChart
            // 
            chartArea2.AxisX.Interval = 1D;
            chartArea2.AxisX.LabelStyle.Enabled = false;
            chartArea2.AxisY.MajorGrid.Enabled = false;
            chartArea2.Name = "ChartArea1";
            this.frequencyChart.ChartAreas.Add(chartArea2);
            this.frequencyChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.frequencyChart.Location = new System.Drawing.Point(0, 0);
            this.frequencyChart.Name = "frequencyChart";
            series2.BorderWidth = 3;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Name = "frequency";
            series2.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.String;
            series3.BorderWidth = 2;
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Color = System.Drawing.Color.Red;
            series3.Name = "frequencyCutoff";
            this.frequencyChart.Series.Add(series2);
            this.frequencyChart.Series.Add(series3);
            this.frequencyChart.Size = new System.Drawing.Size(769, 213);
            this.frequencyChart.TabIndex = 0;
            this.frequencyChart.Text = "chart2";
            title2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title2.Name = "Title";
            this.frequencyChart.Titles.Add(title2);
            // 
            // fcValue
            // 
            this.fcValue.AutoSize = true;
            this.fcValue.Location = new System.Drawing.Point(271, 223);
            this.fcValue.Name = "fcValue";
            this.fcValue.Size = new System.Drawing.Size(13, 13);
            this.fcValue.TabIndex = 2;
            this.fcValue.Text = "5";
            // 
            // frequencySelectBar
            // 
            this.frequencySelectBar.BackColor = System.Drawing.SystemColors.Control;
            this.frequencySelectBar.Location = new System.Drawing.Point(249, 45);
            this.frequencySelectBar.Minimum = 1;
            this.frequencySelectBar.Name = "frequencySelectBar";
            this.frequencySelectBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.frequencySelectBar.Size = new System.Drawing.Size(45, 175);
            this.frequencySelectBar.TabIndex = 0;
            this.frequencySelectBar.Value = 5;
            this.frequencySelectBar.ValueChanged += new System.EventHandler(this.frequencySelectBar_ValueChanged);
            // 
            // fcLabel
            // 
            this.fcLabel.AutoSize = true;
            this.fcLabel.Location = new System.Drawing.Point(242, 223);
            this.fcLabel.Name = "fcLabel";
            this.fcLabel.Size = new System.Drawing.Size(23, 13);
            this.fcLabel.TabIndex = 1;
            this.fcLabel.Text = "FC:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Frequency Cutoff";
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.classesBox);
            this.panel3.Location = new System.Drawing.Point(7, 21);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(229, 239);
            this.panel3.TabIndex = 1;
            // 
            // classesBox
            // 
            this.classesBox.BackColor = System.Drawing.Color.White;
            this.classesBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.classesBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.classesBox.Location = new System.Drawing.Point(0, 0);
            this.classesBox.Name = "classesBox";
            this.classesBox.ReadOnly = true;
            this.classesBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.classesBox.Size = new System.Drawing.Size(227, 237);
            this.classesBox.TabIndex = 3;
            this.classesBox.Text = "Developer Information";
            // 
            // analysisTab
            // 
            this.analysisTab.BackColor = System.Drawing.SystemColors.Control;
            this.analysisTab.Controls.Add(this.chkAllData);
            this.analysisTab.Controls.Add(this.chkF1);
            this.analysisTab.Controls.Add(this.chkRecall);
            this.analysisTab.Controls.Add(this.chkPrecision);
            this.analysisTab.Controls.Add(this.label3);
            this.analysisTab.Controls.Add(this.btnApplyNumSamples);
            this.analysisTab.Controls.Add(this.cbShowRecommendations);
            this.analysisTab.Controls.Add(this.lblShowRecommendations);
            this.analysisTab.Controls.Add(this.label9);
            this.analysisTab.Controls.Add(this.sampleRecommendations);
            this.analysisTab.Controls.Add(this.splitContainer1);
            this.analysisTab.Controls.Add(this.historyChart);
            this.analysisTab.Controls.Add(this.statusBox);
            this.analysisTab.Location = new System.Drawing.Point(4, 22);
            this.analysisTab.Name = "analysisTab";
            this.analysisTab.Padding = new System.Windows.Forms.Padding(3);
            this.analysisTab.Size = new System.Drawing.Size(1079, 532);
            this.analysisTab.TabIndex = 0;
            this.analysisTab.Text = "Analysis";
            // 
            // chkAllData
            // 
            this.chkAllData.AutoSize = true;
            this.chkAllData.Checked = true;
            this.chkAllData.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAllData.Location = new System.Drawing.Point(372, 259);
            this.chkAllData.Name = "chkAllData";
            this.chkAllData.Size = new System.Drawing.Size(63, 17);
            this.chkAllData.TabIndex = 18;
            this.chkAllData.Text = "All Data";
            this.chkAllData.UseVisualStyleBackColor = true;
            this.chkAllData.CheckedChanged += new System.EventHandler(this.chkAllData_CheckedChanged);
            // 
            // chkF1
            // 
            this.chkF1.AutoSize = true;
            this.chkF1.Checked = true;
            this.chkF1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkF1.Enabled = false;
            this.chkF1.Location = new System.Drawing.Point(303, 259);
            this.chkF1.Name = "chkF1";
            this.chkF1.Size = new System.Drawing.Size(63, 17);
            this.chkF1.TabIndex = 17;
            this.chkF1.Text = "Avg. F1";
            this.chkF1.UseVisualStyleBackColor = true;
            this.chkF1.CheckedChanged += new System.EventHandler(this.chkF1_CheckedChanged);
            // 
            // chkRecall
            // 
            this.chkRecall.AutoSize = true;
            this.chkRecall.Checked = true;
            this.chkRecall.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRecall.Enabled = false;
            this.chkRecall.Location = new System.Drawing.Point(216, 259);
            this.chkRecall.Name = "chkRecall";
            this.chkRecall.Size = new System.Drawing.Size(81, 17);
            this.chkRecall.TabIndex = 16;
            this.chkRecall.Text = "Avg. Recall";
            this.chkRecall.UseVisualStyleBackColor = true;
            this.chkRecall.CheckedChanged += new System.EventHandler(this.chkRecall_CheckedChanged);
            // 
            // chkPrecision
            // 
            this.chkPrecision.AutoSize = true;
            this.chkPrecision.Checked = true;
            this.chkPrecision.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPrecision.Enabled = false;
            this.chkPrecision.Location = new System.Drawing.Point(116, 259);
            this.chkPrecision.Name = "chkPrecision";
            this.chkPrecision.Size = new System.Drawing.Size(94, 17);
            this.chkPrecision.TabIndex = 15;
            this.chkPrecision.Text = "Avg. Precision";
            this.chkPrecision.UseVisualStyleBackColor = true;
            this.chkPrecision.CheckedChanged += new System.EventHandler(this.chkPrecision_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 260);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Graph Data to Show:";
            // 
            // btnApplyNumSamples
            // 
            this.btnApplyNumSamples.Location = new System.Drawing.Point(866, 294);
            this.btnApplyNumSamples.Name = "btnApplyNumSamples";
            this.btnApplyNumSamples.Size = new System.Drawing.Size(69, 22);
            this.btnApplyNumSamples.TabIndex = 13;
            this.btnApplyNumSamples.Text = "Apply";
            this.btnApplyNumSamples.UseVisualStyleBackColor = true;
            this.btnApplyNumSamples.Click += new System.EventHandler(this.btnApplyNumSamples_Click);
            // 
            // cbShowRecommendations
            // 
            this.cbShowRecommendations.FormattingEnabled = true;
            this.cbShowRecommendations.Location = new System.Drawing.Point(810, 295);
            this.cbShowRecommendations.Name = "cbShowRecommendations";
            this.cbShowRecommendations.Size = new System.Drawing.Size(54, 21);
            this.cbShowRecommendations.TabIndex = 12;
            // 
            // lblShowRecommendations
            // 
            this.lblShowRecommendations.AutoSize = true;
            this.lblShowRecommendations.Location = new System.Drawing.Point(770, 298);
            this.lblShowRecommendations.Name = "lblShowRecommendations";
            this.lblShowRecommendations.Size = new System.Drawing.Size(34, 13);
            this.lblShowRecommendations.TabIndex = 11;
            this.lblShowRecommendations.Text = "Show";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.White;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(770, 268);
            this.label9.MinimumSize = new System.Drawing.Size(300, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(300, 20);
            this.label9.TabIndex = 10;
            this.label9.Text = "Sample Recommendations";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sampleRecommendations
            // 
            this.sampleRecommendations.BackColor = System.Drawing.SystemColors.InfoText;
            this.sampleRecommendations.ForeColor = System.Drawing.SystemColors.Info;
            this.sampleRecommendations.Location = new System.Drawing.Point(770, 322);
            this.sampleRecommendations.Multiline = true;
            this.sampleRecommendations.Name = "sampleRecommendations";
            this.sampleRecommendations.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.sampleRecommendations.Size = new System.Drawing.Size(300, 207);
            this.sampleRecommendations.TabIndex = 9;
            this.sampleRecommendations.WordWrap = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Location = new System.Drawing.Point(3, 6);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.top1Chart);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(761, 250);
            this.splitContainer1.SplitterDistance = 209;
            this.splitContainer1.TabIndex = 8;
            // 
            // top1Chart
            // 
            chartArea3.AxisY.Maximum = 100D;
            chartArea3.AxisY.Minimum = 0D;
            chartArea3.Name = "ChartArea1";
            this.top1Chart.ChartAreas.Add(chartArea3);
            this.top1Chart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.top1Chart.Location = new System.Drawing.Point(0, 0);
            this.top1Chart.Name = "top1Chart";
            series4.BorderWidth = 3;
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series4.Name = "Avg. Precision";
            series5.BorderWidth = 3;
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series5.Color = System.Drawing.Color.Fuchsia;
            series5.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series5.Name = "Avg. Recall";
            series6.BorderWidth = 3;
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series6.Color = System.Drawing.Color.ForestGreen;
            series6.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series6.Name = "Avg. F1";
            this.top1Chart.Series.Add(series4);
            this.top1Chart.Series.Add(series5);
            this.top1Chart.Series.Add(series6);
            this.top1Chart.Size = new System.Drawing.Size(207, 248);
            this.top1Chart.TabIndex = 3;
            this.top1Chart.Text = "chart3";
            title3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title3.Name = "Title";
            title3.Text = "Metric Values for Top Recommendation";
            this.top1Chart.Titles.Add(title3);
            this.tooGraphs.SetToolTip(this.top1Chart, "The precision, recall, and F1 values corresponding to the \r\nbest recommendation u" +
        "sing the provided configuration.");
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.top3Chart);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.top5Chart);
            this.splitContainer2.Size = new System.Drawing.Size(548, 250);
            this.splitContainer2.SplitterDistance = 239;
            this.splitContainer2.TabIndex = 0;
            // 
            // top3Chart
            // 
            chartArea4.AxisY.Maximum = 100D;
            chartArea4.AxisY.Minimum = 0D;
            chartArea4.Name = "ChartArea1";
            this.top3Chart.ChartAreas.Add(chartArea4);
            this.top3Chart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.top3Chart.Location = new System.Drawing.Point(0, 0);
            this.top3Chart.Name = "top3Chart";
            series7.BorderWidth = 3;
            series7.ChartArea = "ChartArea1";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series7.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series7.Name = "Avg. Precision";
            series8.BorderWidth = 3;
            series8.ChartArea = "ChartArea1";
            series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series8.Color = System.Drawing.Color.Fuchsia;
            series8.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series8.Name = "Avg. Recall";
            series9.BorderWidth = 3;
            series9.ChartArea = "ChartArea1";
            series9.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series9.Color = System.Drawing.Color.ForestGreen;
            series9.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series9.Name = "Avg. F1";
            this.top3Chart.Series.Add(series7);
            this.top3Chart.Series.Add(series8);
            this.top3Chart.Series.Add(series9);
            this.top3Chart.Size = new System.Drawing.Size(237, 248);
            this.top3Chart.TabIndex = 2;
            this.top3Chart.Text = "chart2";
            title4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title4.Name = "Title";
            title4.Text = "Metric Values for Top Three Recommendations";
            this.top3Chart.Titles.Add(title4);
            this.tooGraphs.SetToolTip(this.top3Chart, "The precision, recall, and F1 values corresponding to the \r\nthree best recommenda" +
        "tions using the provided configuration.");
            // 
            // top5Chart
            // 
            chartArea5.AxisY.Maximum = 100D;
            chartArea5.AxisY.Minimum = 0D;
            chartArea5.Name = "ChartArea1";
            this.top5Chart.ChartAreas.Add(chartArea5);
            this.top5Chart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend2.Name = "Legend1";
            this.top5Chart.Legends.Add(legend2);
            this.top5Chart.Location = new System.Drawing.Point(0, 0);
            this.top5Chart.Name = "top5Chart";
            series10.BorderWidth = 3;
            series10.ChartArea = "ChartArea1";
            series10.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series10.Legend = "Legend1";
            series10.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series10.Name = "Avg. Precision";
            series11.BorderWidth = 3;
            series11.ChartArea = "ChartArea1";
            series11.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series11.Color = System.Drawing.Color.Fuchsia;
            series11.Legend = "Legend1";
            series11.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series11.Name = "Avg. Recall";
            series12.BorderWidth = 3;
            series12.ChartArea = "ChartArea1";
            series12.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series12.Color = System.Drawing.Color.ForestGreen;
            series12.Legend = "Legend1";
            series12.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series12.Name = "Avg. F1";
            this.top5Chart.Series.Add(series10);
            this.top5Chart.Series.Add(series11);
            this.top5Chart.Series.Add(series12);
            this.top5Chart.Size = new System.Drawing.Size(303, 248);
            this.top5Chart.TabIndex = 1;
            this.top5Chart.Text = "chart1";
            title5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            title5.Name = "Title";
            title5.Text = "Metric Values for Top Five Recommendations";
            this.top5Chart.Titles.Add(title5);
            this.tooGraphs.SetToolTip(this.top5Chart, "The precision, recall, and F1 values corresponding to the \r\nfive best recommendat" +
        "ions using the provided configuration.");
            // 
            // historyChart
            // 
            this.historyChart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.historyChart.AutoScroll = true;
            this.historyChart.BackColor = System.Drawing.Color.White;
            this.historyChart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.historyChart.Controls.Add(this.historyLabel10);
            this.historyChart.Controls.Add(this.historyLabel9);
            this.historyChart.Controls.Add(this.historyLabel8);
            this.historyChart.Controls.Add(this.historyLabel7);
            this.historyChart.Controls.Add(this.historyLabel6);
            this.historyChart.Controls.Add(this.historyLabel5);
            this.historyChart.Controls.Add(this.historyLabel4);
            this.historyChart.Controls.Add(this.historyLabel3);
            this.historyChart.Controls.Add(this.historyLabel2);
            this.historyChart.Controls.Add(this.historyLabel1);
            this.historyChart.Controls.Add(this.label2);
            this.historyChart.Location = new System.Drawing.Point(770, 6);
            this.historyChart.Name = "historyChart";
            this.historyChart.Size = new System.Drawing.Size(306, 250);
            this.historyChart.TabIndex = 7;
            // 
            // historyLabel10
            // 
            this.historyLabel10.AutoSize = true;
            this.historyLabel10.Location = new System.Drawing.Point(3, 281);
            this.historyLabel10.Name = "historyLabel10";
            this.historyLabel10.Size = new System.Drawing.Size(54, 13);
            this.historyLabel10.TabIndex = 32;
            this.historyLabel10.Text = "History 10";
            this.historyLabel10.Visible = false;
            this.historyLabel10.Click += new System.EventHandler(this.historyLabel_Click);
            this.historyLabel10.MouseEnter += new System.EventHandler(this.historyLabel_MouseEnter);
            this.historyLabel10.MouseLeave += new System.EventHandler(this.historyLabel_MouseLeave);
            // 
            // historyLabel9
            // 
            this.historyLabel9.AutoSize = true;
            this.historyLabel9.Location = new System.Drawing.Point(3, 254);
            this.historyLabel9.Name = "historyLabel9";
            this.historyLabel9.Size = new System.Drawing.Size(48, 13);
            this.historyLabel9.TabIndex = 31;
            this.historyLabel9.Text = "History 9";
            this.historyLabel9.Visible = false;
            this.historyLabel9.Click += new System.EventHandler(this.historyLabel_Click);
            this.historyLabel9.MouseEnter += new System.EventHandler(this.historyLabel_MouseEnter);
            this.historyLabel9.MouseLeave += new System.EventHandler(this.historyLabel_MouseLeave);
            // 
            // historyLabel8
            // 
            this.historyLabel8.AutoSize = true;
            this.historyLabel8.Location = new System.Drawing.Point(3, 229);
            this.historyLabel8.Name = "historyLabel8";
            this.historyLabel8.Size = new System.Drawing.Size(48, 13);
            this.historyLabel8.TabIndex = 30;
            this.historyLabel8.Text = "History 8";
            this.historyLabel8.Visible = false;
            this.historyLabel8.Click += new System.EventHandler(this.historyLabel_Click);
            this.historyLabel8.MouseEnter += new System.EventHandler(this.historyLabel_MouseEnter);
            this.historyLabel8.MouseLeave += new System.EventHandler(this.historyLabel_MouseLeave);
            // 
            // historyLabel7
            // 
            this.historyLabel7.AutoSize = true;
            this.historyLabel7.Location = new System.Drawing.Point(3, 202);
            this.historyLabel7.Name = "historyLabel7";
            this.historyLabel7.Size = new System.Drawing.Size(48, 13);
            this.historyLabel7.TabIndex = 29;
            this.historyLabel7.Text = "History 7";
            this.historyLabel7.Visible = false;
            this.historyLabel7.Click += new System.EventHandler(this.historyLabel_Click);
            this.historyLabel7.MouseEnter += new System.EventHandler(this.historyLabel_MouseEnter);
            this.historyLabel7.MouseLeave += new System.EventHandler(this.historyLabel_MouseLeave);
            // 
            // historyLabel6
            // 
            this.historyLabel6.AutoSize = true;
            this.historyLabel6.Location = new System.Drawing.Point(3, 174);
            this.historyLabel6.Name = "historyLabel6";
            this.historyLabel6.Size = new System.Drawing.Size(48, 13);
            this.historyLabel6.TabIndex = 28;
            this.historyLabel6.Text = "History 6";
            this.historyLabel6.Visible = false;
            this.historyLabel6.Click += new System.EventHandler(this.historyLabel_Click);
            this.historyLabel6.MouseEnter += new System.EventHandler(this.historyLabel_MouseEnter);
            this.historyLabel6.MouseLeave += new System.EventHandler(this.historyLabel_MouseLeave);
            // 
            // historyLabel5
            // 
            this.historyLabel5.AutoSize = true;
            this.historyLabel5.Location = new System.Drawing.Point(3, 147);
            this.historyLabel5.Name = "historyLabel5";
            this.historyLabel5.Size = new System.Drawing.Size(48, 13);
            this.historyLabel5.TabIndex = 27;
            this.historyLabel5.Text = "History 5";
            this.historyLabel5.Visible = false;
            this.historyLabel5.Click += new System.EventHandler(this.historyLabel_Click);
            this.historyLabel5.MouseEnter += new System.EventHandler(this.historyLabel_MouseEnter);
            this.historyLabel5.MouseLeave += new System.EventHandler(this.historyLabel_MouseLeave);
            // 
            // historyLabel4
            // 
            this.historyLabel4.AutoSize = true;
            this.historyLabel4.Location = new System.Drawing.Point(3, 120);
            this.historyLabel4.Name = "historyLabel4";
            this.historyLabel4.Size = new System.Drawing.Size(48, 13);
            this.historyLabel4.TabIndex = 26;
            this.historyLabel4.Text = "History 4";
            this.historyLabel4.Visible = false;
            this.historyLabel4.Click += new System.EventHandler(this.historyLabel_Click);
            this.historyLabel4.MouseEnter += new System.EventHandler(this.historyLabel_MouseEnter);
            this.historyLabel4.MouseLeave += new System.EventHandler(this.historyLabel_MouseLeave);
            // 
            // historyLabel3
            // 
            this.historyLabel3.AutoSize = true;
            this.historyLabel3.Location = new System.Drawing.Point(3, 93);
            this.historyLabel3.Name = "historyLabel3";
            this.historyLabel3.Size = new System.Drawing.Size(48, 13);
            this.historyLabel3.TabIndex = 25;
            this.historyLabel3.Text = "History 3";
            this.historyLabel3.Visible = false;
            this.historyLabel3.Click += new System.EventHandler(this.historyLabel_Click);
            this.historyLabel3.MouseEnter += new System.EventHandler(this.historyLabel_MouseEnter);
            this.historyLabel3.MouseLeave += new System.EventHandler(this.historyLabel_MouseLeave);
            // 
            // historyLabel2
            // 
            this.historyLabel2.AutoSize = true;
            this.historyLabel2.Location = new System.Drawing.Point(3, 65);
            this.historyLabel2.Name = "historyLabel2";
            this.historyLabel2.Size = new System.Drawing.Size(48, 13);
            this.historyLabel2.TabIndex = 24;
            this.historyLabel2.Text = "History 2";
            this.historyLabel2.Visible = false;
            this.historyLabel2.Click += new System.EventHandler(this.historyLabel_Click);
            this.historyLabel2.MouseEnter += new System.EventHandler(this.historyLabel_MouseEnter);
            this.historyLabel2.MouseLeave += new System.EventHandler(this.historyLabel_MouseLeave);
            // 
            // historyLabel1
            // 
            this.historyLabel1.AutoSize = true;
            this.historyLabel1.Location = new System.Drawing.Point(3, 38);
            this.historyLabel1.Name = "historyLabel1";
            this.historyLabel1.Size = new System.Drawing.Size(48, 13);
            this.historyLabel1.TabIndex = 23;
            this.historyLabel1.Text = "History 1";
            this.historyLabel1.Visible = false;
            this.historyLabel1.Click += new System.EventHandler(this.historyLabel_Click);
            this.historyLabel1.MouseEnter += new System.EventHandler(this.historyLabel_MouseEnter);
            this.historyLabel1.MouseLeave += new System.EventHandler(this.historyLabel_MouseLeave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(127, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "History";
            // 
            // statusBox
            // 
            this.statusBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.statusBox.BackColor = System.Drawing.Color.White;
            this.statusBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.statusBox.Location = new System.Drawing.Point(3, 276);
            this.statusBox.Multiline = true;
            this.statusBox.Name = "statusBox";
            this.statusBox.ReadOnly = true;
            this.statusBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.statusBox.Size = new System.Drawing.Size(760, 253);
            this.statusBox.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configurationToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1087, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.disableTooltipsToolStripMenuItem});
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.configurationToolStripMenuItem.Text = "File";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.loadToolStripMenuItem.Text = "Load...";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.saveAsToolStripMenuItem.Text = "Save As...";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(867, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Recommender";
            // 
            // recommenderSelectionComboBox
            // 
            this.recommenderSelectionComboBox.FormattingEnabled = true;
            this.recommenderSelectionComboBox.Location = new System.Drawing.Point(949, 9);
            this.recommenderSelectionComboBox.Name = "recommenderSelectionComboBox";
            this.recommenderSelectionComboBox.Size = new System.Drawing.Size(133, 21);
            this.recommenderSelectionComboBox.TabIndex = 3;
            this.recommenderSelectionComboBox.SelectedIndexChanged += new System.EventHandler(this.recommenderSelected);
            // 
            // frequencyChart
            chartArea2.AxisX.Interval = 1D;
            chartArea2.AxisX.LabelStyle.Enabled = false;
            chartArea2.AxisY.MajorGrid.Enabled = false;
            // chkShowXLabels
            this.chkShowXLabels.AutoSize = true;
            this.chkShowXLabels.Location = new System.Drawing.Point(302, 21);
            this.chkShowXLabels.Name = "chkShowXLabels";
            this.chkShowXLabels.Size = new System.Drawing.Size(119, 17);
            this.chkShowXLabels.TabIndex = 1;

             // tooGraphs
            // 
            this.tooGraphs.AutoPopDelay = 7500;
            this.tooGraphs.InitialDelay = 500;
            this.tooGraphs.ReshowDelay = 100;
                         // 
            // disableTooltipsToolStripMenuItem
            // 
            this.disableTooltipsToolStripMenuItem.CheckOnClick = true;
            this.disableTooltipsToolStripMenuItem.Name = "disableTooltipsToolStripMenuItem";
            this.disableTooltipsToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.disableTooltipsToolStripMenuItem.Text = "Disable Tooltips";
            this.disableTooltipsToolStripMenuItem.CheckedChanged += new System.EventHandler(this.disableTooltipsToolStripMenuItem_CheckedChanged);
            // 
            // CreationAssistant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1087, 585);
            this.Controls.Add(this.recommenderSelectionComboBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "CreationAssistant";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Creation Assistant Supporting Triage Recommenders (CASTR)";
            this.tabControl1.ResumeLayout(false);
            this.configurationTab.ResumeLayout(false);
            this.configurationTab.PerformLayout();
            this.heuristicsPanelTop.ResumeLayout(false);
            this.heuristicsPanelTop.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.heuristicsChart)).EndInit();
            this.heuristicsOptionsPanel.ResumeLayout(false);
            this.heuristicsOptionsPanel.PerformLayout();
            this.heuristicsPanel.ResumeLayout(false);
            this.heuristicsPanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.frequencyChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frequencySelectBar)).EndInit();
            this.panel3.ResumeLayout(false);
            this.analysisTab.ResumeLayout(false);
            this.analysisTab.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.top1Chart)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.top3Chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.top5Chart)).EndInit();
            this.historyChart.ResumeLayout(false);
            this.historyChart.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage analysisTab;
        private System.Windows.Forms.DataVisualization.Charting.Chart top5Chart;
        private System.Windows.Forms.TextBox statusBox;
        private System.Windows.Forms.TabPage configurationTab;
        private System.Windows.Forms.Panel heuristicsPanelTop;
        private System.Windows.Forms.Label heuristicsLabel;
        private System.Windows.Forms.Panel heuristicsPanel;
        private System.Windows.Forms.DataVisualization.Charting.Chart heuristicsChart;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RichTextBox classesBox;
        private System.Windows.Forms.Label fcValue;
        private System.Windows.Forms.Label fcLabel;
        private System.Windows.Forms.TrackBar frequencySelectBar;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox heuristicsBox10;
        private System.Windows.Forms.ComboBox heuristicsBox9;
        private System.Windows.Forms.ComboBox heuristicsBox8;
        private System.Windows.Forms.ComboBox heuristicsBox7;
        private System.Windows.Forms.ComboBox heuristicsBox6;
        private System.Windows.Forms.ComboBox heuristicsBox5;
        private System.Windows.Forms.ComboBox heuristicsBox4;
        private System.Windows.Forms.ComboBox heuristicsBox3;
        private System.Windows.Forms.ComboBox heuristicsBox2;
        private System.Windows.Forms.ComboBox heuristicsBox1;
        private System.Windows.Forms.Label pathLabel10;
        private System.Windows.Forms.Label pathLabel9;
        private System.Windows.Forms.Label pathLabel8;
        private System.Windows.Forms.Label pathLabel7;
        private System.Windows.Forms.Label pathLabel6;
        private System.Windows.Forms.Label pathLabel5;
        private System.Windows.Forms.Label pathLabel4;
        private System.Windows.Forms.Label pathLabel3;
        private System.Windows.Forms.Label pathLabel2;
        private System.Windows.Forms.Label pathLabel1;
        private System.Windows.Forms.Label pathGroupLabel;
        private System.Windows.Forms.Label dataSourceLabel;
        private System.Windows.Forms.Panel heuristicsOptionsPanel;
        private System.Windows.Forms.Label percentageLabel10;
        private System.Windows.Forms.Label percentageLabel9;
        private System.Windows.Forms.Label percentageLabel8;
        private System.Windows.Forms.Label percentageLabel7;
        private System.Windows.Forms.Label percentageLabel6;
        private System.Windows.Forms.Label percentageLabel5;
        private System.Windows.Forms.Label percentageLabel4;
        private System.Windows.Forms.Label percentageLabel3;
        private System.Windows.Forms.Label percentageLabel2;
        private System.Windows.Forms.Label percentageLabel1;
        private System.Windows.Forms.Label pathPercentageLabel;
        private System.Windows.Forms.ComboBox numOfHeuristicsBox;
        private System.Windows.Forms.Label numHeuristicsLabel;
        private System.Windows.Forms.ComboBox otherHeuristicBox;
        private System.Windows.Forms.Label otherLabel;
        private System.Windows.Forms.Label otherSettingsLabel;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel historyChart;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataVisualization.Charting.Chart top1Chart;
        private System.Windows.Forms.DataVisualization.Charting.Chart top3Chart;
        private System.Windows.Forms.Label historyLabel10;
        private System.Windows.Forms.Label historyLabel9;
        private System.Windows.Forms.Label historyLabel8;
        private System.Windows.Forms.Label historyLabel7;
        private System.Windows.Forms.Label historyLabel6;
        private System.Windows.Forms.Label historyLabel5;
        private System.Windows.Forms.Label historyLabel4;
        private System.Windows.Forms.Label historyLabel3;
        private System.Windows.Forms.Label historyLabel2;
        private System.Windows.Forms.Label historyLabel1;
        private System.ComponentModel.BackgroundWorker backgroundProcess;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox mlAlgorithmSelect;
        private System.Windows.Forms.Button autoConfigButton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox recommenderSelectionComboBox;
        private System.Windows.Forms.TextBox sampleRecommendations;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ComboBox cbShowRecommendations;
        private System.Windows.Forms.Label lblShowRecommendations;
        private System.Windows.Forms.Button btnApplyNumSamples;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkAllData;
        private System.Windows.Forms.CheckBox chkF1;
        private System.Windows.Forms.CheckBox chkRecall;
        private System.Windows.Forms.CheckBox chkPrecision;
        private System.Windows.Forms.CheckBox chkShowXLabels;
        private System.Windows.Forms.DataVisualization.Charting.Chart frequencyChart;
        private System.Windows.Forms.ToolTip tooGraphs;
        private System.Windows.Forms.ToolStripMenuItem disableTooltipsToolStripMenuItem;
    }
}