﻿/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Windows.Forms;
using System.IO;
using CASEA.src.functions;

namespace CASEA.src.ui_forms
{
    public partial class SurveyPage : Form
    {
        Dictionary<string, string> surveyResults = new Dictionary<string, string>();
        static int sessionID = 1;
        public static int attempt = 0;
        static string filePath = ".../.../assets/survey/results.xml";
        

        public SurveyPage()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (Control c in this.groupBox1.Controls)
            {
                if (c is TextBox)
                {
                    surveyResults.Add(c.Name, c.Text);
                }
            }

            if (!File.Exists(filePath))
            {
                XDocument doc =
                    new XDocument(
                        new XElement("root",
                            new XElement("survey",
                                new XElement("sessionID", 1),
                                new XElement("date", DateTime.Now),
                                new XElement("results",
                                    surveyResults.Select(x => new XElement(x.Key, x.Value))),
                                new XElement("tests")
                             )                             
                        )
                    );
                doc.Save(filePath);
            }
            else
            {
                XDocument doc = XDocument.Load(filePath);
                int temp;
                foreach (XElement survey in doc.Elements().Elements())
                {
                    temp = Convert.ToInt32(survey.Element("sessionID").Value);
                    if (temp > sessionID) sessionID = temp;
                }
                sessionID++;
                doc.Element("root").Add(new XElement("survey",
                                new XElement("sessionID", sessionID),
                                new XElement("date", DateTime.Now),
                                new XElement("results",
                                    surveyResults.Select(x => new XElement(x.Key, x.Value)))));
                doc.Save(filePath);
            }

            MainMenu form = new MainMenu();
            this.Hide();
            form.ShowDialog();
            this.Close();
        }

        private void radioButton_Checked(object sender, EventArgs e)
        {
            RadioButton radio = (RadioButton)sender;
            string question = radio.Parent.Name;
            if (surveyResults.Keys.Contains(question))
            {
                surveyResults.Remove(question);
            }
            surveyResults.Add(question, radio.Text);
        }

        //Now needs accuracy values
        public static void recommenderCreated(int freq, List<HeuristicEnum> chosenHeuristics, HeuristicEnum otherHeuristics, classifier.EvaluationMetrics accuracy)
        {

            int counter = 1;
            Dictionary<string, HeuristicEnum> heuristics = new Dictionary<string, HeuristicEnum>();
            foreach (HeuristicEnum h in chosenHeuristics)
            {
                heuristics.Add("h" + counter.ToString(), h);
                counter++;
            }

            XElement test = new XElement(
                                new XElement("test",
                                    new XElement("attempt", attempt),
                                    new XElement("chosenFrequencyCutoff", freq),
                                    new XElement("chosenHeuristics", heuristics.Select(x => new XElement(x.Key, x.Value))),
                                    new XElement("otherHeuristics", otherHeuristics),
                                    new XElement("accuracy",
                                        new XElement("top5",
                                            new XElement("precision", Math.Round(accuracy.precision[2] * 100, 2) + "%"),
                                            new XElement("recall", Math.Round(accuracy.recall[2] * 100, 2) + "%")),
                                        new XElement("top3",
                                            new XElement("precision", Math.Round(accuracy.precision[1] * 100, 2) + "%"),
                                            new XElement("recall", Math.Round(accuracy.recall[1] * 100, 2) + "%")),
                                        new XElement("top1",
                                            new XElement("precision", Math.Round(accuracy.precision[0] * 100, 2) + "%"),
                                            new XElement("recall", Math.Round(accuracy.recall[0] * 100, 2) + "%")))));

            if (File.Exists(filePath))
            {
                XDocument doc = XDocument.Load(filePath);
                foreach (XElement survey in doc.Elements().Elements())
                {
                    int temp = Convert.ToInt32(survey.Element("sessionID").Value);
                    if (temp == sessionID)
                    {
                        survey.Add(test);
                    }
                }
                doc.Save(filePath);
            }
        }
    }
}
