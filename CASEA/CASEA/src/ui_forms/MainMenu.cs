﻿/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using NLog;

namespace CASEA.src.ui_forms
{
    public partial class MainMenu : Form
    {
        private CreationAssistant creationAssistant;

        private static Logger logger = LogManager.GetCurrentClassLogger();

        //private Splash splash;
        //private Stopwatch sw;

        public MainMenu()
        {
            InitializeComponent();
            
            /* Testing the splash screen (Needs work)
            sw = new Stopwatch();

            splash = new Splash();
            splash.Show();

            sw.Start();
            while (sw.ElapsedMilliseconds < 5000) ;
            splash.Close();
             */
        }

        // Open configuration for a saved recommender
        private void btnCreationAssistant_Click(object sender, EventArgs e)
        {
            // If the window is disposed or invisible then create a new one with a recommender.
            if (creationAssistant == null || creationAssistant.IsDisposed)
            {
                loadRecommenderDialog.InitialDirectory = System.IO.Path.GetFullPath("../../recommenders");
                if (loadRecommenderDialog.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        creationAssistant = new CreationAssistant(loadRecommenderDialog.FileName);
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.Message);
                        return;
                    }
                    creationAssistant.Show();
                }
            }
        }

        private void btnNewFrmRep_Click(object sender, EventArgs e)
        {
            NewRepositoryRecommender newChild = new NewRepositoryRecommender();
            newChild.ShowDialog();
        }

        // Save a new XML recommender
        private void btnNewFrmXML_Click(object sender, EventArgs e)
        {
            NewXMLRecommender newChild = new NewXMLRecommender();
            newChild.ShowDialog();
        }

        // Combine reports module. <SENSITIVE>
        private void btnCombine_Click(object sender, EventArgs e)
        {
            CombineReportsForm newChild = new CombineReportsForm();
            newChild.ShowDialog();
        }

        private void btnNewFrmDatabase_Click(object sender, EventArgs e)
        {
            SQLConnectionForm sqlCon = new SQLConnectionForm();
            sqlCon.Show();
        }

        private void btnNewFromBitBucket_Click(object sender, EventArgs e)
        {
            NewBitbucketRecommender newChild = new NewBitbucketRecommender();
            newChild.ShowDialog();
        }

        private void btnViewLog_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(System.IO.Path.GetFullPath("log.txt"));
        }
    }
}
