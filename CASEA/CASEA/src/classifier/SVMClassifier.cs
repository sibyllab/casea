﻿/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accord.MachineLearning.VectorMachines;
using Accord.MachineLearning.VectorMachines.Learning;
using Accord.Statistics.Kernels;
using Accord.Statistics.Kernels.Sparse;
using CASEA.src.ui_forms;
using Lucene.Net.Store;
using NLog;

namespace CASEA.src.classifier
{
    public class SVMClassifier : MLAlgorithm
    {
        private MulticlassSupportVectorMachine machine;
        private MulticlassSupportVectorLearning teacher;

        private static Logger logger = LogManager.GetCurrentClassLogger();

        // private constructor for loading recommenders        
        public SVMClassifier() { } // no args for testing
        public SVMClassifier(RecommenderType type, InputOutputPack ioPack)
            : this(type, ioPack.TDS, ioPack.VDS)
        { // call original constructor
        }
        private SVMClassifier(MulticlassSupportVectorMachine loadedMachine, TrainingData tds, ValidationData vds)
        {
            machine = loadedMachine;
            TDS = tds;
            VDS = vds;
        }
        public SVMClassifier(RecommenderType type, TrainingData machineIO, ValidationData validationInput)
        {

            logger.Info("Creating SVM classifier");

            // Learning teacher;
            TDS = machineIO;
            VDS = validationInput;

            string[] classNames;
            int[][] instances;

            switch (type)
            {
                case RecommenderType.Component:
                    classNames = TDS.output.componentInstances.names;
                    instances = TDS.output.componentInstances.effects;
                    break;
                case RecommenderType.Developer:
                default:
                    classNames = TDS.output.developerInstances.names;
                    instances = TDS.output.developerInstances.whoFixed;
                    break;
            }

            if (classNames.Length < 2)
            {
                logger.Warn("Can't create a classifer with one class!");
                System.Windows.Forms.MessageBox.Show("Can't create a classifer with one class!", "Error");
                return;
            }

            //doPCA();

            // Create a multi-label machine. Args are (length of input, kernel, length of output);
            int numFeatures = TDS.input.inputArray[0].Length;
            machine = new MulticlassSupportVectorMachine(numFeatures, new Gaussian(), classNames.Length);

            // Creates the mult-label learning algorithm
            int[] instanceLabels = getClassNames(instances);
                     
            teacher = new MulticlassSupportVectorLearning(machine, TDS.input.inputArray, instanceLabels);

            // Configure the teaching algorithm
            teacher.Algorithm = (svm, classInputs, classOutputs, i, j) => new SequentialMinimalOptimization(svm, classInputs, classOutputs);

            // Teach the classifier
            try
            {
                double error = teacher.Run();
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
            }

            // GetPrecisionRecall
            getAveragePrecisionRecall(type, classNames);
        }
        
        // Save the machine to a given location
        public override void saveMachine(string filePath)
        {
            if(machine != null)
                machine.Save(filePath + "/classifier.mchn");
        }

        // Load a machine
        public static MLAlgorithm loadMachine(string filePath, TrainingData tds, ValidationData vds)
        {
            return new SVMClassifier(MulticlassSupportVectorMachine.Load(filePath + "/classifier.mchn"), tds, vds);
        }

        protected override void getResults(double[] input, out double[] output)
        {
            machine.Compute(input, MulticlassComputeMethod.Voting, out output);
        }
    }
}
