﻿/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accord.MachineLearning.VectorMachines;
using Accord.MachineLearning.VectorMachines.Learning;
using Accord.Statistics.Kernels;
using Accord.MachineLearning.Bayes;
using Accord.Statistics.Distributions;
using Accord.Statistics.Distributions.Univariate;
using CASEA.src.ui_forms;
using CASEA.src.functions;
using Accord.Statistics.Analysis;
using NLog;

namespace CASEA.src.classifier
{
    public class NBClassifier : MLAlgorithm
    {
        
        private NaiveBayes<IUnivariateDistribution> machine;

        private static Logger logger = LogManager.GetCurrentClassLogger();

        // private constructor for loading recommenders        
        public NBClassifier() { } // no args for testing
        public NBClassifier(RecommenderType type, InputOutputPack ioPack)
            : this(type, ioPack.TDS, ioPack.VDS){ // call original constructor
        }
        private NBClassifier(NaiveBayes<IUnivariateDistribution> loadedMachine, TrainingData tds, ValidationData vds)
        {
            machine = loadedMachine;
            TDS = tds;
            VDS = vds;
        }
        public NBClassifier(RecommenderType type, TrainingData machineIO, ValidationData validationInput)
        {

            logger.Info("Creating NB classifier");

            // Learning teacher;
            TDS = machineIO;
            VDS = validationInput;

            string[] classNames;
            int[][] instances;

            switch (type)
            {
                case RecommenderType.Component:
                    classNames = TDS.output.componentInstances.names;
                    instances = TDS.output.componentInstances.effects;
                    break;
                case RecommenderType.Developer:
                default:
                    classNames = TDS.output.developerInstances.names;
                    instances = TDS.output.developerInstances.whoFixed;
                    break;
            }

            if (classNames.Length < 2)
            {
                logger.Warn("Can't create a classifer with one class!");
                System.Windows.Forms.MessageBox.Show("Can't create a classifier with one class!", "Warning");
                return;
            }

            // Check if a class only has one training instance
            for (int i = 0; i < classNames.Length; i++)
            {
                int numInstances = XMLParser.numOfReportsSolved(TDS.output.developerInstances.whoFixed, i);
                //if (numInstances < 2)
                //{
                logger.Info(classNames[i] + " has " + numInstances + " developer training instance");
                //}
            }

            // Create a multi-label machine.
            int[] instanceLabels = getClassNames(instances);

            // NB doesn't seem to like classifying really sparse data - use PCA to reduce dimensions
            doPCA();

            machine = new NaiveBayes<IUnivariateDistribution>(classNames.Length, TDS.input.inputArray[0].Length, new NormalDistribution());
            machine.Estimate(TDS.input.inputArray, instanceLabels, true, new Accord.Statistics.Distributions.Fitting.NormalOptions() { Regularization = 1 });

            getAveragePrecisionRecall(type, classNames);
        }

         // Save the machine to a given location
        public override void saveMachine(string filePath)
        {
            if(machine != null)
                machine.Save(filePath + "/classifier.mchn");
        }

        // Load a machine
        public static MLAlgorithm loadMachine(string filePath, TrainingData tds, ValidationData vds)
        {
            return new NBClassifier(NaiveBayes.Load<IUnivariateDistribution> (filePath + "/classifier.mchn"), tds, vds);
        }
       
        protected override void getResults(double[] input, out double[] output)
        {
            double likelihood;
            machine.Compute(input, out likelihood, out output);
        }
    }
}
