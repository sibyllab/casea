﻿/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CASEA.src.functions;

namespace CASEA.src.classifier
{
    // Holds input information
    public class Input
    {
        public Input() { }
        public Input(string[] w, double[][] ia) {
            words = w;
            inputArray = ia;
        }

        public Input(Input toCopy)
        {
            this.inputArray = new double[toCopy.inputArray.Length][];
            Array.Copy(toCopy.inputArray, this.inputArray, toCopy.inputArray.Length);

            this.words = new string[toCopy.words.Length];
            Array.Copy(toCopy.words, this.words, toCopy.words.Length);
        }
        public string[] words;
        public double[][] inputArray;
    }

    public class DeveloperInstances
    {
        public string[] names;
        public int[][] whoFixed;
    }

    public class ComponentInstances
    {
        public string[] names;
        public int[][] effects;
    }


    // Holds output information
    public class Output
    {
        public DeveloperInstances developerInstances;
        public ComponentInstances componentInstances;

        public Output() { }
        public Output(DeveloperInstances di, ComponentInstances ci)
        {
            this.developerInstances = di;
            this.componentInstances = ci;
        }
    }

    // Hold pathgroup information as cannot serialize Dictionary
    public class PathGroups
    {
        public string[] pathNames;
        public int[] pathDistribution;
    }

    // Hold component group information
    public class Component
    {
        public string[] names;
        public string[][] developers;
        public int[] freqs;
    }

    public class TrainingData
    {
        public Input input;
        public Output output;
        public PathGroups paths;
        public Component components;

        public TrainingData() { }
               
        public TrainingData(string[] words, double[][] inputs, DeveloperInstances devInstances, ComponentInstances compInstances, Component c, PathGroups p)
        {
            // Set up the inputs
            input = new Input(words, inputs);

            // Set up the training instance information
            output = new Output(devInstances, compInstances);

            // Component
            components = c;

            // Path Groups
            paths = p;
        }

        // update the outputs
        public void updateOutputs(string[] names, HeuristicEnum[] heuristics, HeuristicEnum other, TrainingData data, string filePath)
        {
            TrainingData temp = XMLParser.updateOutputs(names, heuristics, other, data, filePath);
            this.components = temp.components;
            this.output = temp.output;
        }

        // private method for constructing the reportsSolved
        public int[] reportDistribution()
        {
            int[] numSolved = new int[output.developerInstances.names.Length];
            for (int i = 0; i < numSolved.Length; i++)
            {
                numSolved[i] = XMLParser.numOfReportsSolved(output.developerInstances.whoFixed, i);
            }

            return numSolved;
        }

        internal int[] componentDistribution()
        {
            int[] componentFreq = new int[output.componentInstances.names.Length];
            for (int i = 0; i < componentFreq.Length; i++)
            {
                componentFreq[i] = XMLParser.componentFrequency(output.componentInstances.effects, i);
            }

            return componentFreq;
        }

        // This and getComponentNames() are separate methods so that TrainingData does not need to know about recommender types.
        public string[] getDeveloperNames()
        {
            string[] devNames = new string[output.developerInstances.names.Length];
            for (int iii = 0; iii < devNames.Length; iii++)
            {
                devNames[iii] = output.developerInstances.names[iii];
            }

            return devNames;
        }

        public string[] getComponentNames()
        {
            string[] compNames = new string[output.componentInstances.names.Length];
            for (int iii = 0; iii < compNames.Length; iii++)
            {
                compNames[iii] = output.componentInstances.names[iii];
            }

            return compNames;
        }
    }
}
