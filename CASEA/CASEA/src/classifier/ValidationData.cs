﻿/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CASEA.src.classifier
{
    public class ValidationData
    {
        public Input input;
        public string[] components;
        public string[] reportIds;

        public ValidationData() { }
        public ValidationData(string[] words, double[][] inputs, string[] reportIds, string[] components)
        {
            input = new Input(words, inputs);
            this.components = components;
            this.reportIds = reportIds;
        }
    }
}
