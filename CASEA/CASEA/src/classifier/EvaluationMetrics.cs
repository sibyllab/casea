﻿/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CASEA.src.classifier
{
    public class EvaluationMetrics
    {
        /*
         * Arrays for holding evaluation results
         * Indexes are:
         *         0 - top 1
         *         1 - top 3 
         *         2 - top 5
         */

        public double[] precision = new double[3]; // 3 Elements 
        public double[] recall = new double[3]; // Recall in the same format.
        public double[] f1 = new double[3]; // F1-measure

        /**
         * Return:
         *  < 0 --> this accuracy is less than argument
         *  = 0 --> this accuracy is the same as the argument within DELTA
         *  > 0 --> this accuracy is larger than argument
         **/
        internal int compare(EvaluationMetrics otherMetrics)
        {
            double DELTA = 0.0001;

            // Calculate an average for accuracies
            double thisAccuracy_F1_Average = this.f1.Average();
            double otherAccuracy_F1_Average = otherMetrics.f1.Average();

            if ((thisAccuracy_F1_Average - otherAccuracy_F1_Average) < DELTA * -1)
                return -1;
            if ((thisAccuracy_F1_Average - otherAccuracy_F1_Average) > DELTA)
                return 1;
            return 0;
        }

        override public String ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 3; i++)
            {
                sb.Append("P" + i + ":" + this.precision[i]);
                sb.Append(",");
                sb.Append("R" + i + ":" + this.recall[i]);
                sb.Append(",");
                sb.Append("F1" + i + ":" + this.f1[i]);
                sb.Append(",");
            }
            return sb.ToString();
        }

        internal static double calculateF1(double precision, double recall)
        {
            // See https://en.wikipedia.org/wiki/F1_score
            if (precision + recall == 0)
                return 0;

            return 2 * (precision * recall) / (precision + recall);
        }
    }
}
