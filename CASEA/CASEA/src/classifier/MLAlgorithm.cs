﻿/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/

using Accord.Statistics.Analysis;
using CASEA.src.save_structure;
using CASEA.src.ui_forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;

namespace CASEA.src.classifier
{
    public abstract class MLAlgorithm
    {

        public enum Types {Auto, SVM, NaiveBayes};

        protected TrainingData TDS;
        protected ValidationData VDS;
        protected static CreationAssistant ui;

        public EvaluationMetrics averageMetrics = new EvaluationMetrics();

        protected abstract void getResults(double[] input, out double[] output);
        public abstract void saveMachine(string filePath);

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static MLAlgorithm getClassifier(RecommenderType type, MLAlgorithm.Types mlType, InputOutputPack data, CreationAssistant window)
        {
            ui = window;
            switch (mlType)
            {
                case MLAlgorithm.Types.NaiveBayes: return new NBClassifier(type, data);
                case MLAlgorithm.Types.SVM:
                default: return new SVMClassifier(type, data);
            }
        }

        public static MLAlgorithm loadMachine(string filePath, SavedRecommender r)
        {
            MLAlgorithm ml;
          
            if (r.algorithm.Equals(MLAlgorithm.Types.NaiveBayes))
                ml = NBClassifier.loadMachine(filePath, r.TDS, r.VDS);
            else // Default to SVM
                ml = SVMClassifier.loadMachine(filePath, r.TDS, r.VDS);
            return ml;
        }

        protected int[] getClassNames(int[][] outputs)
        {
            int[] output = new int[outputs.Length];

            for (int i = 0; i < output.Length; i++)
            {
                for (int j = 0; j < outputs[0].Length; j++)
                {
                    if (outputs[i][j] == 1)
                    {
                        output[i] = j;
                    }
                }
            }

            return output;
        }

        // Calculate the precison and recall and return in a 2 element array
        protected void getAveragePrecisionRecall(RecommenderType type, string[] classNames)
        {

            List<EvaluationMetrics> results = new List<EvaluationMetrics>();
            // numerators and denominators
            //double[] numerators = { 0.0, 0.0, 0.0 };
            
            // use the top 1, 3 and 5 to determine the precision.            
            double[] precisionDenominators = { 1.0, 3.0, 5.0 };
            double recallDenominator = 0.0;

            // Loop through the number of results.
            int numEvalReports = VDS.input.inputArray.Length;
            for (int i = 0; i < numEvalReports; i++)
            {
                // holds the top results
                string[] topN;
                EvaluationMetrics metrics = new EvaluationMetrics();

                // Precision, recall, F1 data
                double[] precision = new double[3];
                double[] recall = new double[3];
                double[] f1 = new double[3];

                string actualComponent = VDS.components[i];

                double[] numerators;

                try
                {
                    topN = getTopN(5, VDS.input.inputArray[i], classNames);
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                    break;
                }

                switch (type)
                {
                    case RecommenderType.Component:
                        recallDenominator = 1.0;
                        numerators = calculateComponent(actualComponent, topN);
                        break;
                    case RecommenderType.Developer:
                    default:
                        // Find out what component team should be
                        string[] componentTeam = getComponentTeam(actualComponent);
                        recallDenominator = componentTeam.Length;
                        if (componentTeam == null)
                        {
                            logger.Info("Unknown component " + actualComponent + " - No predictions made." + "(" + actualComponent + ") ");
                            continue;
                        }

                        numerators = calculateDeveloper(componentTeam, topN);
                        break;
                }

                // Calculate precision and recall
                for (int j = 0; j < 3; j++)
                {
                    precision[j] = numerators[j] / precisionDenominators[j];
                    recall[j] = numerators[j] / recallDenominator;
                    f1[j] = EvaluationMetrics.calculateF1(precision[j], recall[j]);
                }

                // Set the precision and recall arrays of this data object
                metrics.precision = precision;
                metrics.recall = recall;
                metrics.f1 = f1;

                results.Add(metrics);
            }

            // Calculate average metrics
            List<double> top1_Precision = new List<double>();
            List<double> top3_Precision = new List<double>();
            List<double> top5_Precision = new List<double>();
            List<double> top1_Recall = new List<double>();
            List<double> top3_Recall = new List<double>();
            List<double> top5_Recall = new List<double>();
            List<double> top1_F1 = new List<double>();
            List<double> top3_F1 = new List<double>();
            List<double> top5_F1 = new List<double>();

            foreach (EvaluationMetrics m in results)
            {
                top1_Precision.Add(m.precision[0]);
                top3_Precision.Add(m.precision[1]);
                top5_Precision.Add(m.precision[2]);

                top1_Recall.Add(m.recall[0]);
                top3_Recall.Add(m.recall[1]);
                top5_Recall.Add(m.recall[2]);

                top1_F1.Add(m.f1[0]);
                top3_F1.Add(m.f1[1]);
                top5_F1.Add(m.f1[2]);
            }

            double[] averagePrecision = new double[3];
            double[] averageRecall = new double[3];
            double[] averageF1 = new double[3];

            averagePrecision[0] = top1_Precision.Average();
            averagePrecision[1] = top3_Precision.Average();
            averagePrecision[2] = top5_Precision.Average();
            averageMetrics.precision = averagePrecision;

            averageRecall[0] = top1_Recall.Average();
            averageRecall[1] = top3_Recall.Average();
            averageRecall[2] = top5_Recall.Average();
            averageMetrics.recall = averageRecall;

            averageF1[0] = top1_F1.Average();
            averageF1[1] = top3_F1.Average();
            averageF1[2] = top5_F1.Average();
            averageMetrics.f1 = averageF1;

            // TODO: Remove - for testing 
            Console.Write("Average Precision: ");
            foreach (double n in averageMetrics.precision)
            {
                Console.Write(n + ", ");
            }
            Console.WriteLine();

            Console.Write("Average Recall: ");
            foreach (double n in averageMetrics.recall)
            {
                Console.Write(n + ", ");
            }
            Console.WriteLine();

            Console.Write("Average F1: ");
            foreach (double n in averageMetrics.f1)
            {
                Console.Write(n + ", ");
            }
            Console.WriteLine();

        }

        private string[] getComponentTeam(string component)
        {
            string[] team = { };
            for (int j = 0; j < TDS.components.names.Length; j++)
            {
                if (component == TDS.components.names[j])
                {
                    team = TDS.components.developers[j];
                }
            }
            return team;
        }

        private double[] calculateComponent(string correct, string[] topN)
        {

            // TODO: Remove - for testing
            Console.Write("Actual: " + correct + " Predicted: ");
            foreach (string c in topN)
                Console.Write(c + ",");
            Console.WriteLine();

            double[] numerators = new double[3];
            for (int j = 0; j < 5; j++)
            {
                if (correct.Equals(topN[j]))
                {
                    if (j < 1) // Top 1
                        numerators[0] = 1.0;
                    if (j < 3) // Top 3
                        numerators[1] = 1.0;
                    if (j < 5) // Top 5
                        numerators[2] = 1.0;
                }
            }
            return numerators;
        }

        private double[] calculateDeveloper(string[] componentTeam, string[] topN)
        {
            double[] numerators = new double[3];
            if (componentTeam != null)
            {
                // TODO: Remove - for testing
                
               // Console.Write("Team: ");
                //foreach (string d in componentTeam)
                  //  Console.Write(d + ",");
                Console.Write("Predicted: ");
                foreach (string d in topN)
                    Console.Write(d + ",");
                Console.WriteLine();
                
               
                for (int j = 0; j < 5; j++)
                {
                    if (componentTeam.Contains(topN[j]))
                    {
                        if (j < 1) // Top 1
                            numerators[0] += 1.0;
                        if (j < 3) // Top 3
                            numerators[1] += 1.0;
                        numerators[2] += 1.0; // Top 5
                    }
                }
            }
            return numerators;
        }

        // The go-to as we can check top 1, 3, and 5 with this array.
        private string[] getTopN(int n, double[] input, string[] classNames)
        {
            // Array for catching data
            double[] classes = new double[classNames.Length];
            string[] topN = new string[n];
            int maxIndex;

            // Ask for output and store to output
            try
            {
                getResults(input, out classes);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            // Check that recommendation made
            bool recMade = false;
            foreach(double prediction in classes)
            {
                if (prediction > 0)
                    recMade = true;
            }

            if (recMade == false)
            {
                System.Windows.Forms.MessageBox.Show("No recommendation made. Recommending using frequency ordering.");
                for (int i = 0; i < n; i++)
                    if (i < classNames.Length)
                        topN[i] = classNames[i];
                    else
                        topN[i] = String.Empty;
                throw new Exception();
            }
            else
            {
                // Find the top N largest results
                for (int i = 0; i < n; i++)
                {
                    // check if fewer than N classes
                    if (i >= classes.Length)
                    {
                        topN[i] = String.Empty;
                        continue;
                    }

                    maxIndex = 0;
                    for (int j = 1; j < classes.Length; j++)
                    {
                        if (classes[j] > classes[maxIndex])
                            maxIndex = j;
                    }
                    classes[maxIndex] = -2.0; // Mark class name as used
                    topN[i] = classNames[maxIndex];
                }
            }
            return topN;
        }

        public Dictionary<string, string[][]> getSampleRecommendations(int numReports, int numRecommendations, RecommenderType type)
        {
            string[] classNames = { "UNKNOWN" };
            switch (type)
            {
                case RecommenderType.Component: classNames = TDS.output.componentInstances.names; break;
                case RecommenderType.Developer: classNames = TDS.output.developerInstances.names; break;
            }

            Dictionary<string, string[][]> recommendations = new Dictionary<string, string[][]>();

            numReports = Math.Min(numReports, VDS.input.inputArray.Length);

            List<int> selectedReports = new List<int>(Enumerable.Range(0, numReports));

            // Choose random samples
            selectedReports.Clear();
            Random rnd = new Random();
            int selected = 0;
            while (selectedReports.Count() < numReports)
            {
                selected = rnd.Next(0, VDS.input.inputArray.Length);
                if (selectedReports.Contains(selected) == false)
                    selectedReports.Add(selected);
            }
            
             
            foreach (int report in selectedReports)
            {
                string[] predictions;
                string[] possible = { };
                string[][] results = new string[2][];
                string component = VDS.components[report];
                switch (type)
                {
                    case RecommenderType.Component: possible = new string[]{component}; break;
                    case RecommenderType.Developer: possible = getComponentTeam(component); break;
                }

                try
                {
                    predictions = getTopN(numRecommendations, VDS.input.inputArray[report], classNames);
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                    break;
                }

                results[0] = possible;
                results[1] = predictions;
                recommendations.Add(VDS.reportIds[report] + " - " + component, results);
            }

            return recommendations;
        }

        protected void doPCA()
        {
            // See http://accord-framework.net/docs/html/T_Accord_Statistics_Analysis_PrincipalComponentAnalysis.htm

            ui.appendToStatus("Performing Principal Component Analysis to reduce dimensions (This may take a long time).");
            // Creates the Principal Component Analysis of the given source
            var pcaTDS = new PrincipalComponentAnalysis(TDS.input.inputArray, AnalysisMethod.Center);
            var pcaVDS = new PrincipalComponentAnalysis(VDS.input.inputArray, AnalysisMethod.Center);

            // Compute the Principal Component Analysis
            pcaTDS.Compute();
            pcaVDS.Compute();

            // Creates a projection (max dimension must be <= number of instances to work)
            int dimensions = Math.Min(pcaTDS.GetNumberOfComponents(0.8f), Math.Min(TDS.input.inputArray.Length, VDS.input.inputArray.Length));
            //ui.appendToStatus("# of Dimensions: " + dimensions);

            TDS.input.inputArray = pcaTDS.Transform(TDS.input.inputArray, dimensions);
            VDS.input.inputArray = pcaVDS.Transform(VDS.input.inputArray, dimensions);

            ui.appendToStatus("PCA is done.");
        }
    }
}
