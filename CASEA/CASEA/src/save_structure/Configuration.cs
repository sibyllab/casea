﻿/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CASEA.src.ui_forms;
using CASEA.src.functions;
using CASEA.src.classifier;

namespace CASEA.src.save_structure
{
    public class Configuration // For storing the state of the recommender. Needs to include heuristics information.
    {
        public int frequencyCutoff { set; get; }        
        public HeuristicEnum[] heuristics { set; get; }
        public HeuristicEnum otherHeuristic { set; get; }
        public int attempt;
        public MLAlgorithm.Types algorithm;
        public RecommenderType confType;

        // Default constructor
        public Configuration() 
        {
            frequencyCutoff = 5;
            heuristics = new HeuristicEnum[] {
                HeuristicEnum.AssignedTo,
                HeuristicEnum.AssignedTo,
                HeuristicEnum.AssignedTo,
                HeuristicEnum.AssignedTo,
                HeuristicEnum.AssignedTo
            };
            otherHeuristic = HeuristicEnum.AssignedTo;
            attempt = 0;
            algorithm = MLAlgorithm.Types.SVM;
        }

        public Configuration(int defaultThreshold, int numHeuristics, HeuristicEnum defaultHeuristic, MLAlgorithm.Types algorithm, RecommenderType recType)
        {
            frequencyCutoff = defaultThreshold;
            heuristics = new HeuristicEnum[numHeuristics];
            for(int i=0; i<heuristics.Length; i++){
                heuristics[i] = defaultHeuristic;
            };
            otherHeuristic = defaultHeuristic;
            attempt = 0;
            this.algorithm = algorithm;
            this.confType = recType;
        }

        public Configuration(int threshold, HeuristicEnum other, HeuristicEnum[] selectedHeuristics, MLAlgorithm.Types algorithm, RecommenderType recType)
        {
            frequencyCutoff = threshold;
            heuristics = new HeuristicEnum[selectedHeuristics.Length];
            
            for (int iii = 0; iii < heuristics.Length; iii++)
            {
                heuristics[iii] = selectedHeuristics[iii];
            }

            otherHeuristic = other;
            attempt = 0;
            this.algorithm = algorithm;
            this.confType = recType;
        }


        override public String ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("(" + algorithm + ") ");
            sb.Append("Threshold:" + this.frequencyCutoff);
            sb.Append(",");
            for (int i = 0; i < heuristics.Length; i++)
            {
                sb.Append("Heuristic " + i + ":" + heuristics[i].ToString());
                sb.Append(",");
            }
            sb.Append("Other:" + this.otherHeuristic.ToString());
            return sb.ToString();
        }
    }
}