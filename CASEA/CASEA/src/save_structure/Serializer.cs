﻿/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml.Serialization;
using NLog;

namespace CASEA.src.save_structure
{
    public class Serializer
    {
        private static char[] xmlExtension = new char[] { '.', 'x', 'm', 'l' };

        private static Logger logger = LogManager.GetCurrentClassLogger();

        // Serialize the data
        public static void SerializeRecommender(string filePath, SavedRecommender saveObject)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SavedRecommender));
                using (MemoryStream input = new MemoryStream())
                using (FileStream output = new FileStream($@"{filePath.TrimEnd(xmlExtension)}.cstr", FileMode.Create, FileAccess.Write))
                {
                    serializer.Serialize(input, saveObject);
                    using (GZipStream compression = new GZipStream(output, CompressionMode.Compress))
                    {
                        byte[] recBytes = input.ToArray();
                        compression.Write(recBytes, 0, recBytes.Length);
                    }
                }
            }
            catch (Exception exception)
            {
                logger.Error(exception.ToString());
            }
        }

        // Deserialize the data
        public static SavedRecommender DeserializeRecommender(string filePath)
        {
            using (GZipStream decompress = new GZipStream(File.OpenRead(filePath), CompressionMode.Decompress))
            using (TextReader textReader = new StreamReader(decompress))
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(SavedRecommender));
                //TextReader textReader = new StreamReader(filePath);// + "/recommender_data.xml");
                SavedRecommender savedRecommender = (SavedRecommender)deserializer.Deserialize(textReader);
                textReader.Close();

                return savedRecommender;
            }
        }
    }
}
