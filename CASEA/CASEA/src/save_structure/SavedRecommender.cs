﻿/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using CASEA.src.classifier;
using CASEA.src.functions;
using CASEA.src.ui_forms;

namespace CASEA.src.save_structure
{
    [Serializable]
    public class SavedRecommender// Stores data to be saved via serialization. Requires Heuristics / pathgroup data yet.
    {
        // Elements necessary for a recommender to be analyzed.
        public string recommenderName;
        public TrainingData TDS;
        public ValidationData VDS;
        public Configuration currentConfiguration;
        public List<Configuration> history;
        public MLAlgorithm.Types algorithm;
        public int attempt;

        // Constructors
        public SavedRecommender() { }
        public SavedRecommender(string name, TrainingData tds, ValidationData vds, Configuration current)
        {
            recommenderName = name;
            TDS = tds;
            VDS = vds;
            currentConfiguration = current;
        }

        // create a new configuration and push the old one.
        public void newRecommender(int frequencyCutoff, List<HeuristicEnum> heuristics, HeuristicEnum other, MLAlgorithm.Types algorithm, RecommenderType recType)
        {
            // Add current to history then set current to the configuration which is passed.
            history.Add(new Configuration(frequencyCutoff, other, heuristics.ToArray(), algorithm, recType));
            currentConfiguration.attempt = attempt;
            currentConfiguration = new Configuration(frequencyCutoff, other, heuristics.ToArray(), algorithm, recType);

            // If the count of history is > 10 at this point remove an element
            // TODO Why is this needed?
            if (history.Count > 10)
            {
                history.RemoveAt(0);
            }
        }
    }
}