﻿/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/

using CASEA.src.classifier;
using CASEA.src.save_structure;
using CASEA.src.functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CASEA.src.ui_forms;

namespace CASEA.src.configuration
{
    class AutomaticConfiguration
    {

        private static HeuristicEnum DEFAULT_HEURISTIC = HeuristicEnum.AssignedTo;
        private static int DEFAULT_NUM_HEURISTICS = 5;                
        private static int DEFAULT_THRESHOLD = 5;
        
        private SavedRecommender recommender;
        private String filePath;
        public Configuration configuration { get; }
        private EvaluationMetrics bestAccuracy;
        private string[] lifeCyclePaths;
        private CreationAssistant app;
        private RecommenderType type;        

        public AutomaticConfiguration(RecommenderType type, SavedRecommender r, String path, CreationAssistant window)
        {
            this.type = type;
            recommender = r;
            filePath = path;
            app = window;            

            List<HeuristicEnum> heuristics = new List<HeuristicEnum>();
            for (int i = 0; i < DEFAULT_NUM_HEURISTICS; i++)
                heuristics.Add(DEFAULT_HEURISTIC);

            int startingThreshold = calculateMean(); ;
           
             

            this.configuration = new Configuration(startingThreshold, DEFAULT_HEURISTIC, heuristics.ToArray(), r.algorithm, type);            
            this.lifeCyclePaths = recommender.TDS.paths.pathNames.Take(configuration.heuristics.Count()).ToArray();

            bestAccuracy = new EvaluationMetrics();
        }

        public bool execute()
        {
            app.appendToStatus("Creating baseline recommender.");
            MLAlgorithm classifier = createRecommender(configuration);
            bestAccuracy = classifier.averageMetrics;
            app.appendToStatus("Baseline (F1): " + bestAccuracy.f1.Average().ToString());

            switch (type)
            {
                case RecommenderType.Developer:
                    Configuration newConfiguration = findHeuristics(configuration);
                    configuration.frequencyCutoff = findThreshold(newConfiguration);
                    break;
                case RecommenderType.Component:
                    configuration.frequencyCutoff = findThreshold(configuration);
                    break;
            }
            

            app.appendToStatus("Best Automated Configuration");
            app.appendToStatus("Threshold: " + configuration.frequencyCutoff);

            if (RecommenderType.Developer.Equals(type))
            {
                foreach (HeuristicEnum h in configuration.heuristics)
                {
                    app.appendToStatus(h.ToString());
                }
                app.appendToStatus("Other: " + configuration.otherHeuristic);
                app.appendToStatus("Metrics: " + classifier.averageMetrics);
                app.appendToStatus("Average F1: " + bestAccuracy.f1.Average());
                app.appendToStatus("Average Precision: " + bestAccuracy.precision.Average());
                app.appendToStatus("Average Recall: " + bestAccuracy.recall.Average());
            }
            return true;
        }

        /* Based on empirical evidence, this configuration works best for Eclipse data set*/
        private void bestUserConfigurationEclipse()
        {
            Configuration userConfig = new Configuration();
            userConfig.frequencyCutoff = 65;
            userConfig.heuristics = new HeuristicEnum[] { HeuristicEnum.FixedBy, HeuristicEnum.Resolver, HeuristicEnum.AssignedTo, HeuristicEnum.FirstResponder, HeuristicEnum.AssignedTo, HeuristicEnum.AssignedTo };            
            userConfig.otherHeuristic = HeuristicEnum.AssignedTo;
            userConfig.algorithm = MLAlgorithm.Types.SVM;

            MLAlgorithm classifier = createRecommender(userConfig);

            app.appendToStatus("Best User Configuration (Eclipse)");
            app.appendToStatus("Threshold: " + userConfig.frequencyCutoff);
            foreach (HeuristicEnum h in userConfig.heuristics)
            {
                app.appendToStatus(h.ToString());
            }
            app.appendToStatus("Other: " + userConfig.otherHeuristic);
            app.appendToStatus("Metrics: " + classifier.averageMetrics);
            app.appendToStatus("Average F1: " + bestAccuracy.f1.Average());
            app.appendToStatus("Average Precision: " + bestAccuracy.precision.Average());
            app.appendToStatus("Average Recall: " + bestAccuracy.recall.Average());


        }

        private MLAlgorithm createRecommender(Configuration configuration)
        {
            // Apply the heuristics to the training data
            recommender.TDS.updateOutputs(lifeCyclePaths, configuration.heuristics, configuration.otherHeuristic, recommender.TDS, filePath);

            // Create the recommender
            return MLAlgorithm.getClassifier(type, configuration.algorithm, XMLParser.frequencyFiltering(type, recommender.TDS, recommender.VDS, configuration.frequencyCutoff), app);
        }

        private int findThreshold(Configuration c)
        {
            Configuration config = new Configuration(c.frequencyCutoff, c.otherHeuristic, c.heuristics, c.algorithm, type);

            // Assume that better threshold is greater
            int maxThreshold;

            int MAX_ITERATIONS = 50;            
            int bestThreshold = calculateMean();

            int[] thresholdSteps = { 50, 25, 10, 5, 1 };            
            for (int stepIndex = 0; stepIndex < thresholdSteps.Length; stepIndex++)
            {
                if (stepIndex == 0)
                {
                    maxThreshold = getMaxThreshold(type);
                }
                else
                {
                    maxThreshold = bestThreshold;
                    config.frequencyCutoff = bestThreshold - thresholdSteps[stepIndex - 1];
                }
                config.frequencyCutoff = c.frequencyCutoff + thresholdSteps[stepIndex];                
                bestAccuracy = new EvaluationMetrics();

                for (int iterations = 0; iterations < MAX_ITERATIONS && config.frequencyCutoff < maxThreshold; config.frequencyCutoff += thresholdSteps[stepIndex])
                {                                  
                    MLAlgorithm classifier = createRecommender(config);
                    EvaluationMetrics newAccuracy = classifier.averageMetrics;

                    printConfiguration(config, newAccuracy);

                    if (foundBetter(type, bestAccuracy, newAccuracy))
                    {
                        bestAccuracy = newAccuracy;                        
                        bestThreshold = config.frequencyCutoff;

                        // Inform user
                        switch (type)
                        {
                            case RecommenderType.Developer:
                                app.appendToStatus("==> Best Threshold: " + bestThreshold + "(Precision: " + bestAccuracy.precision.Average() + ")");
                                break;
                            case RecommenderType.Component:
                                app.appendToStatus("==> Best Threshold: " + bestThreshold + "(Recall: " + bestAccuracy.recall.Average() + ")");
                                break;
                        }
                    }
                }
            }
            return bestThreshold;
        }

        private int getMaxThreshold(RecommenderType type)
        {
            int[] labelDistribtion = new int[0];

            switch (type)
            {
                case RecommenderType.Developer:
                    labelDistribtion = recommender.TDS.reportDistribution();
                    
                    break;
                case RecommenderType.Component:
                    labelDistribtion = recommender.TDS.componentDistribution();
                    break;

            }
            Array.Sort(labelDistribtion, (x, y) => y.CompareTo(x));

            // Set maximum freqency cutoff to the highest developer.
            return labelDistribtion[0];            
        }

        private Configuration findHeuristics(Configuration bestConfiguration)
        {

            HeuristicEnum[] bestHeuristics = new HeuristicEnum[bestConfiguration.heuristics.Count()];
            Array.Copy(bestConfiguration.heuristics, bestHeuristics, bestConfiguration.heuristics.Count());

            HeuristicEnum[] heuristicList = (HeuristicEnum[])Enum.GetValues(typeof(HeuristicEnum));

            Configuration config = new Configuration(bestConfiguration.frequencyCutoff, DEFAULT_NUM_HEURISTICS, DEFAULT_HEURISTIC, bestConfiguration.algorithm, type);

            MLAlgorithm classifier;
            HeuristicEnum bestHeuristic;
            int threshold = 0, bestThreshold = 0;
            EvaluationMetrics newAccuracy;

            for (int i = 0; i < lifeCyclePaths.Count(); i++)
            {
                bestHeuristic = DEFAULT_HEURISTIC;
                bestThreshold = 0;
                
                foreach (HeuristicEnum h in heuristicList)
                {
                    if (h != DEFAULT_HEURISTIC)
                    {
                        config.heuristics[i] = h;

                        // Apply the heuristics to the training data
                        recommender.TDS.updateOutputs(lifeCyclePaths, config.heuristics, config.otherHeuristic, recommender.TDS, filePath);
                        // Create recommender with average reports solved as threshold                                             
                        threshold = calculateMean();
                        config.frequencyCutoff = threshold;
                        classifier = createRecommender(config);

                        newAccuracy = classifier.averageMetrics;

                        printConfiguration(config, newAccuracy);

                        if (foundBetter(type, bestAccuracy, newAccuracy))
                        {
                            bestAccuracy = newAccuracy;
                            bestHeuristic = h;
                            bestThreshold = threshold;

                            // Inform user
                            switch (type)
                            {
                                case RecommenderType.Developer:
                                    app.appendToStatus("==> Best Heuristic for " + i + ": " + h.ToString() + "(Precision: " + bestAccuracy.precision.Average() + ")");
                                    break;
                                case RecommenderType.Component:
                                    app.appendToStatus("==> Best Heuristic for " + i + ": " + h.ToString() + "(Recall: " + bestAccuracy.recall.Average() + ")");
                                    break;
                            }

                        }                      
                    }
                }
                config.heuristics[i] = bestHeuristic;
                config.frequencyCutoff = bestThreshold > 0 ? bestThreshold : threshold;             
            }

            // Find best for "Other"
           
            bestHeuristic = DEFAULT_HEURISTIC;
            foreach (HeuristicEnum h in heuristicList)
            {
                if (h != DEFAULT_HEURISTIC)
                {
                    config.otherHeuristic = h;

                    // Apply the heuristics to the training data
                    recommender.TDS.updateOutputs(lifeCyclePaths, config.heuristics, config.otherHeuristic, recommender.TDS, filePath);
                    // Create recommender with average reports solved as threshold                                             
                    threshold = calculateMean();
                    config.frequencyCutoff = threshold;

                    classifier = createRecommender(config);
                    newAccuracy = classifier.averageMetrics;

                    printConfiguration(config, newAccuracy);

                    if (foundBetter(type, bestAccuracy, newAccuracy))
                    {
                        bestAccuracy = newAccuracy;
                        bestHeuristic = h;
                        bestThreshold = threshold;

                        // Inform user
                        switch (type)
                        {
                            case RecommenderType.Developer:
                                app.appendToStatus("==> Best Heuristic for 'Other': " + h.ToString() + "(Precision: " + bestAccuracy.precision.Average() + ")");
                                break;
                            case RecommenderType.Component:
                                app.appendToStatus("==> Best Heuristic for 'Other': " + h.ToString() + "(Recall: " + bestAccuracy.recall.Average() + ")");
                                break;
                        }

                    }
                }
            }
            config.otherHeuristic = bestHeuristic;
            config.frequencyCutoff = bestThreshold > 0 ? bestThreshold : threshold; ;



            for (int i = 0; i < lifeCyclePaths.Count(); i++)
            {
                bestHeuristics[i] = config.heuristics[i];
            }

            bestConfiguration.heuristics = config.heuristics;
            bestConfiguration.frequencyCutoff = config.frequencyCutoff;
            bestConfiguration.otherHeuristic = config.otherHeuristic;

            return bestConfiguration;
        }

        private int calculateMean()
        {

            switch (type)
            {
                case RecommenderType.Developer:
                    return (int)recommender.TDS.reportDistribution().Average();
                    
                case RecommenderType.Component:
                    return (int)recommender.TDS.componentDistribution().Average();                    
            }
            return DEFAULT_THRESHOLD;
        }
            
        private void printConfiguration(Configuration config, EvaluationMetrics accuracy)
        {
            app.appendToStatus(config.ToString() + ",");
            app.appendToStatus("F1:" + accuracy.f1.Average() + ",");
            app.appendToStatus("P:" + accuracy.precision.Average() + ",");
            app.appendToStatus("R:" + accuracy.recall.Average());
        }

        private bool foundBetter(RecommenderType type, EvaluationMetrics bestSoFar, EvaluationMetrics newResult)
        {
            switch (type)
            {
                case RecommenderType.Developer:
                    // Look for best precision
                    if (bestSoFar.precision.Average() < newResult.precision.Average())
                        return true;
                    break;
                case RecommenderType.Component:                
                    // Look for best recall
                    if (bestSoFar.recall.Average() < newResult.recall.Average())
                        return true;
                    break;
            }
            return false;
        }
    }
}
