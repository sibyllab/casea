﻿/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using CASEA.Properties;
using CASEA.src.classifier;
using Lucene.Net.Analysis;
using Lucene.Net.Index;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Analysis.Tokenattributes;
using CASEA.src.ui_forms;

namespace CASEA.src.functions
{
    public class XMLParser : Parser
    {
        private static char[] ignore = { 'V', 'C' }; // Resolutions to ignore

        // For parsing the TDS in new recommenders.
        public static TrainingData parseTDS(string filePath)
        {
            // Open up the input file
            XDocument inputFile = XDocument.Load(filePath);
            
            //Analyzer analyzer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_CURRENT);
            TokenStream stream;

            // Output variables
            TrainingData TDS;
            Dictionary<string, double[]> uniqueWords = new Dictionary<string, double[]>();
            Dictionary<string, int[]> developers = new Dictionary<string, int[]>();
            Dictionary<string, int> pathGroups = new Dictionary<string, int>();
            Component component = new Component();
            Dictionary<string, List<string>> componentTeams = new Dictionary<string, List<string>>();
            Dictionary<string, int> componentFreqs = new Dictionary<string, int>();
            List<string> bugComponents = new List<string>();


            // Construct the input arrays. 
            int index = 0;
            foreach (XElement bugreport in inputFile.Elements().Elements())
            {
                string words;

                try
                {
                    words = bugreport.Element("bug_text").Value;
                }
                catch
                {
                    words = bugreport.Element("summary").Value;
                }

                string nouns = filterNouns(words);

                string[] wordArray;
                List<String> wordList = new List<String>();
             
                stream = new StandardTokenizer(Lucene.Net.Util.Version.LUCENE_30, new StringReader(nouns));                
                stream = new LengthFilter( new PorterStemFilter( new StandardFilter( new LowerCaseFilter(stream))), 2, 50);
                
                while(stream.IncrementToken())
                {
                    wordList.Add(stream.GetAttribute<ITermAttribute>().Term.ToString());
                }
                
                wordArray = wordList.ToArray();

                // Create and increment path groups

                string path = bugreport.Element("pathgroup").Value;
               
                // Test if already in resolution group format
                switch (path)
                {
                    case "FIXED":
                    case "NEW":
                    case "ASSIGNED":
                    case "REOPENED":
                    case "WORKS FOR ME":
                    case "WORKSFORME":
                    case "WONTFIX":
                    case "DUPLCIATE": break; // do nothing
                    default:
                        // Must be path group format
                        path = bugreport.Element("pathgroup").Value.TrimEnd(ignore).Last<char>().ToString();
                        path = getResolutionName(path);
                        break;
                }

                if (!pathGroups.ContainsKey(path))
                {
                    pathGroups.Add(path, 1);
                }
                else
                {
                    pathGroups[path]++;
                }

                // Create developers dictionary
                string dev = bugreport.Element("assigned").Value;
                if (developers.Keys.Contains(dev))
                {
                    developers[dev][index] = 1;
                }
                else
                {
                    developers.Add(dev, new int[inputFile.Elements().Elements().Count()]);
                    developers[dev][index] = 1;
                }

                // Create component dictionary
                bugComponents.Add(bugreport.Element("component").Value);                

                // Create unique words dictionary
                foreach (string word in wordArray)
                {
                    if (stopWords.Contains(word)) continue; // =D
                    else if (uniqueWords.Keys.Contains(word.ToLower()))
                    {
                        uniqueWords[word.ToLower()][index] += 1.0;
                    }
                    else if (word.Length > 0)
                    {
                        uniqueWords.Add(word.ToLower(), new double[inputFile.Elements().Elements().Count()]);
                        uniqueWords[word.ToLower()][index] += 1.0;
                    }
                    else { }
                } 
                index++;

                // Component information
                recordComponentInformation(bugreport, dev, componentTeams, componentFreqs);
            }

            List<string> singleTerms = new List<string>();

            foreach (KeyValuePair<string, Double[]> keyPair in uniqueWords)
            {
                int occurrenceCount = 0;

                // Checks the Double[] for each key pair and looks to see if there is more than one
                // instance of the word being in a report (represented by more than one array location
                // containing a value >= 1.0).
                foreach (double occurrence in keyPair.Value)
                {
                    if (occurrence > 0)
                    {
                        occurrenceCount++;

                        if (occurrenceCount > 1)
                        {
                            break;
                        }
                    }
                }

                // Saves the keys of terms with only one occurrence.
                if (occurrenceCount == 1)
                {
                    singleTerms.Add(keyPair.Key);
                }
            }

            // Removes these terms, as they cannot be used for recommenders.
            foreach (string s in singleTerms)
            {
                uniqueWords.Remove(s);
            }

            // Construct the inputs for recommendation
            double[][] TDSInputArray = new double[inputFile.Elements().Elements().Count()][];
            for (int i = 0; i < inputFile.Elements().Elements().Count(); i++)
            {
                double[] wordArray = new double[uniqueWords.Keys.Count];
                index = 0;
                foreach (KeyValuePair<string, double[]> kv in uniqueWords)
                {
                    wordArray[index] = kv.Value[i];
                    index++;
                }
                TDSInputArray[i] = wordArray;
            }

            // set the component elements
            component.names = componentTeams.Keys.ToArray();
            component.developers = componentTeams.Values.Select(list => list.ToArray()).ToArray();
            component.freqs = componentFreqs.Values.ToArray();

            //// Sort the pathGroups
            pathGroups = (from entry in pathGroups orderby entry.Value descending select entry).ToDictionary(pair => pair.Key, pair => pair.Value);

            // Fill in path groups (can't serialize a Dictionary)
            PathGroups paths = new PathGroups();
            paths.pathNames = new string[pathGroups.Keys.Count];
            paths.pathDistribution = new int[pathGroups.Keys.Count];
            for (int i = 0; i < pathGroups.Keys.Count; i++)
            {
                paths.pathNames[i] = pathGroups.Keys.ElementAt(i);
                paths.pathDistribution[i] = pathGroups.Values.ElementAt(i);
            }

            int numBugReports = inputFile.Elements().Elements().Count();
            // Construct developer recommendation information
            DeveloperInstances devInstances = createDevRecInstances(numBugReports, developers);
            
            // Construct component recommendation information
            ComponentInstances componentInstances = createCompRecInstances(bugComponents, componentTeams.Keys.ToArray());
            

            // Create and return the testing data set
            TDS = new TrainingData(uniqueWords.Keys.ToArray(), TDSInputArray, 
                devInstances, componentInstances, component, paths);

           

            return TDS;
        }

        public static TrainingData updateOutputs(string[] names, HeuristicEnum[] heuristics, HeuristicEnum other, TrainingData data, string filePath)
        {
            // Load the XDocument
            XDocument TDS = XDocument.Load(filePath + "/TDS.xml");
            Dictionary<string, int[]> developers = new Dictionary<string, int[]>();
            List<List<int>> outputArray = new List<List<int>>();
            Component component = new Component();
            Dictionary<string, List<string>> componentTeams = new Dictionary<string, List<string>>();
            Dictionary<string, int> componentFreqs = new Dictionary<string, int>();
            string developer = null;
            int index = 0;

            // Construct a new developer output for each list.
            foreach (XElement bugreport in TDS.Elements().Elements())
            {
                string path = bugreport.Element("pathgroup").Value;

                // Test if already in resolution group format
                switch (path)
                {
                    case "FIXED":
                    case "NEW":
                    case "ASSIGNED":
                    case "REOPENED":
                    case "WORKS FOR ME":
                    case "WORKSFORME":
                    case "WONTFIX":
                    case "DUPLCIATE": break; // do nothing
                    default:
                        // Must be path group format
                        path = bugreport.Element("pathgroup").Value.TrimEnd(ignore).Last<char>().ToString();
                        path = getResolutionName(path);
                        break;
                }

                if (names.Contains(path))
                {
                    for (int i = 0; i < names.Length; i++)
                    {
                        if (names[i] == null) continue;
                        if (names[i].Equals(path))
                        {
                            switch (heuristics[i]) // Switch around the heuristic
                            {
                                case HeuristicEnum.AssignedTo:
                                    developer = bugreport.Element("assigned").Value;
                                    break;
                                case HeuristicEnum.FirstResponder:
                                    if (bugreport.Element("firstresponder") != null)
                                        developer = bugreport.Element("firstresponder").Value;
                                    else developer = null;
                                    break;
                                case HeuristicEnum.FixedBy:
                                    if (bugreport.Element("fixedby") != null)
                                        developer = bugreport.Element("fixedby").Value;
                                    else developer = null;
                                    break;
                                case HeuristicEnum.Reporter:
                                    if (bugreport.Element("reporter") != null)
                                        developer = bugreport.Element("reporter").Value;
                                    else developer = null;
                                    break;
                                case HeuristicEnum.Resolver:
                                    if (bugreport.Element("resolver") != null)
                                        developer = bugreport.Element("resolver").Value;
                                    else developer = null;
                                    break;
                                case HeuristicEnum.Unused:
                                    developer = null;
                                    break;
                                default:
                                    developer = null;
                                    break;
                            }                           
                        }
                    }
                }
                else
                {
                    switch (other) // Switch around the other heuristic
                    {
                        case HeuristicEnum.AssignedTo:
                            developer = bugreport.Element("assigned").Value;
                            break;
                        case HeuristicEnum.FirstResponder:
                            if (bugreport.Element("firstresponder") != null)
                                developer = bugreport.Element("firstresponder").Value;
                            else developer = null;
                            break;
                        case HeuristicEnum.FixedBy:
                            if (bugreport.Element("fixedby") != null)
                                    developer = bugreport.Element("fixedby").Value;
                            else developer = null;
                            break;
                        case HeuristicEnum.Reporter:
                            if (bugreport.Element("reporter") != null)
                                developer = bugreport.Element("reporter").Value;
                            else developer = null;
                            break;
                        case HeuristicEnum.Resolver:
                            if (bugreport.Element("resolver") != null)
                                developer = bugreport.Element("resolver").Value;
                            else developer = null;
                            break;
                        case HeuristicEnum.Unused:
                            developer = null;
                            break;
                        default:
                            developer = null;
                            break;
                    }
                   
                }

                if (developer == null)
                {
                    // do nothing
                }
                else if (developers.Keys.Contains(developer))
                {
                    developers[developer][index] = 1;
                }
                else
                {
                    developers.Add(developer, new int[TDS.Elements().Elements().Count()]);
                    developers[developer][index] = 1;
                }

                // Constructing the component information
                recordComponentInformation(bugreport, developer, componentTeams, componentFreqs);

                index++;                
            }

            int numBugReports = index;

            data.output = new Output(createDevRecInstances(numBugReports, developers), data.output.componentInstances);
            
            component.names = componentTeams.Keys.ToArray();
            component.developers = componentTeams.Values.Select(list => list.ToArray()).ToArray();
            component.freqs = componentFreqs.Values.ToArray();
                  
            data.components = component;

            return data;
        }

        // Parse the validation testing set
        public static ValidationData parseVDS(TrainingData TDS, string filePath)
        {
            // Open up the input file
            XDocument inputFile = XDocument.Load(filePath);

            // Return object
            ValidationData VDS;

            // Parsing tokenizer
            TokenStream stream;

            // Variables
            string[] uniqueWords = TDS.input.words;
            double[][] inputArray = new double[inputFile.Elements().Elements().Count()][];
            List<string> components = new List<string>();
            List<string> bugIds = new List<string>();

            // Construct the inputArray
            int index = 0;
            foreach (XElement bugreport in inputFile.Elements().Elements())
            {
                double[] array = new double[TDS.input.inputArray[0].Length];

                string[] words; //= bugreport.Element("bug_text").Value.Split(delimiterChars);
                List<string> wordList = new List<string>();
                string bug_text;

                try
                {
                    bug_text = bugreport.Element("bug_text").Value;
                }
                catch
                {
                    bug_text = bugreport.Element("summary").Value;
                }
                
                bug_text = bug_text.Replace("0", " ").Replace("1", " ").Replace("2", " ").Replace("3", " ").Replace("4", " ");
                bug_text = bug_text.Replace("5", " ").Replace("6", " ").Replace("7", " ").Replace("8", " ").Replace("9", " ");
                stream = new StandardTokenizer(Lucene.Net.Util.Version.LUCENE_30, new StringReader(bug_text));
                stream = new LengthFilter(new PorterStemFilter(new StandardFilter(new LowerCaseFilter(stream))), 2, 20);

                while (stream.IncrementToken())
                {
                    wordList.Add(stream.GetAttribute<ITermAttribute>().Term.ToString());
                }

                words = wordList.ToArray();

                foreach (string word in words)
                {
                    if (uniqueWords.Contains(word.ToLower()))
                    {
                        int idx = 0;
                        for (int i = 0; i < uniqueWords.Count(); i++)
                        {
                            if (uniqueWords[i].Equals(word.ToLower()))
                            {
                                idx = i;
                                break;
                            }
                        }
                        array[idx] += 1.0;
                    }
                }
                inputArray[index] = array;
                index++;
                components.Add(bugreport.Element("component").Value);
                bugIds.Add(bugreport.Element("reportid").Value);
            }

            // Create and return the validation data set
            VDS = new ValidationData(uniqueWords, inputArray, bugIds.ToArray(), components.ToArray());

            return VDS;
        }

        private static void recordComponentInformation(XElement report, string developer, Dictionary<string, List<string>> teams, Dictionary<string, int> freqs)
        {
            string componentName = report.Element("component").Value;
            // Constructing the component teams
            if (!teams.Keys.Contains(componentName))
            {
                teams.Add(componentName, new List<string>());
            }

            if (!teams[componentName].Contains(developer))
            {
                teams[componentName].Add(developer);
            }

            // Constructing the component frequencies
            if (!freqs.Keys.Contains(componentName))
            {
                freqs.Add(componentName, 1);
            }
            else
            {
                freqs[componentName]++;
            }
        }

        private static string getResolutionName(string r)
        {
            switch (r)
            {
                case "F": return "FIXED";
                case "N": return "NEW";
                case "A": return "ASSIGNED";
                case "R": return "REOPENED";
                case "M": return "WORKS FOR ME";
                case "X": return "WONTFIX";
                default: return "UNKNOWN RESOLUTION";
            }
        }
    }
}
