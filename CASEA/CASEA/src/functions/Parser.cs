﻿/*
Copyright(C) 2012-2016  John Anvik, Trevor Henders, Henry Burton, Justin Canada, Marshall Brooks

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see<http://www.gnu.org/licenses/>
*/
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using CASEA.Properties;
using CASEA.src.classifier;
using Lucene.Net.Analysis;
using Lucene.Net.Index;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Analysis.Tokenattributes;
using CASEA.src.ui_forms;

// For Parts-of-Speech analysis
using java.io;
using java.util;
using edu.stanford.nlp.ling;
using edu.stanford.nlp.tagger.maxent;


namespace CASEA.src.functions
{
    public class Parser
    {
        protected static string[] stopWords = Resources.StopWords.Split(',');

        // For parts-of-speech tagging
        static string Model = @"models\wsj-0-18-bidirectional-nodistsim.tagger";
        static MaxentTagger tagger = new MaxentTagger(Model);


        // Filter frequency by iterating over the lists.
        public static InputOutputPack frequencyFiltering(RecommenderType recType, TrainingData TDS, ValidationData VDS, int cutoff)
        {
            // Returns and abbreviated version of the TDS with different words and cutoff filtering
            TrainingData filteredTDS;
            ValidationData filteredVDS;
            DeveloperInstances devInstances = TDS.output.developerInstances;
            ComponentInstances componentInstances = TDS.output.componentInstances;

            // multidimensional lists
            List<List<double>> inputList = TDS.input.inputArray.Select(array => array.ToList()).ToList();
            
            switch (recType)
            {
                case RecommenderType.Component:
                    // remove components below threshold
                    List<string> components = TDS.output.componentInstances.names.OfType<string>().ToList();
                    List<List<int>> effects = TDS.output.componentInstances.effects.Select(array => array.ToList()).ToList();
                    int componentFreq;

                    for (int i = components.Count - 1; i >= 0; i--)
                    {
                        componentFreq = componentFrequency(TDS.output.componentInstances.effects, i);
                        if (componentFreq < cutoff || componentFreq == 1)
                        {
                            foreach (List<int> row in effects)
                            {
                                row.RemoveAt(i);
                            }
                            components.RemoveAt(i);
                        }
                    }

                    // remove reports which are no longer included
                    for (int i = TDS.output.componentInstances.effects.Length - 1; i >= 0; i--)
                    {
                        if (effects.ElementAt(i).ToArray().Contains(1) == false)
                        {
                            inputList.RemoveAt(i);
                            effects.RemoveAt(i);
                        }
                    }

                    componentInstances.names = components.ToArray();
                    componentInstances.effects = effects.Select(list => list.ToArray()).ToArray();

                    break;
                case RecommenderType.Developer:
                default:
                    // remove developers below threshold
                    List<string> developers = TDS.output.developerInstances.names.OfType<string>().ToList();
                    List<List<int>> whoFixed = TDS.output.developerInstances.whoFixed.Select(array => array.ToList()).ToList();
                    int reportsSolved;
                    for (int i = developers.Count - 1; i >= 0; i--)
                    {
                        reportsSolved = numOfReportsSolved(TDS.output.developerInstances.whoFixed, i);
                        if (reportsSolved < cutoff || reportsSolved == 1)
                        {
                            foreach (List<int> row in whoFixed)
                            {
                                row.RemoveAt(i);
                            }
                            developers.RemoveAt(i);
                        }
                    }

                    // remove rows which now have no assignee
                    for (int i = TDS.output.developerInstances.whoFixed.Length - 1; i >= 0; i--)
                    {
                        if (!reportAssigned(whoFixed.ElementAt(i).ToArray()))
                        {
                            inputList.RemoveAt(i);
                            whoFixed.RemoveAt(i);
                        }
                    }

                    devInstances.names = developers.ToArray();
                    devInstances.whoFixed = whoFixed.Select(list => list.ToArray()).ToArray();

                    break;
            }

            
            

            // Update input array
            List<int> toRemove = removeObsoleteFeatures(inputList);
            TDS.input.inputArray = inputList.Select(list => list.ToArray()).ToArray();

            // Remove obsolete words 
            // Update VDS.input which must match TDS.input columns
            List<string> words = TDS.input.words.OfType<string>().ToList();
            List<List<double>> vdsList = VDS.input.inputArray.Select(array => array.ToList()).ToList();

            foreach (int index in toRemove){
                words.RemoveAt(index);
                foreach (List<double> row in vdsList)
                    row.RemoveAt(index);
            }
            VDS.input.inputArray = vdsList.Select(list => list.ToArray()).ToArray();

            filteredTDS = new TrainingData(words.ToArray(), inputList.Select(list => list.ToArray()).ToArray(),
                devInstances, componentInstances, TDS.components, TDS.paths);
            filteredVDS = new ValidationData(words.ToArray(), vdsList.Select(list => list.ToArray()).ToArray(), VDS.reportIds, VDS.components);

            // Construct output            
            InputOutputPack output = new InputOutputPack(filteredTDS, filteredVDS); 
            return output;
        }


        /**
         * Removes columns where there are no longer any values.
         * Returns a list of indices of the columns that were removed.
         */
        public static List<int> removeObsoleteFeatures(List<List<double>> features)
        {
            List<int> obsoleteWordIndices = new List<int>();

            // remove columns of words which are obsolete
            double[][] columns = features.Select(list => list.ToArray()).ToArray();

            if (features.Count > 0)
            {
                for (int i = features.ElementAt(0).Count - 1; i >= 0; i--)
                {
                    if (!wordAppears(columns, i))
                    {
                        foreach (List<double> row in features)
                        {
                            row.RemoveAt(i);
                        }
                        obsoleteWordIndices.Add(i);
                    }
                }
            }
            return obsoleteWordIndices;
        }

        // Tells how many reports a developer solved
        internal static int numOfReportsSolved(int[][] whoFixed, int column)
        {
            int total = 0;

            for (int i = 0; i < whoFixed.Length; i++)
            {
                if (whoFixed[i][column] > 0)
                    total += 1;
            }

            return total;
        }

        internal static int componentFrequency(int[][] effects, int column)
        {
            int total = 0;

            for (int i = 0; i < effects.Length; i++)
            {
                if (effects[i][column] > 0)
                    total += 1;
            }

            return total;
        }

        // Tells whether the report has a developer assigned to it
        private static bool reportAssigned(int[] array)
        {
            if (array.Contains(1))
            {
                return true;
            }
            return false;
        }

        // Tells whether the word appears
        private static bool wordAppears(double[][] array, int column)
        {
            for (int i = 0; i < array.Length; i++)
            {
                //Console.WriteLine("(Row " + i + " Column " + column + ") - Word value: " + array[i][column]);
                if (array[i][column] > 0)
                {
                    return true;
                }
            }
            return false;
        }

        // Perform tf-idf weighting
        private static double[] calculateIDF(double[][] input)
        {
            // construct idf
            double[] idf = new double[input[0].Length];
            for (int i = 0; i < input.Length; i++)
            {
                for (int j = 0; j < input[0].Length; j++)
                {
                    if (input[i][j] > 0.0)
                    {
                        idf[j] += 1.0;
                    }
                }
            }

            // find idf values. 
            for (int i = 0; i < idf.Length; i++)
            {
                // See http://www.tfidf.com/
                idf[i] = Math.Log10(input.Length / idf[i]);
            }

            // return idf array.
            return idf;
        }

        protected static ComponentInstances createCompRecInstances(List<string> bugComponents, string[] names)
        {
            ComponentInstances compInstances = new ComponentInstances();
            int index;

            int[][] bugEffectsComponent = new int[bugComponents.Count][];
            for (int i = 0; i < bugComponents.Count; i++)
            {
                int[] components = new int[names.Length];
                index = 0;
                foreach (string componentName in names)
                {
                    if (componentName.Equals(bugComponents[i]))
                        components[index] = 1;
                    else
                        components[index] = -1;
                    index++;
                }
                bugEffectsComponent[i] = components;
            }

            compInstances.names = names;
            compInstances.effects = bugEffectsComponent.ToArray();

            return compInstances;
        }

        protected static DeveloperInstances createDevRecInstances(int numBugReports, Dictionary<string, int[]> developers)
        {
            DeveloperInstances devInstances = new DeveloperInstances();
            int index;

            int[][] whoFixed = new int[numBugReports][];
            for (int i = 0; i < numBugReports; i++)
            {
                int[] developerArray = new int[developers.Keys.Count];
                index = 0;
                foreach (KeyValuePair<string, int[]> kv in developers)
                {
                    if (kv.Value[i] == 1)
                        developerArray[index] = 1;
                    else
                        developerArray[index] = -1;
                    index++;
                }
                whoFixed[i] = developerArray;
            }

            devInstances.names = developers.Keys.ToArray();
            devInstances.whoFixed = whoFixed.ToArray();

            return devInstances;
        }

        public static void calculateTfIdf(ref double[][] wordFrequency)
        {
            // TF-IDF weighting
            double[] idf = calculateIDF(wordFrequency);
            for (int i = 0; i < wordFrequency.Length; i++)
            {
                for (int j = 0; j < idf.Length; j++)
                {
                    if (i < wordFrequency.Length) // Multiply if this element exists
                    {
                        wordFrequency[i][j] *= idf[j];
                    }
                }
            }
        }

        public static string filterNouns(string text)
        {
            string[] nounPOS = { "NNP", "NN", "NNS", "NNPS" };
            string[] symbols = { "-LSB-", "-RSB-" }; // left & right square brackets
            string nouns = "";

           var sentences = MaxentTagger.tokenizeText(new java.io.StringReader(text)).toArray();
            foreach (ArrayList sentence in sentences)
            {
                var taggedSentence = tagger.tagSentence(sentence);
                for(int i=0; i<taggedSentence.size(); i++)
                {
                    var term = taggedSentence.get(i);
                    int splitAt = term.ToString().LastIndexOf('/');
                    string pos = term.ToString().Substring(splitAt+1);
                    string noun = term.ToString().Substring(0, splitAt);
                    if (nounPOS.Contains(pos) && !symbols.Contains(noun))
                        nouns += noun + " ";
                }
            }
            return nouns;
        }
    }
}
