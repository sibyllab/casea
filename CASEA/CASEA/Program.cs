﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using CASEA.src.ui_forms;

namespace CASEA
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new CASEA.src.ui_forms.MainMenu());
            //Application.Run(new CASEA.src.ui_forms.SurveyPage());
            //Application.Run(new CreationAssistant("C:\\Users\\John\\Documents\\casea\\CASEA\\CASEA\\recommenders\\Eclipse"));
            //Application.Run(new CreationAssistant(""));
        }
    }
}
